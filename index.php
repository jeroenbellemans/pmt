<?php
	
	// Start Session
	session_start();
	// Set timezont on GMT+1
	date_default_timezone_set('Europe/Amsterdam');
	// Include database connection
	include 'database/my-connection.php';
	// Include the functionalities
	include 'config/functions/escape.php';
	include 'config/functions/elapsed-time.php';
	include 'config/functions/time_left.php';
	include 'config/functions/first_character.php';
	// Include actions saved to database
	include 'config/actions/users.php';

	$sql = mysqli_query($con, "SELECT * FROM `users` WHERE `id` = '" . $_SESSION['id'] . "' AND `ban` = '1'");
	$count = mysqli_num_rows($sql);

	if ($count == '1') {
		header("location: logout.php");
	}

?>
<html>

	<head>

		<meta name="viewport" content="width=device-width" />
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/modal.css">
		<link rel="stylesheet" href="assets/css/ico-font.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<title>Time2Change | Project Management Tool</title>

	</head>

	<body>

		<?php

		if (!isset($_SESSION['id'])) {

			$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : false;
			$user_id = isset($_GET['uid']) ? $_GET['uid'] : false;

			if (!empty($redirect) && !empty($user_id)) {

				$sql = "SELECT * FROM `users` WHERE `id` = '" . $user_id . "'";

				if ($result = $con->query($sql)) {

					while ($user = $result->fetch_object()) {

						$_SESSION['id'] = $user->id;

						$sql2 = "UPDATE `users` SET `status` = '1' WHERE `id` = '" . $user->id . "'";
						$con->query($sql2);

						header("location: http://" . $_SERVER['HTTP_HOST'] . "" . $_SERVER['REQUEST_URI'] ."");
						exit();

					}

				}

			} else {

				header("location: login.php");
				exit();

			}

		} else {

			include 'includes/frame/frame.php';

		}

		?>

		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="assets/js/form-validations.js"></script>
		<script src="assets/js/stopwatch.js"></script>

	</body>

</html>

<?php 

if (isset($_SESSION['message'])) {
	unset($_SESSION['message']);
}

?>

<?php
/*
	$current_date = $date;

	$sql2 = mysqli_query($con, "SELECT TIMESTAMPDIFF(SECOND, `started_at`, `ended_at`) as `difference` FROM `calendar_worklog` WHERE DATE(`date`) = '" . $current_date . "' AND `user_id` = '" . $_SESSION['id'] . "'");
	$row2 = mysqli_fetch_array($sql2);

	$hours_worked = $row2['difference'];

	echo '<p style="padding: 0;">Total ' . str_replace(':', 'h', gmdate("H:i", $hours_worked)) . ' min</p>';
*/
?>