<?php

	$subcat = isset($_GET['subcat']) ? $_GET['subcat'] : false;
	$option = isset($_GET['option']) ? $_GET['option'] : false;
	$year = isset($_GET['year']) ? $_GET['year'] : false;
	$month = isset($_GET['month']) ? $_GET['month'] : false;
	$project_id = isset($_GET['project_id']) ? $_GET['project_id'] : false;
	$filter = isset($_GET['filter']) ? $_GET['filter'] : false;
	$display = isset($_GET['display']) ? $_GET['display'] : false;

?>

<?php

	if (isset($_POST['filter-results'])) {

		$_SESSION['project'] = htmlentities($_POST['project']);
		$_SESSION['user'] = htmlentities($_POST['user']);
		$_SESSION['task'] = htmlentities($_POST['task']);
		$_SESSION['label'] = htmlentities($_POST['label']);
		$_SESSION['status'] = htmlentities($_POST['status']);
		$_SESSION['date'] = htmlentities($_POST['date']);
 		$_SESSION['day_1'] = htmlentities($_POST['day_1']);
		$_SESSION['month_1'] = htmlentities($_POST['month_1']);
		$_SESSION['year_1'] = htmlentities($_POST['year_1']);
		$_SESSION['day_2'] = htmlentities($_POST['day_2']);
		$_SESSION['month_2'] = htmlentities($_POST['month_2']);
		$_SESSION['year_2'] = htmlentities($_POST['year_2']);
		$_SESSION['order'] = htmlentities($_POST['order']);
		$display = htmlentities($_POST['display']);

		header("location: index.php?cat=reports&subcat=timings&option=setup&filter=true&display=" . $display . "");
		exit;

	}

?>

<!--

<div class="utilities">

	<a class="success-button left" href="#" data-toggle="modal" data-target="#CreateProject">+ Add a new project</a>

</div>

-->

<?php

if (!$subcat) { 

?>
<h1>Reports</h1>

<p>View reports concerning users and projects.</p>

<div class="section-divider"></div>

<div class="content">

	<div class="sum-item">

		<h2>Projects</h2>

	</div>

	<div class="sum-box">

		<ul>

			<li>

				<div class="image">

					<span class="icon icon-history"></span>

				</div>

				<div class="caption">

					<p><a href="index.php?cat=reports&subcat=timings">Timings</a></p>
					<p class="more">Browse through every monthly timings report. You can browse through the projects and create a printable in no time.</p>

				</div>

			</li>
			<li class="divider"></li>
			<li>

				<div class="image">

					<span class="icon icon-file-empty"></span>

				</div>

				<div class="caption">

					<p><a href="index.php?cat=reports&subcat=timings&option=setup">Create new report</a></p>
					<p class="more">Create a new report based on your chosen criteria.</p>

				</div>

			</li>

		</ul>

	</div>

	<div class="sum-item">

		<h2>Users</h2>

	</div>

	<div class="sum-box">

		<ul>

			<li>

				<div class="image">

					<span class="icon icon-user-check"></span>

				</div>

				<div class="caption">
				
					<p><a href="index.php?cat=reports&subcat=attendances">Attendances</a></p>
					<p class="more">View each users his attendences compiled in an archive for each month. A global overview or an individual overview can be printed.</p>

				</div>

			</li>
			<li class="divider"></li>
			<li>

				<div class="image">

					<span class="icon icon-file-empty"></span>

				</div>

				<div class="caption">

					<p><a href="index.php?cat=reports&subcat=attendances&option=setup">Create new report</a></p>
					<p class="more">Create a new report based on your chosen criteria.</p>

				</div>

			</li>

		</ul>

	</div>

</div>

<?php

	} else 

	if ($subcat == 'timings') {

		if (!$option) { ?>

		<div class="special-navigation">

			<div class="browse-calendar">

				<a href="index.php?cat=reports"><button class="btn-default enabled single"><span class="icon icon-reply"></span> Return to reports</button></a>

			</div>

			<div class="choose-user"></div>

		</div>

		<div class="section-divider"></div>

		<?php

			echo '<div class="content">';

			$starting_year = '2015';

			for ($i = $starting_year; $i < (date("Y") + 1); $i++) {

				echo '<div class="sum-item"><h2>' . $i . '</h2></div>';

				if ($i == '2015') {
					$starting_month = '8';
				} else {
					$starting_month = '1';
				}

				if ($i == date("Y")) {
					$current_month = date("n");
				} else {
					$current_month = '13';
				}

				echo '<ul class="tags">';

				for ($i2 = $starting_month; $i2 < $current_month; $i2++) {

					$date = DateTime::createFromFormat("!m", $i2);
					$month = $date->format('F');

					echo '<li><a href="index.php?cat=reports&subcat=timings&option=detail&year=' . $i . '&month=' . sprintf("%02d", $i2) . '">' . $month . '</a></li>';

				}

				echo '</ul>';

			}

			echo '</div>';

		} else

		if ($option == 'setup') { ?>

		<div class="content">

			<div class="filter-form">

				<div class="sum-item">

					<h2 style="margin-top: 20px;">New report</h2>

				</div>

				<div class="sum-box no-hover special-box">

					<form action="" method="POST">

						<div class="form-group">
							<select id="project" name="project">
								<option value="x">Show all projects</option>
								<?php

								$sql = "SELECT * FROM `projects`";

								if ($result = $con->query($sql)) {

									while ($project = $result->fetch_object()) {

										if ($project->id == $_SESSION['project']) {
											$selected = 'selected="selected"';
										} else {
											$selected = '';
										}

										echo '<option value="' . $project->id . '" ' . $selected . '>' . ucfirst($project->name) . '</option>';

									}

								}

								?>
							</select>
						</div>

						<div class="form-group">
							<select id="user" name="user">
								<option value="x">For all users</option>
								<?php

								$sql = "SELECT * FROM `users`";

								if ($result = $con->query($sql)) {

									while ($user = $result->fetch_object()) {

										if ($user->id == $_SESSION['user']) {
											$selected = 'selected="selected"';
										} else {
											$selected = '';
										}

										echo '<option value="' . $user->id . '" ' . $selected . '>' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . '</option>';

									}

								}

								?>
							</select>
						</div>

						<div class="form-group">
							<select id="task" name="task">
								<option value="x">Include timed tasks</option>
							</select>
						</div>

						<div class="form-group">
							<select id="label" name="label">
								<option value="x">With any label</option>
								<?php

								$sql = "SELECT * FROM `labels` WHERE `type` = '2'";

								if ($result = $con->query($sql)) {

									while ($label = $result->fetch_object()) {

										if ($label->id == $_SESSION['label']) {
											$selected = 'selected="selected"';
										} else {
											$selected = '';
										}

										echo '<option value="' . $label->id . '" ' . $selected . '>' . ucfirst($label->name) . '</option>';

									}

								}

								?>
							</select>
						</div>

						<div class="form-group">
							<select id="status" name="status">

								<?php

								switch ($_SESSION['status']) {

									case "x":

										echo '<option value="">And any status</option>';
										echo '<option value="x" selected="selected">Status complete</option>';
										echo '<option value="z">Status incomplete</option>';

										break;

									case "z":

										echo '<option value="">And any status</option>';
										echo '<option value="x">Status complete</option>';
										echo '<option value="z" selected="selected">Status incomplete</option>';
										break;

									default:

										echo '<option value="">And any status</option>';
										echo '<option value="x">Status complete</option>';
										echo '<option value="z">Status incomplete</option>';

								}

								?>
							</select>
						</div>

						<div class="form-group choose-date">
							<select id="date-filter" name="date" onchange="initPeriod();">
								<option value="x">For any date</option>
								<option value="y">During specified period</option>
							</select>

							<div class="form-group period">

								<label for="day_1">From</label>

								<select id="day_1" name="day_1">
									<option value="">-</option>
									<?php

									for ($i = 1; $i < 32; $i++) {

										if ($i == $_SESSION['day_1']) {
											$selected = 'selected="selected"';
										} else {
											$selected = '';
										}

										echo '<option value="' . $i . '" ' . $selected . '>' . sprintf("%02d", $i) . '</option>';
									}

									?>
								</select>

								<select id="month_1" name="month_1">
									<option value="">-</option>
									<option value="01">January</option>
									<option value="02">February</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>

								<select id="year_1" name="year_1">
									<option value="">-</option>
									<?php

									for ($i = 2015; $i < (date("Y") + 1); $i++) {

										if ($i == $_SESSION['year_1']) {
											$selected = 'selected="selected"';
										} else {
											$selected = '';
										}

										echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';

									}

									?>
								</select>

								<label for="day_2">Until</label>

								<select id="day_2" name="day_2">
									<option value="">-</option>
									<?php

									for ($i = 1; $i < 32; $i++) {

										if ($i == $_SESSION['day_2']) {
											$selected = 'selected="selected"';
										} else {
											$selected = '';
										}

										echo '<option value="' . $i . '" ' . $selected . '>' . sprintf("%02d", $i) . '</option>';

									}

									?>
								</select>

								<select id="month_2" name="month_2">
									<option value="">-</option>
									<option value="01">January</option>
									<option value="02">February</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>

								<select id="year_2" name="year_2">
									<option value="">-</option>
									<?php

									for ($i = 2015; $i < (date("Y") + 1); $i++) {

										if ($i == $_SESSION['year_2']) {
											$selected = 'selected="selected"';
										} else {
											$selected = '';
										}

										echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';

									}

									?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<select id="display" name="display">

								<?php

								switch ($display) {
									case 'apart':
										echo '
										<option value="apart" selected="selected">Apart</option>
										<option value="bundeled">Bundeled</option>
										';
										break;

									case 'bundeled':
									echo '
									<option value="apart">Apart</option>
									<option value="bundeled" selected="selected">Bundeled</option>
									';
										break;
									
									default:
									echo '
									<option value="apart">Apart</option>
									<option value="bundeled">Bundeled</option>
									';
										break;
								}

								?>

							</select>

						</div>

						<div class="form-group">

							<select id="order" name="order">

								<?php

								switch ($_SESSION['order']) {
									case 'asc':
										echo '
										<option value="asc" selected="selected">Ascending</option>
										<option value="desc">Descending</option>
										';
										break;

									case 'desc':
									echo '
									<option value="asc">Ascending</option>
									<option value="desc" selected="selected">Descending</option>
									';
										break;
									
									default:
									echo '
									<option value="asc">Ascending</option>
									<option value="desc">Descending</option>
									';
										break;
								}

								?>

							</select>

						</div>

						<div class="form-group" style="float: right;">

							<button class="submit-button-filter" id="filter-results" name="filter-results" style="margin-right: 0;" type="submit">Filter results</button>

						</div>

					</form>

				</div>

			</div>

		</div>

		<?php

		if ($filter == 'true') { 

			if ($display == 'apart') {

			?>

			<div class="content additional-margin">

				<div class="sum-item">

					<h2 style="margin-top: 20px;">Results <?php echo $_SESSION['order']; ?></h2>

				</div>

				<div class="sum-box no-hover">

					<?php

						if ($_SESSION['project'] != 'x') {
							$sql = "SELECT * FROM `projects` WHERE `id` = '" . $_SESSION['project'] . "'";
						} else

						if ($_SESSION['project'] == 'x') {
							$sql = "SELECT * FROM `projects`";
						}

						if ($_SESSION['user'] != 'x') {
							$param_1 = " AND `user_id` = '" . $_SESSION['user'] . "'";
						} else {
							$param_1 = "";
						}

						if ($_SESSION['date'] == 'x') {
							$param_2 = "";
						} else {
							$param_2 = " AND `start_date` >= '" . $_SESSION['year_1'] . "-" . $_SESSION['month_1'] . "-" . $_SESSION['day_1'] . "' AND `start_date` <= '" . $_SESSION['year_2'] . "-" . $_SESSION['month_2'] . "-" . ($_SESSION['day_2'] + 1) . "'";
						}

						if ($_SESSION['order'] == 'asc') {
							$param_3 = " ORDER BY `start_date` ASC";
						} else {
							$param_3 = " ORDER BY `start_date` DESC";
						}

						if ($result = $con->query($sql)) {

							while ($project = $result->fetch_object()) {

							$query3 = "SELECT pt.module_id FROM `users_timings_log` ul INNER JOIN `projects_tasks` pt ON ul.type_id = pt.id WHERE `type` = '0' AND pt.project_id = '" . $project->id . "'" . $param_1 . "" . $param_2 . "" . $param_3 . "";

							$query_count3 = $con->query($query3)->num_rows; ?>

							<div class="single-project">

								<div class="project-heading"><h3 style="margin-bottom: 0;"><?php echo ucfirst($project->name); ?> <span class="filter-count-results"><strong><?php echo $query_count3; ?></strong> Result(s) found</span></h3></div>

								<div class="toggle-this-project" style="margin-top: 10px;">

									<table class="timings" width="100%">

									<?php

									$squl2 = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project->id . "'";

									if ($results2 = $con->query($squl2)) {

										$counts2 = $results2->num_rows;

										if ($counts2 == 0) {

											echo '<p class="info">No modules could be found for this project</p>';

										}

										while ($module = $results2->fetch_object()) { ?>

										<thead>

											<th colspan="3">

												<span style="color: #48a2a1; text-transform: uppercase;"><?php echo ucfirst($module->name); ?></span>

											</th>
											
											<th colspan="3" style="text-align: right;">

												<?php

												$query2 = "SELECT pt.module_id, SEC_TO_TIME(SUM(TIMESTAMPDIFF(SECOND, ul.start_date, ul.end_date))) as `total_hours` FROM `users_timings_log` ul INNER JOIN `projects_tasks` pt ON ul.type_id = pt.id WHERE `type` = '0' AND pt.module_id = '" . $module->id . "'" . $param_1 . "" . $param_2 . " GROUP BY pt.module_id";

												if ($query_result2 = $con->query($query2)) {

													$count_query2 = $query_result2->num_rows;

													if ($count_query2 != '0') {

														$task_total = $query_result2->fetch_object();
														$total_task = $task_total->total_hours;

													} else {

														$total_task = '00:00:00';

													}

												}

												?>

												<span class="module-total-label">Subtotal: <span style="margin-left: 20px;"><?php echo substr($total_task, 0, -3); ?></span></span>

											</th> 

										</thead>

										<?php

											$sql3 = "SELECT * FROM `projects_tasks` WHERE `module_id` = '" . $module->id . "'";

											if ($result3 = $con->query($sql3)) {

												while ($task = $result3->fetch_object()) { ?>

												<thead class="task">

													<th colspan="3" style="padding-left: 25px;">

														<?php echo ucfirst($task->name); ?>

													</th>

													<th colspan="3" style="text-align: right;">

														<span class="task-estimated-time" data-toggle="tooltip" data-placement="top" title="Estimated"><?php echo $task->estimated_time; ?></span>

														<?php

															$subquery = "SELECT SUM(TIMESTAMPDIFF(SECOND, `start_date`, `end_date`)) as `effective_time` FROM `users_timings_log` WHERE `type_id` = '" . $task->id . "' AND `type` = '0'" . $param_1 . "" . $param_2 . "";

															if ($subquery_result = $con->query($subquery)) {

																while ($effective_time = $subquery_result->fetch_object()) {

																	$total_time = $effective_time->effective_time;

																}

															}

														?>

														<span style="color: #000; font-size:12px; font-weight: normal; text-transform: lowercase;"> - </span>

														<span class="task-total-label" data-toggle="tooltip" data-placement="top" title="Effective"><?php echo convert_to_time($total_time); ?></span>

														<span style="color: #000; font-size:12px; font-weight: normal; text-transform: lowercase;"> = </span>

														<?php

															$subquery = "SELECT SUM(TIMESTAMPDIFF(SECOND, `start_date`, `end_date`)) as `effective_time` FROM `users_timings_log` WHERE `type_id` = '" . $task->id . "' AND `type` = '0'" . $param_1 . "" . $param_2 . "";

															if ($subquery_result = $con->query($subquery)) {

																while ($effective_time = $subquery_result->fetch_object()) {

																	$total_time = $effective_time->effective_time;

																}

															}

															$total_estimated = explode(":", $task->estimated_time);

															$total_estimated = ($total_estimated[0] * 3600) + ($total_estimated[1] * 60);

															$balance = ($total_estimated - $total_time);

															echo absolute_value($balance);
															
														?>

													</th>

												</thead>

												<?php

													$sql4 = "SELECT * FROM `users_timings_log` WHERE `type` = '0' AND `type_id` = '" . $task->id . "'" . $param_1 . "" . $param_2 . " ORDER BY `start_date` ASC";

													if ($result4 = $con->query($sql4)) {

														while ($task_log = $result4->fetch_object()) { ?>

														<tbody class="module-times">

															<tr>

																<td class="times indent2" colspan="2" width="40%">

																	<?php echo date("l - d/m/Y", strtotime($task_log->start_date)); ?>

																</td>

																<td class="times" width="39%">

																	<?php

																	$query = "SELECT * FROM `users` WHERE `id` = '" . $task_log->user_id . "'";

																	if ($query_result = $con->query($query)) {

																		while ($user = $query_result->fetch_object()) {

																			echo ucfirst($user->name) . ' ' . ucfirst($user->surname);

																		}

																	}

																	?>

																</td>

																<td align="right" class="times" width="7%">

																	<?php echo date("H:i", strtotime($task_log->start_date)); ?>

																</td>

																<td align="right" class="times" width="7%">

																	<?php echo date("H:i", strtotime($task_log->end_date)); ?>

																</td>

																<td align="right" class="times" width="7%">

																	<?php

																	$query = "SELECT TIMESTAMPDIFF(SECOND, `start_date`, `end_date`) as `difference` FROM `users_timings_log` WHERE `id` = '" . $task_log->id . "'" . $param_2 . "";

																	if ($query_result = $con->query($query)) {

																		while ($log = $query_result->fetch_object()) {

																			echo '<span class="log-label">' . gmdate("H:i", $log->difference) . '</span>';

																		}

																	}

																	?>

																</td>

															</tr>

														</tbody>

														<?php

														}

													}

												}

											}

										}

									}

									?>

									</table>

								</div>

							</div>

							<?php

							}

						}

					?>

				</div>

			</div>

			<?php

			} else

			if ($display == 'bundeled') { ?>

				<div class="content additional-margin">

					<div class="sum-item">

						<h2 style="margin-top: 20px;">Results</h2>

					</div>

					<div class="sum-box no-hover">

						<div class="single-project">

							<table width="100%">

							<?php

							if ($_SESSION['user'] != 'x') {
								$param_1 = " AND `user_id` = '" . $_SESSION['user'] . "'";
							} else {
								$param_1 = "";
							}

							if ($_SESSION['date'] == 'x') {
								$param_2 = "";
							} else {
								$param_2 = " AND `start_date` >= '" . $_SESSION['year_1'] . "-" . $_SESSION['month_1'] . "-" . $_SESSION['day_1'] . "' AND `start_date` <= '" . $_SESSION['year_2'] . "-" . $_SESSION['month_2'] . "-" . ($_SESSION['day_2'] + 1) . "'";
							}

							if ($_SESSION['order'] == 'asc') {
								$param_3 = " ORDER BY `start_date` ASC";
							} else {
								$param_3 = " ORDER BY `start_date` DESC";
							}

							$sql5 = "SELECT *, ul.description as description2 FROM `users_timings_log` ul INNER JOIN `projects_tasks` pt ON ul.type_id = pt.id WHERE `type` = '0'" . $param_1 . "" . $param_2 . "" . $param_3 . "";

								if ($result5 = $con->query($sql5)) {

									while ($log = $result5->fetch_object()) { 

									if ($_SESSION['project'] != 'x') {
									$sql = "SELECT * FROM `projects` WHERE `id` = '" . $log->project_id . "' LIMIT 1";
									} else

									if ($_SESSION['project'] == 'x') {
										$sql = "SELECT * FROM `projects` WHERE `id` = '" . $log->project_id . "'";
									}

									?>

									<tr>

										<td width="15%"><?php echo date("l - d/m/Y", strtotime($log->start_date)); ?></td>
										<td width="20%">

											<?php

											if ($result = $con->query($sql)) {

												while ($project = $result->fetch_object()) {

													echo '<strong>' . ucfirst($project->name) . '</strong>';

												}

											}

											?>

										</td>
										<td>

											<?php echo ucfirst($log->description2); ?>

										</td>
										<td>

											<?php

											$sql7 = "SELECT * FROM `users` WHERE `id` = '" . $log->user_id . "'";

											if ($result7 = $con->query($sql7)) {

												while ($user = $result7->fetch_object()) {

													echo ucfirst($user->name) . ' ' . ucfirst($user->surname); 

												}

											}

											?>

										</td>
										<td><?php echo date("H:i", strtotime($log->start_date)); ?></td>
										<td><?php echo date("H:i", strtotime($log->end_date)); ?></td>

									</tr>

									<?php
									}

								}

							?>

							</table>

						</div>

					</div>

				</div>

				<?php

			}

		}

		} else

		if ($option == 'detail') { ?>

		<div class="special-navigation">

			<div class="browse-calendar">

				<a href="javascript:history.go(-1)"><button class="btn-default enabled single"><span class="icon icon-reply"></span> Return to archive</button></a>

			</div>

			<div class="choose-user"></div>

		</div>

		<div class="section-divider"></div>

			<div class="content additional-margin">

			<div class="sum-item">

				<h2>Timings - <?php $monthname = DateTime::createFromFormat('!m', $month); echo $monthname->format('F'); ?>, <?php echo $year; ?></h2>

			</div>

			<div class="sum-box no-hover">

				<?php

				$begin_date = $year . '-'. sprintf("%02d", $month) . '-01';
				$end_date2 = $year . '-' . sprintf("%02d", $month) . '-32';

				$monthly_param = " AND `start_date` >= '" . $begin_date . "' AND `start_date` <= '" . $end_date2 . "'";

					$sql = "SELECT * FROM `projects`";

					if ($result = $con->query($sql)) {

						while ($project = $result->fetch_object()) {

						$query3 = "SELECT pt.module_id FROM `users_timings_log` ul INNER JOIN `projects_tasks` pt ON ul.type_id = pt.id WHERE `type` = '0' AND pt.project_id = '" . $project->id . "'" . $monthly_param . "";

						$query_count3 = $con->query($query3)->num_rows; ?>

						<div class="single-project">

							<div class="project-heading"><h3 style="margin-bottom: 0;"><?php echo ucfirst($project->name); ?> <span class="filter-count-results"><strong><?php echo $query_count3; ?></strong> Result(s) found</span></h3></div>

							<div class="toggle-this-project" style="margin-top: 10px;">

								<table class="timings" width="100%">

								<?php

								$squl2 = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project->id . "'";

								if ($results2 = $con->query($squl2)) {

									$counts2 = $results2->num_rows;

									if ($counts2 == 0) {

										echo '<p class="info">No modules could be found for this project</p>';

									}

									while ($module = $results2->fetch_object()) { ?>

									<thead>

										<th colspan="3">

											<span style="color: #48a2a1; text-transform: uppercase;"><?php echo ucfirst($module->name); ?></span>

										</th>
										
										<th colspan="3" style="text-align: right;">

											<?php

											$query2 = "SELECT pt.module_id, SEC_TO_TIME(SUM(TIMESTAMPDIFF(SECOND, ul.start_date, ul.end_date))) as `total_hours` FROM `users_timings_log` ul INNER JOIN `projects_tasks` pt ON ul.type_id = pt.id WHERE `type` = '0' AND pt.module_id = '" . $module->id . "'" . $monthly_param . " GROUP BY pt.module_id";

											if ($query_result2 = $con->query($query2)) {

												$count_query2 = $query_result2->num_rows;

												if ($count_query2 != '0') {

													$task_total = $query_result2->fetch_object();
													$total_task = $task_total->total_hours;

												} else {

													$total_task = '00:00:00';

												}

											}

											?>

											<span class="module-total-label">Subtotal: <span style="margin-left: 20px;"><?php echo substr($total_task, 0, -3); ?></span></span>

										</th> 

									</thead>

									<?php

										$sql3 = "SELECT * FROM `projects_tasks` WHERE `module_id` = '" . $module->id . "'";

										if ($result3 = $con->query($sql3)) {

											while ($task = $result3->fetch_object()) { ?>

											<thead class="task">

												<th colspan="3" style="padding-left: 25px;">

													<?php echo ucfirst($task->name); ?>

												</th>

												<th colspan="3" style="text-align: right;">

													<span class="task-estimated-time" data-toggle="tooltip" data-placement="top" title="Estimated"><?php echo $task->estimated_time; ?></span>

													<?php

														$subquery = "SELECT SUM(TIMESTAMPDIFF(SECOND, `start_date`, `end_date`)) as `effective_time` FROM `users_timings_log` WHERE `type_id` = '" . $task->id . "' AND `type` = '0'" . $monthly_param . "";

														if ($subquery_result = $con->query($subquery)) {

															while ($effective_time = $subquery_result->fetch_object()) {

																$total_time = $effective_time->effective_time;

															}

														}

													?>

													<span style="color: #000; font-size:12px; font-weight: normal; text-transform: lowercase;"> - </span>

													<span class="task-total-label" data-toggle="tooltip" data-placement="top" title="Effective"><?php echo convert_to_time($total_time); ?></span>

													<span style="color: #000; font-size:12px; font-weight: normal; text-transform: lowercase;"> = </span>

													<?php

														$subquery = "SELECT SUM(TIMESTAMPDIFF(SECOND, `start_date`, `end_date`)) as `effective_time` FROM `users_timings_log` WHERE `type_id` = '" . $task->id . "' AND `type` = '0'" . $monthly_param . "";

														if ($subquery_result = $con->query($subquery)) {

															while ($effective_time = $subquery_result->fetch_object()) {

																$total_time = $effective_time->effective_time;

															}

														}

														$total_estimated = explode(":", $task->estimated_time);

														$total_estimated = ($total_estimated[0] * 3600) + ($total_estimated[1] * 60);

														$balance = ($total_estimated - $total_time);

														echo absolute_value($balance);
														
													?>

												</th>

											</thead>

											<?php

												$sql4 = "SELECT * FROM `users_timings_log` WHERE `type` = '0' AND `type_id` = '" . $task->id . "'" . $monthly_param . " ORDER BY `start_date` ASC";

												if ($result4 = $con->query($sql4)) {

													while ($task_log = $result4->fetch_object()) { ?>

													<tbody class="module-times">

														<tr>

															<td class="times indent2" colspan="2" width="40%">

																<?php echo date("l - d/m/Y", strtotime($task_log->start_date)); ?>

															</td>

															<td class="times" width="39%">

																<?php

																$query = "SELECT * FROM `users` WHERE `id` = '" . $task_log->user_id . "'";

																if ($query_result = $con->query($query)) {

																	while ($user = $query_result->fetch_object()) {

																		echo ucfirst($user->name) . ' ' . ucfirst($user->surname);

																	}

																}

																?>

															</td>

															<td align="right" class="times" width="7%">

																<?php echo date("H:i", strtotime($task_log->start_date)); ?>

															</td>

															<td align="right" class="times" width="7%">

																<?php echo date("H:i", strtotime($task_log->end_date)); ?>

															</td>

															<td align="right" class="times" width="7%">

																<?php

																$query = "SELECT TIMESTAMPDIFF(SECOND, `start_date`, `end_date`) as `difference` FROM `users_timings_log` WHERE `id` = '" . $task_log->id . "'" . $monthly_param . "";

																if ($query_result = $con->query($query)) {

																	while ($log = $query_result->fetch_object()) {

																		echo '<span class="log-label">' . gmdate("H:i", $log->difference) . '</span>';

																	}

																}

																?>

															</td>

														</tr>

													</tbody>

													<?php

													}

												}

											}

										}

									}

								}

								?>

								</table>

							</div>

						</div>

						<?php

						}

					}

				?>

			</div>

		</div>

		<?php

		}

	} else

	if ($subcat == 'attendances') {

		if (!$option) { ?>

		<div class="special-navigation">

			<div class="browse-calendar">

				<a href="index.php?cat=reports"><button class="btn-default enabled single"><span class="icon icon-reply"></span> Return to reports</button></a>

			</div>

			<div class="choose-user"></div>

		</div>

		<div class="section-divider"></div>

		<?php

			echo '<div class="content">';

			$starting_year = '2015';

			for ($i = $starting_year; $i < (date("Y") + 1); $i++) {

				echo '<div class="sum-item"><h2>' . $i . '</h2></div>';

				if ($i == '2015') {
					$starting_month = '8';
				} else {
					$starting_month = '1';
				}

				if ($i == date("Y")) {
					$current_month = date("n");
				} else {
					$current_month = '13';
				}

				echo '<ul class="tags">';

				for ($i2 = $starting_month; $i2 < $current_month; $i2++) {

					$date = DateTime::createFromFormat("!m", $i2);
					$month = $date->format('F');

					echo '<li><a href="index.php?cat=reports&subcat=attendances&option=detail&year=' . $i . '&month=' . sprintf("%02d", $i2) . '">' . $month . '</a></li>';

				}

				echo '</ul>';

			}

			echo '</div>';

		} else

		if ($option == 'detail') { ?>

			<div class="special-navigation">

			<div class="browse-calendar">

				<a href="javascript:history.go(-1)"><button class="btn-default enabled single"><span class="icon icon-reply"></span> Return to archive</button></a>

			</div>

			<div class="choose-user"></div>

		</div>

		<div class="section-divider"></div>

			<div class="content additional-margin">

			<div class="sum-item">

				<h2>Attendances - <?php $monthname = DateTime::createFromFormat('!m', $month); echo $monthname->format('F'); ?>, <?php echo $year; ?></h2>

			</div>

			<div class="sum-box no-hover">

				<?php

				$begin_date = $year . '-'. sprintf("%02d", $month) . '-01';
				$end_date2 = $year . '-' . sprintf("%02d", $month) . '-32';

				$monthly_param = " AND `start_date` >= '" . $begin_date . "' AND `start_date` <= '" . $end_date2 . "'";

					$sql = "SELECT * FROM `users`";

					if ($result = $con->query($sql)) {

						while ($user = $result->fetch_object()) { ?>

							<div class="single-project">

							<div class="project-heading"><h3 style="margin-bottom: 0;"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></h3></div>

								<div class="toggle-this-project" style="margin-top: 10px;">

									<table width="100%">

										<thead>

											<th>Day</th>
											<th>Date</th>
											<th>Inputs</th>
											<th></th>
											<th style="text-align: center;">AM</th>
											<th>First activity</th>
											<th style="text-align: center;">PM</th>
											<th>Last activity</th>
											<th>Total Hours</th>

										</thead>

										<tbody>

										<?php

										$days_in_month = cal_days_in_month(CAL_GREGORIAN, sprintf("%02d", $month), $year);

										for ($i = 1; $i < ($days_in_month + 1); $i++) { ?>

										<?php $date = sprintf("%02d", $i) . '-' . sprintf("%02d", $month). '-' . $year; ?>
										<?php $date2 = $year . '-' . sprintf("%02d", $month) . '-' . sprintf("%02d", $i); ?>

										<?php

										$class = '';

										if (date("D", strtotime($date2)) == 'Sun' || date("D", strtotime($date2)) == 'Sat') {

											$class = 'weekend';

										} else {

											$class = 'midweek';

										}

										?>

										<tr class="<?php echo $class; ?>">

											<td width="10%">

												<?php echo date("l", strtotime($date)); ?>

											</td>

											<td width="10%">

												<?php echo $date; ?>

											</td>

											<td align="center" width="5%">

												<?php

												$sql2 = "SELECT * FROM `calendar_worklog` WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";

												echo $con->query($sql2)->num_rows;

												?>

											</td>

											<td width="2%">

												<?php

												$sql2 = "SELECT * FROM `calendar_remotework` WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";
												$sql3 = "SELECT * FROM `calendar_sickness` WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";
												$sql4 = "SELECT * FROM `calendar_holliday` WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";

												if ($con->query($sql2)->num_rows > 0) {

													echo '<span class="icon icon-home"></span>';

												} else

												if ($con->query($sql3)->num_rows > 0) {

													echo '<span class="icon icon-sad2"></span>';

												} else 

												if ($con->query($sql4)->num_rows > 0) {

													echo '<span class="icon icon-airplane"></span>';

												}

												?>

											</td>

											<td align="center">

												<?php

												$sql2 = "SELECT * FROM `calendar_worklog` WHERE `user_id` = '" . $user->id . "' AND (`started_at` BETWEEN '" . $date2 . " 00:00:00' AND '" . $date2 . " 12:00:00')";

												$count2 = $con->query($sql2)->num_rows;
												$result2 = $con->query($sql2)->fetch_object();

												if ($count2 > 0) {

													echo '<span class="icon icon-checkmark green"></span>';

												} else {

													$sql3 = "SELECT * FROM `calendar_sickness` WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";
													$sql4 = "SELECT * FROM `calendar_holliday` WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";

													if ($con->query($sql3)->num_rows > 0) {

														echo '<span class="icon icon-blocked red"></span>';

													} else

													if ($con->query($sql4)->num_rows > 0) {

														echo '<span class="icon icon-blocked red"></span>';

													} else {

														echo '<span class="icon icon-cross red"></span>';

													}

												}

												?>

											</td>

											<td>

												<?php

												if ($count2 > 0) {

													$sql3 = "SELECT * FROM `calendar_worklog` WHERE `user_id` = '" . $user->id . "' AND (`started_at` BETWEEN '" . $date2 . " 00:00:00' AND '" . $date2 . " 12:00:00') LIMIT 1";

													$result3 = $con->query($sql3)->fetch_object();

													echo date("H:i", strtotime($result3->started_at));

												} else {

													echo '-';

												}

												?>

											</td>

											<td align="center">

												<?php

												$sql2 = "SELECT * FROM `calendar_worklog` WHERE `user_id` = '" . $user->id . "' AND (`ended_at` BETWEEN '" . $date2 . " 12:00:01' AND '" . $date2 . " 23:59:59')";

												$count2 = $con->query($sql2)->num_rows;
												$result2 = $con->query($sql2)->fetch_object();

												if ($count2 > 0) {

													echo '<span class="icon icon-checkmark green"></span>';

												} else {

													$sql3 = "SELECT * FROM `calendar_sickness` WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";
													$sql4 = "SELECT * FROM `calendar_holliday` WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";

													if ($con->query($sql3)->num_rows > 0) {

														echo '<span class="icon icon-blocked red"></span>';

													} else

													if ($con->query($sql4)->num_rows > 0) {

														echo '<span class="icon icon-blocked red"></span>';

													} else {

														echo '<span class="icon icon-cross red"></span>';

													}

												}

												?>

											</td>

											<td>

												<?php

												if ($count2 > 0) {

													$sql3 = "SELECT * FROM `calendar_worklog` WHERE `user_id` = '" . $user->id . "' AND (`ended_at` BETWEEN '" . $date2 . " 12:00:01' AND '" . $date2 . " 23:59:59') ORDER BY `id` DESC LIMIT 1";

													$result3 = $con->query($sql3)->fetch_object();

													echo date("H:i", strtotime($result3->ended_at));

												} else {

													echo '-';

												}

												?>

											</td>

											<td>

												<?php

												$query2 = "SELECT SEC_TO_TIME(SUM(TIMESTAMPDIFF(SECOND, cw.started_at, cw.ended_at))) as `total_hours` FROM `calendar_worklog` cw WHERE `user_id` = '" . $user->id . "' AND `date` = '" . $date2 . "'";

												if ($query_result2 = $con->query($query2)) {

													$count_query2 = $query_result2->num_rows;

													if ($count_query2 != 0) {

														$task_total = $query_result2->fetch_object();
														$total_task = $task_total->total_hours;

													} else {

														$total_task = '00:00:00';

													}

												}

												?>

												<?php echo substr($total_task, 0, -3); ?>

											</td>

										</tr>

										<?php

										}

										?>

										</tbody>

									</table>

								</div>

							</div>

						<?php

						}

					}

				?>

			</div>

		</div>

		<?php

		}

	}

?>