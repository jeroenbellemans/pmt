<h1>My Work</h1>

<p>An overview of all your tasks, waiting to be completed</p>

<div class="section-divider"></div>

<div class="content">

	<div class="frame">

		<div class="project-content">

			<div class="project-references my-work">

			<?php

			$sql = "SELECT * FROM `projects_tasks` WHERE `label` != '6' AND `responsible` = '" . $_SESSION['id'] . "' ORDER BY `deadline` = '0000-00-00 00:00:00', `deadline` ASC, `created_at` ASC";

			if ($result = $con->query($sql)) {

				$count = $result->num_rows;

				if ($count == 0) { ?>

					<div class="nothing">

						<div class="image">

							<span class="icon icon-bullhorn"></span>

						</div>

						<div class="message">

							<p>Currently you have no tasks left to do</p>

						</div>

					</div>

					<?php

				} else { ?>

				<div class="module-heading"><h3>Open Tasks</h3></div>

				<div class="task-container">

				<?php

					while ($task = $result->fetch_object()) { 

						if (($task->responsible == $_SESSION['id']) || ($task->created_by == $_SESSION['id']) || ($_SESSION['admin'] === true)) { 

							$edit = '<span class="icon icon-pencil edit-task" data-project="' . $task->project_id . '" data-type="1" data-id="' . $task->id . '" data-toggle="tooltip" data-placement="top" title="Edit"></span>';
							$delete = '<span class="icon icon-bin delete-task" data-project="' . $task->project_id . '" data-type="1" data-id="' . $task->id . '" data-toggle="tooltip" data-placement="top" title="Delete"></span>';
							$timing = '<span class="icon icon-clock custom-time" data-project="' . $task->project_id . '" data-type="1" data-id="' . $task->id . '" data-toggle="tooltip" data-placement="top" title="Add custom time"></span>';

						} else {

							$edit = '';
							$delete = '';
							$timing = '';

						}

						if ($task->important == '1') {
							$flag = 'subtile-highlight';
						} else {
							$flag = '';
						}

					?>

					<div class="holder">

						<div class="<?php echo define_priority($task->deadline); ?>">&nbsp;</div>

						<div class="task-name">

							<a href="#" class="view-task" data-id="<?php echo $task->id; ?>" data-project="<?php echo $task->project_id; ?>"><?php echo '#' . $task->id; ?> <?php if (strlen($task->name) > 30) { echo ucfirst(substr($task->name, 0, 30)). '...'; } else { echo ucfirst($task->name); } ?></a> <span class="icons"><?php echo $timing; ?> <?php echo $edit; ?></span>

						</div>

						<div class="task-category">

							<?php

							$sql3 = "SELECT * FROM `projects_categories` WHERE `id` = '" . $task->category . "'";

							if ($result3 = $con->query($sql3)) {

								while ($category = $result3->fetch_object()) {

									echo '<strong>' . ucfirst($category->name) .'</strong>';

								}

							}

							?>

						</div>

						<div class="task-deadline">

							<?php echo time_left($task->deadline); ?>

						</div>

						<div class="task-label">

							<?php

							$sql4 = "SELECT * FROM `labels` WHERE `id` = '" . $task->label . "'";

							if ($result4 = $con->query($sql4)) {

								while ($label = $result4->fetch_object()) {

									echo '<span class="' . str_replace(' ', '-', $label->name) . '">' . ucfirst($label->name) . '</span>';

								}

							}

							?>

						</div>

						<div class="task-responsible another-width">

							<?php

							$sql6 = "SELECT * FROM `projects` WHERE `id` = '" . $task->project_id . "'";

							if ($result6 = $con->query($sql6)) {

								while ($project = $result6->fetch_object()) {

									echo '<strong>' . ucfirst($project->name) . '</strong>';

								}

							}

							?>

						</div>

					</div>

						<?php

						} 

					}

				}		

				?>

				</div>

				<?php

				$sql = "SELECT * FROM `projects_bugs` WHERE `status` != '6' AND `responsible` = '" . $_SESSION['id'] . "' ORDER BY `deadline` = '0000-00-00 00:00:00', `deadline` ASC, `created_at` ASC";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					if ($count == 0) { ?>

						<div class="nothing">

							<div class="image">

								<span class="icon icon-bullhorn"></span>

							</div>

							<div class="message">

								<p>Currently you have no bugs left to do</p>

							</div>

						</div>

					<?php

					} else { ?>

					<div class="module-heading"><h3>Open Bugs</h3></div>

					<div class="task-container">

					<?php

					$timing = '';
					$delete = '';
					$edit = '';

						while ($bug = $result->fetch_object()) { ?>

						<div class="holder">

							<div class="<?php echo define_priority($bug->deadline); ?>">&nbsp;</div>

							<div class="task-name">

								<a href="#" class="view-task" data-id="<?php echo $bug->id; ?>" data-project="<?php echo $bug->project_id; ?>"><?php echo '#' . $bug->id; ?> <?php if (strlen($bug->name) > 30) { echo ucfirst(substr($bug->name, 0, 30)). '...'; } else { echo ucfirst($bug->name); } ?></a> <span class="icons"><?php echo $timing; ?> <?php echo $edit; ?> <?php echo $delete; ?></span>

							</div>

							<div class="task-category">

								<strong><?php if ($bug->category == '') { echo '-'; } else { echo $bug->category; } ?></strong>

							</div>

							<div class="task-deadline">

								<?php echo time_left($bug->deadline); ?>

							</div>

							<div class="task-label">

								<?php

								$sql4 = "SELECT * FROM `labels` WHERE `id` = '" . $bug->status . "'";

								if ($result4 = $con->query($sql4)) {

									while ($label = $result4->fetch_object()) {

										echo '<span class="' . str_replace(' ', '-', $label->name) . '">' . ucfirst($label->name) . '</span>';

									}

								}

								?>

							</div>

							<div class="task-responsible another-width">

								<?php

								$sql6 = "SELECT * FROM `projects` WHERE `id` = '" . $bug->project_id . "'";

								if ($result6 = $con->query($sql6)) {

									while ($project = $result6->fetch_object()) {

										echo '<strong>' . ucfirst($project->name) . '</strong>';

									}

								}

								?>

							</div>

						</div>

							<?php

							} 

						}

					}		

					?>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<!-- View Bug -->

<div class="modal fade" id="ViewBug" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Bug details</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="view-bug-body"></div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="">
				<input type="hidden" class="bug-id" value="">
				<button type="submit" name="save-bug" class="submit-button save-bug" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- View Task -->

<div class="modal fade" id="ViewTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Task details</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="view-task-body"></div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="">
				<input type="hidden" class="task-id" value="">
				<button type="submit" name="save-task" class="submit-button save-task" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Edit Task -->

<div class="modal fade" id="EditTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Task</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="edit-task-body"></div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="">
				<input type="hidden" class="task-id" value="">
				<button type="submit" name="save-edit-task" class="submit-button save-edit-task" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Custom Timing -->

<div class="modal fade" id="CustomTiming" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Custom Timing</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="when">When did you work on this task</label>
							<input type="text" name="when" class="when calendar" id="when" value="<?php echo date("d-m-Y"); ?>">

						</div>

					</div>

					<div class="half" style="clear: both;">

						<div class="form-group">

							<label for="estimated2">I started around</label>
							<select id="estimated2" class="estimated_hour2" name="estimated_hour2">
								<?php

								for ($i = 0; $i < 24; $i++) {

									echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

								}

								?>
							</select>

							<select class="estimated_minute2" name="estimated_minute2">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="estimated4">I ended around</label>
							<select id="estimated4" class="estimated_hour4" name="estimated_hour4">
								<?php

								for ($i = 0; $i < 24; $i++) {

									echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

								}

								?>
							</select>

							<select class="estimated_minute4" name="estimated_minute4">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

						</div>

					</div>

					<!-- Custom logs -->

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="">
				<input type="hidden" class="task-id" value="">
				<button type="submit" name="save-custom-timing" class="submit-button save-custom-timing" href="#">Save</button>

			</div>

		</div>

	</div>

</div>