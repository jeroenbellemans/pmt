<?php

	if (isset($_SESSION['message'])) {

		if ($_SESSION['message'] == 'project_created') {
			$success = '<p class="success">Project created</p>';
		} else
		if ($_SESSION['message'] == 'module_created') {
			$success = '<p class="success">Module created</p>';
		} else
		if ($_SESSION['message'] == 'task_created') {
			$success = '<p class="success">Task created</p>';
		} else
		if ($_SESSION['message'] == 'bug_created') {
			$success = '<p class="success">Bug created</p>';
		}

	} else {

		$success = '';

	}

	$project_id = isset($_GET['pid']) ? $_GET['pid'] : false;
	$p_response = '';
	$m_response = '';
	$t_response = '';
	$b_response = '';

	if (isset($_POST['create-project'])) {

		$name = htmlentities($_POST['name']);
		$responsible = htmlentities($_POST['responsible']);
		$deadline = date("Y-m-d", strtotime($_POST['deadline']));
		$label = htmlentities($_POST['label']);
		$customer = htmlentities($_POST['customer']);
		$channel = htmlentities($_POST['channel']);
		$description = mysqli_real_escape_string($con, $_POST['description']);
		$private = isset($_POST['private']) ? 1 : 0;

		if ($deadline == '1970-01-01') {
			$deadline = '0000-00-00';
		} else {
			$deadline = $deadline . ' 23:59:59';
		}

		if (empty($name)) {
			$p_response = '<span style="color: #a70000; font-weight: bold;">Enter the project name</span>';
		} else
		if (empty($responsible)) {
			$p_response = '<span style="color: #a70000; font-weight: bold;">Select a responsible</span>';
		} else
		if ($deadline < date("Y-m-d") . ' 23:59:59') {
			$p_response = '<span style="color: #a70000; font-weight: bold;">Select valid deadline</span>';
		} else
		if (empty($label)) {
			$p_response = '<span style="color: #a70000; font-weight: bold;">Select a label</span>';
		} else
		if (empty($customer)) {
			$p_response = '<span style="color: #a70000; font-weight: bold;">Enter the customer</span>';
		} else
		if (empty($channel)) {
			$p_response = '<span style="color: #a70000; font-weight: bold;">Select a channel</span>';
		} else {

			$sql = "INSERT INTO `projects` (`name`, `responsible`, `deadline`, `private`, `label`, `customer`, `channel`, `description`, `created_at`, `created_by`) VALUES ('" . $name . "', '" . $responsible . "', '" . $deadline . "', '" . $private . "', '" . $label . "', '" . $customer . "', '" . $channel . "', '" . $description . "', now(), '" . $_SESSION['id'] . "')";
			$con->query($sql);

			$inserted_project_id = mysqli_insert_id($con);

			$sql2 = "INSERT INTO `projects_users` (`project_id`, `user_id`) VALUES ('" . $inserted_project_id . "', '" . $responsible . "')";
			$con->query($sql2);

			$_SESSION['message'] = 'project_created';

			header("location: " . $_SERVER['REQUEST_URI'] . "");
			exit();
		}

	} else

	if (isset($_POST['create-module'])) {

		$project = htmlentities($_POST['project']);
		$name = htmlentities($_POST['name']);

		if (empty($name)) {
			$m_response = '<span style="color: #a70000; font-weight: bold;">Enter the module name</span>';
		} else
		if (empty($project)) {
			$m_response = '<span style="color: #a70000; font-weight: bold;">Select a project</span>';
		} else
		if (!empty($name) && !empty($project)) {

			$sql = "INSERT INTO `projects_modules` (`name`, `project_id`, `created_by`) VALUES ('" . $name . "', '" . $project . "', '" . $_SESSION['id'] . "')";
			$con->query($sql);

			$_SESSION['message'] = 'module_created';

			header("location: " . $_SERVER['REQUEST_URI'] . "");
			exit();
		}

	} else

	if (isset($_POST['create-bug'])) {

		$project = htmlentities($_POST['project-id']);
		$name = htmlentities($_POST['bugname']);
		$responsible = htmlentities($_POST['responsible5']);
		$deadline = date("Y-m-d", strtotime($_POST['deadline5']));
		$category = htmlentities($_POST['category2']);
		$path = htmlentities($_POST['path']);
		$description = mysqli_real_escape_string($con, $_POST['description5']);
		$files = isset($_FILES['choose_file']) ? $_FILES['choose_file'] : false;
		$_FILES['choose_file']['tmp_name'][0] = isset($_FILES['choose_file']['tmp_name'][0]) ? $_FILES['choose_file']['tmp_name'][0] : false;
		$important = isset($_POST['important']) ? 1 : 0;

		if ($deadline == '1970-01-01') {
			$deadline == '0000-00-00';
		}

		$created = date("d-m-Y, H:i");

		if (empty($name)) {
			$b_response = '<span style="color: #a70000; font-weight: bold;">Enter the bug name</span>';
		} else
		if (empty($responsible)) { 
			$b_response = '<span style="color: #a70000; font-weight: bold;">Select a responsible</span>';
		} else
		if (empty($description)) {
			$b_response = '<span style="color: #a70000; font-weight: bold;">Enter a detailed description</span>';
		} else {

			$sql = "INSERT INTO `projects_bugs` (`name`, `path`, `deadline`, `category`, `description`, `important`, `responsible`, `project_id`, `status`, `created_by`, `created_at`) VALUES ('" . $name . "', '" . $path . "', '" . $deadline . "', '" . $category . "', '" . $description . "', '" . $important . "', '" . $responsible . "', '" . $project . "', '5', '" . $_SESSION['id'] . "', now() )";
			$con->query($sql);

			$inserted_id = mysqli_insert_id($con);

			$sql7 = "INSERT INTO `projects_bugs_log` (`type`, `sender`, `receiver`, `bug_id`, `date`) VALUES ('0', '" . $_SESSION['id'] . "', '" . $responsible . "', '" . $inserted_id . "', now())";
			$con->query($sql7);

			$sql3 = "SELECT * FROM `users` WHERE `id` = '" . $responsible . "'";

			if ($result3 = $con->query($sql3)) {

				while ($user = $result3->fetch_object()) {

					$sql5 = "SELECT * FROM `users` WHERE `id` = '" . $_SESSION['id'] . "'";

					if ($result5 = $con->query($sql5)) {

						while ($assigner = $result5->fetch_object()) {

							$sql6 = "SELECT * FROM `projects` WHERE `id` = '" . $project . "'";

							if ($result6 = $con->query($sql6)) {

								while ($project2 = $result6->fetch_object()) {

									$description = nl2br($description);

									$to = $user->email;
									$from = "pmt@time2change.com";
									$subject = "#" . $inserted_id . " - New bug reported: " . $name . "";
									
									// Write the contents of your e-mail here using HTML code
									$message = '
									<table width="100%" style="background-color: #efefef; border-collapse: collapse;">

										<td style="padding: 40px;">

											<table width="100%">

												<tr>

													<td width="25%"></td>
													
													<td style="background-color: #fefefe;  border-top-left-radius: 20px; border-top-right-radius: 20px; padding: 20px;" width="50%">

														<table style="font-family: Lucida Grande, Verdana, Arial, Helvetica, sans-serif; font-size: 12px;" width="100%">

															<tr>

																<td width="95%">

																	<div style="padding-bottom: 10px;">Hi ' . ucfirst($user->name) . ',</div>
																	<div style="font-size: 18px; font-weight: bold; padding-bottom: 20px; text-transform: uppercase;">New bug reported</div>

																</td>

																<td valign="top" width="5%">

																	<img style="width: 100%; height: auto; max-height: auto;" src="http://www.time2change.com/pmt/assets/images/logo_t2c.png">

																</td>

															</tr>

															<tr>

																<td colspan="2"><p>' . ucfirst($assigner->name) . ' ' . ucfirst($assigner->surname) . ' reported a bug: <strong>' . ucfirst($name) . '</strong></p></td>

															</tr>

															<tr>

																<td colspan="2">

																	<div style="background-color: #f5f5f5; border: 1px dashed #d1d1d1; padding: 20px; font-size: 12px; font-style: italic; color: #303030; margin-top: 20px;">

																		' . nl2br($_POST['description']) . '

																	</div>

																</td>

															</tr>

															<tr>

																<td colspan="2">

																	<p style="padding-top: 20px;"><a style="color: #1d3f6a; text-decoration: underline;" target="_blank" href="pmt.time2change.com">Login to view all your bugs</a></p>

																</td>

															</tr>

														</table>

													</td>

													<td width="25%"></td>

												</tr>

												<tr>

													<td width="25%"></td>

													<td style="background-color: #1d3f6a; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; padding: 20px;" width="50%">

														<table style="font-family: Lucida Grande, Verdana, Arial, Helvetica, sans-serif; font-size: 12px;" width="100%">

															<tr>

																<td align="left" width="50%">

																	<p style="color: #ffffff; margin: 0; padding: 0;">Project Management Tool</p>

																</td>
																
																<td align="right" width="50%">

																	<p style="font-size: 11px; color: #ffffff; margin: 0; padding: 0;">&copy; <a style="color: #ffffff; text-decoration: underline;" target="_blank" href="http://www.time2change.com">Time2change</a></p>

																</td>

															</tr>

														</table>

													</td>

													<td width="25%"></td>

												</tr>

											</table>

										</td>

									</table>
									';
												
									$headers = "From: $from\r\n";
									$headers .= "Content-type: text/html\r\n";
									$to = $to;
									
									// Send the actual mail to the users
									mail($to, $subject, $message, $headers);

								}

							}

						}

					}

				}

			}

			if ((file_exists($_FILES['choose_file']['tmp_name'][0])) || (is_uploaded_file($_FILES['choose_file']['tmp_name'][0]))) {

				for ($i = 0; $i < count($_FILES['choose_file']['name']); $i++) {

					$file_name 	= $_FILES['choose_file']['name'][$i];
					$file_tmp 	= $_FILES['choose_file']['tmp_name'][$i];
					$file_size 	= $_FILES['choose_file']['size'][$i];
					$file_error = $_FILES['choose_file']['error'][$i];

					 if ($file_error === 0) {

						$sql4 = "SELECT `id` FROM `projects` WHERE `id` = '". $project ."'";

						if ($res = $con->query($sql4)) {

							while ($projects = $res->fetch_object()) {

								$directory = 'files/uploads/bugs/projects/'. strtolower($projects->id) .'';

								$extension = explode('.', $file_name);

								$extension = strtolower(end($extension));

								if (strlen($extension) > 1) {

									$new_name = uniqid('', false) .'.'. $extension;

								}

								$filename = $directory.'/'. $new_name;

								if (!file_exists($directory)) {

									mkdir($directory, 0777, true);

								}

								move_uploaded_file($file_tmp, $filename);

								$file_path = $filename;

								$sql2 = "INSERT INTO `projects_bugs_files` (`name`, `path`, `bug_id`, `created_at`, `created_by`) VALUES ('" . $_FILES['choose_file']['name'][$i] . "', '". $file_path ."', '" . $inserted_id . "', now(), '" . $_SESSION['id'] . "')";

								$con->query($sql2);

							}

						}

					}

				}

			}

			$_SESSION['message'] = 'bug_created';

			header("location: " . $_SERVER['REQUEST_URI'] . "");
			exit();

		}

	} else 

	if (isset($_POST['create-task'])) {

		$project = $_POST['project-id'];
		$name = $con->real_escape_string($_POST['name']);
		$responsible = htmlentities($_POST['responsible']);
		$deadline = date("Y-m-d", strtotime($_POST['deadline18']));
		$estimate_hour = htmlentities($_POST['estimated_hour']);
		$estimate_minute = htmlentities($_POST['estimated_minute']);
		$category = htmlentities($_POST['category']);
		$module = htmlentities($_POST['module']);
		$description = mysqli_real_escape_string($con, $_POST['description']);
		$files = isset($_FILES['choose_file2']) ? $_FILES['choose_file2'] : false;
		$_FILES['choose_file2']['tmp_name'][0] = isset($_FILES['choose_file2']['tmp_name'][0]) ? $_FILES['choose_file2']['tmp_name'][0] : false;
		$important = isset($_POST['important']) ? 1 : 0;

		if ($deadline == '1970-01-01') {
			$deadline = '0000-00-00';
		} else {
			$deadline = $deadline . ' 23:59:59';
		}

		if (empty($name)) {
			$t_response = '<span style="color: #a70000; font-weight: bold;">Enter the task name</span>';
		} else
		if (empty($responsible)) { 
			$t_response = '<span style="color: #a70000; font-weight: bold;">Select a responsible</span>';
		} else
		if (empty($category)) {
			$t_response = '<span style="color: #a70000; font-weight: bold;">Select a category</span>';
		} else {

			$estimated = $estimate_hour . ':' . $estimate_minute;

			$sql = "INSERT INTO `projects_tasks` (`name`, `description`, `category`, `responsible`, `deadline`, `label`, `estimated_time`, `important`, `project_id`, `module_id`, `created_by`, `created_at`) VALUES ('" . $name . "', '" . $description . "', '" . $category . "', '" . $responsible . "', '" . $deadline . "', '5', '" . $estimated . "', '" . $important . "', '" . $project . "', '" . $module . "', '" . $_SESSION['id'] . "', now())";
			$con->query($sql);

			$inserted_task_id = mysqli_insert_id($con);

			$sql2 = "INSERT INTO `projects_tasks_users` (`project_id`, `task_id`, `user_id`) VALUES ('" . $project . "', '" . $inserted_task_id . "', '" . $responsible . "')";
			$con->query($sql2);

			$sql3 = "SELECT * FROM `users` WHERE `id` = '" . $responsible . "'";

			if ($result3 = $con->query($sql3)) {

				while ($user = $result3->fetch_object()) {

					$sql4 = "SELECT * FROM `users` WHERE `id` = '" . $_SESSION['id'] . "'";

					if ($result4 = $con->query($sql4)) {

						while ($assigner = $result4->fetch_object()) {

							$sql5 = "SELECT * FROM `projects` WHERE `id` = '" . $project . "'";

							if ($result5 = $con->query($sql5)) {

								while ($project2 = $result5->fetch_object()) {

									echo nl2br($_POST['description']);

									$to = $user->email;
									$from = "info@time2change.com";
									$subject = "#" . $inserted_task_id . " - New task created: " . $name . "";
									
									// Write the contents of your e-mail here using HTML code
									$message = '
									<table width="100%" style="background-color: #efefef; border-collapse: collapse;">

										<td style="padding: 40px;">

											<table width="100%">

												<tr>

													<td width="25%"></td>
													
													<td style="background-color: #fefefe;  border-top-left-radius: 20px; border-top-right-radius: 20px; padding: 20px;" width="50%">

														<table style="font-family: Lucida Grande, Verdana, Arial, Helvetica, sans-serif; font-size: 12px;" width="100%">

															<tr>

																<td width="95%">

																	<div style="padding-bottom: 10px;">Hi ' . ucfirst($user->name) . ',</div>
																	<div style="font-size: 18px; font-weight: bold; padding-bottom: 20px; text-transform: uppercase;">New task created</div>

																</td>

																<td valign="top" width="5%">

																	<img style="width: 100%; height: auto; max-height: auto;" src="http://www.time2change.com/pmt/assets/images/logo_t2c.png">

																</td>

															</tr>

															<tr>

																<td colspan="2"><p>' . ucfirst($assigner->name) . ' ' . ucfirst($assigner->surname) . ' created a task: <strong>' . ucfirst($name) . '</strong></p></td>

															</tr>

															<tr>

																<td colspan="2">

																	<div style="background-color: #f5f5f5; border: 1px dashed #d1d1d1; padding: 20px; font-size: 12px; font-style: italic; color: #303030; margin-top: 20px;">

																		' . nl2br($_POST['description']) . '

																	</div>

																</td>

															</tr>

															<tr>

																<td colspan="2">

																	<p style="padding-top: 20px;"><a style="color: #1d3f6a; text-decoration: underline;" target="_blank" href="pmt.time2change.com">Login to view all your tasks</a></p>

																</td>

															</tr>

														</table>

													</td>

													<td width="25%"></td>

												</tr>

												<tr>

													<td width="25%"></td>

													<td style="background-color: #1d3f6a; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; padding: 20px;" width="50%">

														<table style="font-family: Lucida Grande, Verdana, Arial, Helvetica, sans-serif; font-size: 12px;" width="100%">

															<tr>

																<td align="left" width="50%">

																	<p style="color: #ffffff; margin: 0; padding: 0;">Project Management Tool</p>

																</td>
																
																<td align="right" width="50%">

																	<p style="font-size: 11px; color: #ffffff; margin: 0; padding: 0;">&copy; <a style="color: #ffffff; text-decoration: underline;" target="_blank" href="http://www.time2change.com">Time2change</a></p>

																</td>

															</tr>

														</table>

													</td>

													<td width="25%"></td>

												</tr>

											</table>

										</td>

									</table>
									';
												
									$headers = "From: $from\r\n";
									$headers .= "Content-type: text/html\r\n";
									$to = $to;
									
									// Send the actual mail to the users
									mail($to, $subject, $message, $headers);

								}

							}

						}

					}

				}

			}

			if ((file_exists($_FILES['choose_file2']['tmp_name'][0])) || (is_uploaded_file($_FILES['choose_file2']['tmp_name'][0]))) {

				for ($i = 0; $i < count($_FILES['choose_file2']['name']); $i++) {

					$file_name 	= $_FILES['choose_file2']['name'][$i];
					$file_tmp 	= $_FILES['choose_file2']['tmp_name'][$i];
					$file_size 	= $_FILES['choose_file2']['size'][$i];
					$file_error = $_FILES['choose_file2']['error'][$i];

					 if ($file_error === 0) {

						$sql14 = "SELECT `id` FROM `projects` WHERE `id` = '". $project ."'";

						if ($res = $con->query($sql14)) {

							while ($projects = $res->fetch_object()) {

								$directory = 'files/uploads/tasks/projects/'. strtolower($projects->id) .'';

								$extension = explode('.', $file_name);

								$extension = strtolower(end($extension));

								if (strlen($extension) > 1) {

									$new_name = uniqid('', false) .'.'. $extension;

								}

								$filename = $directory.'/'. $new_name;

								if (!file_exists($directory)) {

									mkdir($directory, 0777, true);

								}

								move_uploaded_file($file_tmp, $filename);

								$file_path = $filename;

								$sql22 = "INSERT INTO `projects_tasks_files` (`name`, `path`, `task_id`, `created_at`, `created_by`) VALUES ('" . $_FILES['choose_file2']['name'][$i] . "', '". $file_path ."', '" . $inserted_task_id . "', now(), '" . $_SESSION['id'] . "')";

								$con->query($sql22);

							}

						}

					}

				}

			}

			$_SESSION['message'] = 'task_created';

			header("location: " . $_SERVER['REQUEST_URI'] . "");
			exit();

		}

	}

?>

<?php

	if (!empty($project_id)) {
		$task_tab = 'active';
		$project_tab = '';
	} else {
		$task_tab = '';
		$project_tab = 'active';
	}

?>

<div class="work-environment">

	<div class="hopper">

		<select class="hop-project" onchange="hopProject();">

			<option value="">Hop to a project</option>

			<?php

			$sql = "SELECT 
						`id`, `name`
					FROM 
						`projects`
					ORDER BY
						`name`
					ASC
					";

			if ($result = $con->query($sql)) {

				while ($project = $result->fetch_object()) {

					if ($project->id == $project_id) {

						$selected = 'selected="selected"';

					} else {
						$selected = '';
					}

					echo '<option value="' . $project->id . '" ' . $selected . '>' . ucfirst($project->name) . '</option>';

				}

			}

			?>

		</select>

	</div>

	<div class="tab-nav">

		<ul class="nav nav-tabs" role="tablist">

			<?php

			if (!empty($project_id)) {

			?>

			<li role="presentation"><a href="#project" aria-controls="project" role="tab" data-toggle="tab"><span class="icon icon-plus"></span> Project</a></li>

			<?php

			} else {

			?>

			<li role="presentation" class="active"><a href="#project" aria-controls="project" role="tab" data-toggle="tab"><span class="icon icon-plus"></span> Project</a></li>

			<?php

			}

			if (!empty($project_id)) {

			?>

			<li role="presentation"><a href="#module" aria-controls="module" role="tab" data-toggle="tab"><span class="icon icon-plus"></span> Module</a></li>
			<li role="presentation" class="active"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab"><span class="icon icon-plus"></span> Task</a></li>
			<li role="presentation"><a href="#bugs" aria-controls="bugs" role="tab" data-toggle="tab"><span class="icon icon-plus"></span> Bug</a></li>

			<?php

			}

			?>

		</ul>

	</div>

	<div class="tab-content">

		<div role="tabpanel" class="tab-pane <?php echo $project_tab; ?>" id="project">

			<h2>Create new project</h2>

			<?php echo $success; ?>

			<form action="" method="post">

				<div class="form-group">

					<input autocomplete="off" class="name" name="name" type="text" id="name" value="<?php echo (isset($_POST['name']) ? $_POST['name'] : ""); ?>" placeholder="Projectname">

				</div>

				<div class="form-group">

					<select id="responsible" class="responsible" name="responsible">
						<option value="">Select a responsible</option>
						<?php

							$sql = "SELECT * FROM `users`";

							if ($result = $con->query($sql)) {

								while ($user = $result->fetch_object()) { 

									if ($user->id == $_POST['responsible']) {
										$selected = 'selected="selected"';
									} else {
										$selected = '';
									}

								?>

								<option value="<?php echo $user->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

								<?php

								}

							}

						?>
					</select>

				</div>

				<div class="form-group">

					<input autocomplete="off" class="deadline calendar" name="deadline" type="text" id="datepicker" value="<?php echo (isset($_POST['deadline']) ? $_POST['deadline'] : ""); ?>" placeholder="Deadline">

				</div>

				<div class="form-group">

					<select name="label" class="label" id="label">
						<option value="">Select a label</option>
						<?php

							$sql = "SELECT * FROM `labels` WHERE `type` = '1'";

							if ($result = $con->query($sql)) {

								while ($label = $result->fetch_object()) { 

									if ($label->id == $_POST['label']) {
										$selected = 'selected="selected"';
									} else {
										$selected = '';
									}

								?>

								<option value="<?php echo $label->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($label->name); ?></option>

								<?php

								}

							}

						?>
					</select>

				</div>

				<div class="form-group">

					<input autocomplete="off" class="customer" name="customer" type="text" id="customer" value="<?php echo (isset($_POST['customer']) ? $_POST['customer'] : ""); ?>" placeholder="Customer">

				</div>

				<div class="form-group">

					<select name="channel" class="channel" id="channel">
						<option value="">Choose a channel</option>
						<?php

							$sql = "SELECT * FROM `users_groups`";

							if ($result = $con->query($sql)) {

								while ($group = $result->fetch_object()) { 

									if ($group->id == $_POST['channel']) {
										$selected = 'selected="selected"';
									} else {
										$selected = '';
									}

								?>

								<option value="<?php echo $group->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($group->name); ?></option>

								<?php

								}

							}

						?>
					</select>

				</div>

				<div class="form-group">

					<textarea name="description" class="description" id="description" placeholder="Describe the project"><?php echo (isset($_POST['description']) ? $_POST['description'] : ""); ?></textarea>

				</div>

				<div class="form-group">
					
					<?php

					if (isset($_POST['private']) == 1) {
						$checked = 'checked="checked"';
					} else {
						$checked = '';
					}

					?>

					<label><input type="checkbox" name="private" <?php echo $checked; ?> id="private"> <span class="label">Mark as a private project</span></label>

				</div>

				<p style="padding: 0; margin-top: 15px;"><button type="submit" class="create-project" name="create-project">Create project</button><span class="newproject-message" style="float: right; font-size: 12px;"><?php echo $p_response; ?></span></p>

			</form>

		</div>

		<div role="tabpanel" class="tab-pane" id="module">

			<h2>Create new module</h2>

			<?php echo $success; ?>

			<form action="" method="post">

				<div class="form-group">

					<input type="text" style="margin-bottom: 0;" name="name" class="name" id="name" value="<?php echo (isset($_POST['name']) ? $_POST['name'] : ""); ?>" placeholder="Modulename">

				</div>

				<?php

				if (!$project_id) {

					$sql = "SELECT
								*
							FROM
								`projects`
							";

					echo '<div class="form-group">';
					echo '<select class="project-id" name="project">';
					echo '<option value="">Select a project</option>';

					if ($result = $con->query($sql)) {

						while ($project = $result->fetch_object()) { 

							if ($project->id == $_POST['project']) {
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}

							echo '<option value="' . $project->id . '" ' . $selected . '>' . ucfirst($project->name) . '</option>';

						}

					}

					echo '</select>';
					echo '</div>';

				} else {

					echo '<input type="hidden" class="project-id" name="project" value="' . $project_id . '">';

				}

				?>

				<p style="padding: 0; margin-top: 15px;"><button type="submit" class="create-module" name="create-module">Create module</button><span class="newmodule-message" style="float: right; font-size: 12px;"><?php echo $m_response; ?></span></p>

			</form>

		</div>

		<div role="tabpanel" class="tab-pane <?php echo $task_tab; ?>" id="tasks">

			<h2>Create new task</h2>

			<?php echo $success; ?>

			<form action="" method="post" enctype="multipart/form-data">

				<div class="form-group">

					<input type="text" name="name" class="name" id="name" placeholder="Taskname">

				</div>

				<div class="form-group">

					<select id="responsible" class="responsible" name="responsible">
						<option value="">Select responsible</option>
						<?php

							$sql = "SELECT * FROM `users`";

							if ($result = $con->query($sql)) {

								while ($user = $result->fetch_object()) { ?>

								<option value="<?php echo $user->id; ?>"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

								<?php

								}

							}

						?>
					</select>

				</div>

				<div class="form-group">

					<input autocomplete="off" class="deadline18 calendar" name="deadline18" type="text" id="datepicker18" value="<?php echo (isset($_POST['deadline']) ? $_POST['deadline'] : ""); ?>" placeholder="Deadline">

				</div>

				<div class="form-group">

					<select id="category" class="category" name="category">
						<option value="">Choose category:</option>
						<?php

							$sql = "SELECT * FROM `projects_categories` ORDER BY `name` ASC";

							if ($result = $con->query($sql)) {

								while ($category = $result->fetch_object()) { ?>

								<option value="<?php echo $category->id; ?>"><?php echo ucfirst($category->name); ?></option>

								<?php

								}

							}

						?>
					</select>

				</div>

				<div class="form-group">

					<label for="estimated">Estimated on </label>
					<select class="small-select" id="estimated" class="estimated_hour" name="estimated_hour">
					<?php

					for ($i = 0; $i < 201; $i++) {

						echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

					}

					?>
					</select>

					<select class="estimated_minute small-select" name="estimated_minute">
						<option value="00">00</option>
						<option value="05">05</option>
						<option value="10">10</option>
						<option value="15">15</option>
						<option value="20">20</option>
						<option value="25">25</option>
						<option value="30">30</option>
						<option value="35">35</option>
						<option value="40">40</option>
						<option value="45">45</option>
						<option value="50">50</option>
						<option value="55">55</option>
					</select>

				</div>

				<div class="form-group">

					<select name="module" class="module" id="module">
						<option value="">Link to module:</option>
						<?php

							$sql = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project_id . "'";

							if ($result = $con->query($sql)) {

								while ($module = $result->fetch_object()) { ?>

								<option value="<?php echo $module->id; ?>"><?php echo ucfirst($module->name); ?></option>

								<?php

								}

							}

						?>
					</select>

				</div>

				<div class="form-group">

					<textarea name="description" class="description" id="description" placeholder="Detailed description"></textarea>

				</div>

				<div class="form-group">

					<input type="file" id="taskfiles" class="choose_file2" name="choose_file2[]" multiple style="margin-bottom: 10px; margin-top: 5px;">

				</div>

				<div class="form-group">
					
					<label><input type="checkbox" name="important" id="important"> <span class="label">Yes, mark as important</span></label>

				</div>

				<input type="hidden" class="project-id" name="project-id" value="<?php echo $project_id; ?>">

				<p style="padding: 0; margin-top: 15px;"><button type="submit" class="create-task" name="create-task">Create task</button><span class="newtask-message" style="float: right; font-size: 12px;"><?php echo $t_response; ?></span></p>

			</form>

		</div>

		<div role="tabpanel" class="tab-pane" id="bugs">

			<h2>Create new bug</h2>

			<?php echo $success; ?>

			<form action="" method="post" enctype="multipart/form-data">

				<div class="form-group">

					<input type="text" name="bugname" class="bugname" placeholder="Bugname" id="bugname" value="<?php echo (isset($_POST['bugname']) ? $_POST['bugname'] : ""); ?>">

				</div>

				<div class="form-group">

					<select id="responsible5" class="responsible5" name="responsible5">
						<option value="">Select a responsible</option>
						<?php

							$sql = "SELECT * FROM `users`";

							if ($result = $con->query($sql)) {

								while ($user = $result->fetch_object()) { ?>

								<option value="<?php echo $user->id; ?>"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

								<?php

								}

							}

						?>
					</select>

				</div>

				<div class="form-group">

					<input type="text" name="deadline5" class="deadline5 calendar" placeholder="Deadline" id="deadline5" value="<?php echo (isset($_POST['deadline5']) ? $_POST['deadline5'] : ""); ?>">

				</div>

				<div class="form-group">

					<input type="text" name="path" class="path" placeholder="Path/URL" id="path" value="<?php echo (isset($_POST['path']) ? $_POST['path'] : ""); ?>">

				</div>

				<div class="form-group">

					<input type="text" name="category2" class="category2" placeholder="Category" id="category2" value="<?php echo (isset($_POST['category2']) ? $_POST['category2'] : ""); ?>">

				</div>

				<div class="form-group">

					<textarea name="description5" class="description5" placeholder="Detailed description" id="description5"><?php echo (isset($_POST['description5']) ? $_POST['description5'] : ""); ?></textarea>

				</div>

				<div class="form-group">

					<input type="file" id="choose_file" class="choose_file" name="choose_file[]" multiple style="margin-bottom: 20px; margin-top: 5px;">

				</div>

				<div class="form-group">
					
					<label><input type="checkbox" name="important3" id="important3" value="1"> <span class="label">Yes, mark as important</span></label>

				</div>

				<input type="hidden" class="project-id" name="project-id" value="<?php echo $project_id; ?>">

				<p style="padding: 0; margin-top: 15px;"><button type="submit" class="create-bug" name="create-bug">Create bug</button><span class="newbug-message" style="float: right; font-size: 12px;"><?php echo $b_response; ?></span></p>

			</form>

		</div>

	</div>

</div>

<div class="work-listings">

	<div class="more-listing-options">

		<?php

		// Total My Tasks
			$query_1 = "SELECT * FROM `projects_tasks` WHERE `responsible` = '" . $_SESSION['id'] . "' AND `label` != '6' AND `completed` != '1'";
			$query_count_1 = $con->query($query_1)->num_rows;

		// Total My Bugs
			$query_2 = "SELECT * FROM `projects_bugs` WHERE `responsible` = '" . $_SESSION['id'] . "' AND `status` != '6' AND `completed` != '1'";
			$query_count_2 = $con->query($query_2)->num_rows;

		// Total My To-do's
			$query_count_3 = ($query_count_1 + $query_count_2);

		// Total My Important Tasks
			$query_4 = "SELECT * FROM `projects_tasks` WHERE `responsible` = '" . $_SESSION['id'] . "' AND `label` != '6' AND `completed` != '1' AND `important` = '1'";
			$query_count_4 = $con->query($query_4)->num_rows;

		// Total My Important Bugs
			$query_5 = "SELECT * FROM `projects_bugs` WHERE `responsible` = '" . $_SESSION['id'] . "' AND `status` != '6' AND `completed` != '1' AND `important` = '1'";
			$query_count_5 = $con->query($query_5)->num_rows;

		// Total My Important To-do's
			$query_count_6 = ($query_count_4 + $query_count_5);

		// All Total Tasks
			$query_7 = "SELECT * FROM `projects_tasks` WHERE `label` != '6' AND `completed` != '1'";
			$query_count_7 = $con->query($query_7)->num_rows;

		// All Total Bugs
			$query_8 = "SELECT * FROM `projects_bugs` WHERE `status` != '6' AND `completed` != '1'";
			$query_count_8 = $con->query($query_8)->num_rows;

		// Total To-do's
			$query_count_9 = ($query_count_7 + $query_count_8);

		// All Finished Tasks
			$query_10 = "SELECT * FROM `projects_tasks` WHERE `label` = '6' AND `completed` = '0' AND `created_by` = '" . $_SESSION['id'] . "'";
			$query_count_10 = $con->query($query_10)->num_rows;

		// All Finished Bugs
			$query_11 = "SELECT * FROM `projects_bugs` WHERE `status` = '6' AND `completed` = '0' AND `created_by` = '" . $_SESSION['id'] . "'";
			$query_count_11 = $con->query($query_11)->num_rows;

		// Total Finished To-do's
			$query_count_12 = ($query_count_10 + $query_count_11);

		// All Finished Tasks
			$query_13 = "SELECT * FROM `projects_tasks` WHERE `label` = '6' AND `completed` = '1' AND `created_by` = '" . $_SESSION['id'] . "'";
			$query_count_13 = $con->query($query_13)->num_rows;

		// All Finished Bugs
			$query_14 = "SELECT * FROM `projects_bugs` WHERE `status` = '6' AND `completed` = '1' AND `created_by` = '" . $_SESSION['id'] . "'";
			$query_count_14 = $con->query($query_14)->num_rows;

		// Total Finished To-do's
			$query_count_15 = ($query_count_13 + $query_count_14);

		?>

		<ul>

			<li class="containing-tool active2"><span class="icon icon-clipboard"></span>

				<ul>

					<li class="tool show-my-tasks active" data-origin="show-my-tasks">My to-do's <span class="count">(<?php echo $query_count_3; ?>)</span></li>
					<li class="tool show-my-bugs" data-origin="show-my-bugs">My bugs <span class="count">(<?php echo $query_count_2; ?>)</span></li>
					<li class="tool show-important-tasks" data-origin="show-important-tasks">My Important to-do's <span class="count">(<?php echo $query_count_6; ?>)</span></li>
					<li class="tool show-all-tasks" data-origin="show-all-tasks">All to-do's <span class="count">(<?php echo $query_count_9; ?>)</span></li>
					<li class="tool show-all-for_reviews" data-origin="show-all-for_reviews">To review <span class="count">(<?php echo $query_count_12; ?>)</span></li>
					<li class="tool show-my-completed-tasks" data-origin="show-my-completed-tasks">My reviewed <span class="count">(<?php echo $query_count_15; ?>)</span></li>

				</ul>

			</li>

			<?php

			if (empty($project_id)) {
				$data_attr = '';
			} else {
				$data_attr = 'data-project="' . $project_id . '"';
			}

			?>

			<li class="containing-tool intranet" <?php echo $data_attr; ?>><span class="icon icon-sphere"></span></li>
			<li class="containing-tool treeview" <?php echo $data_attr; ?>><span class="icon icon-tree"></span></li>
			<li class="containing-tool"><span class="icon icon-calendar"></span></li>
			<li class="containing-tool"><span class="icon icon-users"></span></li>

			<li class="containing-tool"><span class="icon icon-history"></span>

				<ul>

					<li class="tool show-my-timings" <?php echo $data_attr; ?>>All my logs <span class="icon"><span class="icon icon-clock"></span></span></li>
					<li class="tool show-all-timings" <?php echo $data_attr; ?>>Everyone's logs <span class="icon"><span class="icon icon-clock2"></span></span></li>
					<li class="tool show-filter-timings" <?php echo $data_attr; ?>>Filter logs <span class="icon"><span class="icon icon-filter"></span></span></li>
					<li class="tool show-timings-vs" <?php echo $data_attr; ?>>Estimated vs effective <span class="icon"><span class="icon icon-calculator"></span></span></li>

				</ul>

			</li>

		</ul>

	</div>

	<div class="push-my-listings"></div>

	<div class="my-listings">

		<?php

	$sql = "SELECT 
				*,
				projects_tasks.id AS task_id,
				projects_tasks.name AS task_name,
				projects_tasks.deadline AS task_deadline,
				projects_tasks.important AS important_task,
				users.name AS task_creator_name,
				users.surname AS task_creator_surname,
				users2.name AS task_responsible_name,
				users2.surname AS task_responsible_surname,
				projects.name AS project_name, 
				projects_categories.name AS category_name,
				labels.name AS label_name
			FROM `projects_tasks` 
				INNER JOIN `projects` ON projects.id = projects_tasks.project_id
				INNER JOIN `projects_categories` ON projects_categories.id = projects_tasks.category
				INNER JOIN `users` ON users.id = projects_tasks.created_by
				INNER JOIN `users` AS users2 ON users2.id = projects_tasks.responsible
				INNER JOIN `labels` ON labels.id = projects_tasks.label
			WHERE 
				projects_tasks.label != '6' 
			AND 
				projects_tasks.responsible = '" . $_SESSION['id'] . "' 
			ORDER BY 
				projects_tasks.deadline = '0000-00-00 00:00:00', 
				projects_tasks.deadline ASC, 
				projects_tasks.created_at ASC
			";

	$sql2 = "SELECT 
				*,
				projects_bugs.name AS bug_name,
				projects_bugs.deadline AS bug_deadline,
				projects_bugs.id AS bug_id,
				projects_bugs.important AS important_bug,
				users.name AS bug_creator_name,
				users.surname AS bug_creator_surname,
				users2.name AS bug_responsible_name,
				users2.surname AS bug_responsible_surname,
				projects.name AS project_name,
				labels.name AS label_name
			FROM `projects_bugs` 
				INNER JOIN `projects` ON projects.id = projects_bugs.project_id
				INNER JOIN `users` ON users.id = projects_bugs.created_by
				INNER JOIN `users` AS users2 ON users2.id = projects_bugs.responsible
				INNER JOIN `labels` ON labels.id = projects_bugs.status
			WHERE 
				projects_bugs.status != '6' 
			AND 
				projects_bugs.responsible = '" . $_SESSION['id'] . "'
			ORDER BY 
				projects_bugs.deadline = '0000-00-00 00:00:00', 
				projects_bugs.deadline ASC, 
				projects_bugs.created_at ASC
			";

	if (($con->query($sql)->num_rows == 0) && ($con->query($sql2)->num_rows == 0)) {

		echo '<p class="no-tasks">You are not assigned to any open tasks/bugs</p>';

	} else {

		if ($result = $con->query($sql)) {

		?>

		<table width="100%">

				<thead>

					<th width="3%">Ref</th>
					<th width="18%">Name</th>
					<th width="3%"></th>
					<th width="3%"></th>
					<th width="18%">Project</th>
					<th width="9%">Category</th>
					<th width="8%">Deadline</th>
					<th width="10%">Label</th>
					<th width="14%">Responsible</th>
					<th width="14%">Assigned by</th>

				</thead>

				<?php

			while ($task = $result->fetch_object()) {

				$sql3 = "SELECT 
							*,
							projects_tasks_milestones.name AS milestone_name,
							projects_tasks_milestones.id AS milestone_id,
							users3.name AS milestone_firstname,
							users3.surname AS milestone_lastname
						FROM `projects_tasks_milestones` 
							INNER JOIN `users` AS users3 ON users3.id = projects_tasks_milestones.created_by
						WHERE 
							`task_id` = '" . $task->task_id . "'
						ORDER BY `deadline` ASC
						";

				?>

				<tr class="no-divider" data-id="<?php echo $task->task_id; ?>" data-type="task" data-origin="show-my-tasks">

					<td class="<?php echo define_priority($task->task_deadline); ?>">#<?php echo sprintf("%04d", $task->task_id); ?></td>
					<td><?php echo ucfirst($task->task_name); ?></td>
					<td><?php if ($task->important_task == 1) : echo '<span class="icon icon-notification"></span>'; endif; ?></td>
					<td><?php if ($con->query($sql3)->num_rows > 0) : echo '<span class="icon icon-arrow-down2" style="font-size: 9px; color: #c1c1c1;"></span>'; endif; ?></td>
					<td><?php echo ucfirst($task->project_name); ?></td>
					<td><?php echo ucfirst($task->category_name); ?></td>
					<td><?php echo time_left($task->task_deadline); ?></td>
					<td class="the-label" data-id="<?php echo $task->task_id; ?>"><span class="<?php echo str_replace(' ', '-', $task->label_name); ?>"><?php echo ucfirst($task->label_name); ?></span></td>
					<td><?php echo ucfirst($task->task_responsible_name); ?> <?php echo ucfirst($task->task_responsible_surname); ?></td>
					<td><?php echo ucfirst($task->task_creator_name); ?> <?php echo ucfirst($task->task_creator_surname); ?></td>

				</tr>

				<?php

				if ($result3 = $con->query($sql3)) {

					while ($milestone = $result3->fetch_object()) {

						if ($milestone->completed == 1) {
							$milestone_class = 'complete icon-checkmark';
						} else {
							$milestone_class = 'incomplete icon-checkmark';
						}

						?>

						<tr class="milestone">

							<td class="milestone-bullet"><span class="<?php echo $milestone_class; ?>" data-id="<?php echo $milestone->milestone_id; ?>"></span></td>
							<td class="milestone-check" colspan="5"><?php echo $milestone->milestone_name; ?></td>

							<td class="milestone-deadline" colspan="4"><?php echo time_left($milestone->deadline); ?></td>

						</tr>

						<?php

					}

				}

			}

			echo '</table>';

		}

	?>

	<?php

		if ($result2 = $con->query($sql2)) { ?>

		<table width="100%">

			<tr>

				<td class="view-divider" colspan="10"></td>

			</tr>

		</table>

		<table width="100%">

			<?php

			while ($bug = $result2->fetch_object()) {

				?>

				<tr class="no-divider" data-id="<?php echo $bug->bug_id; ?>" data-type="bug" data-origin="show-my-tasks">

					<td class="<?php echo define_priority($bug->bug_deadline); ?>" width="3%">#<?php echo sprintf("%04d", $bug->bug_id); ?></td>
					<td width="18%"><?php echo ucfirst($bug->bug_name); ?></td>
					<td width="3%"><?php if ($bug->important_bug == 1) : echo '<span class="icon icon-notification"></span>'; endif; ?></td>
					<td width="3%">&nbsp;</td>
					<td width="18%"><?php echo ucfirst($bug->project_name); ?></td>
					<td width="9%"><?php echo ucfirst($bug->category); ?></td>
					<td width="8%"><?php echo time_left($bug->bug_deadline); ?></td>
					<td class="the-label" data-id="<?php echo $bug->bug_id; ?>" width="10%"><span class="<?php echo str_replace(' ', '-', $bug->label_name); ?>"><?php echo ucfirst($bug->label_name); ?></span></td>
					<td width="14%"><?php echo ucfirst($bug->bug_responsible_name); ?> <?php echo ucfirst($bug->bug_responsible_surname); ?></td>
					<td width="14%"><?php echo ucfirst($bug->bug_creator_name); ?> <?php echo ucfirst($bug->bug_creator_surname); ?></td>

				</tr>

				<?php

			}

			?>

			</tbody>


		</table>

			<?php

		}

	}

?>

	</div>

</div>

<?php unset($_SESSION['message']); ?>