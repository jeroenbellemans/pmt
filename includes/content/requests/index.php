<h1>Requests</h1>

<p>Accept or decline the requests send by the users</p>

<div class="section-divider"></div>

<div class="tabbed">

	<ul class="nav nav-tabs" role="tablist">

		<li role="presentation" class="active"><a href="#pending" aria-controls="pending" role="tab" data-toggle="tab"><span class="icon2 icon-loop2"></span> Pending</a></li>
		<li role="presentation"><a href="#accepted" aria-controls="accepted" role="tab" data-toggle="tab"><span class="icon2 icon-checkmark"></span> Accepted</a></li>
		<li role="presentation"><a href="#declined" aria-controls="declined" role="tab" data-toggle="tab"><span class="icon2 icon-cross"></span> Declined</a></li>
		<!--<li role="presentation"><a href="#canceled" aria-controls="canceled" role="tab" data-toggle="tab"><span class="icon2 icon-blocked"></span> Canceled</a></li>-->

	</ul>

</div>

<!--

<div class="utilities">

	<a class="neutral-button right" href="#" data-toggle="modal" data-target="#ChangePassword">Change password</a>

</div>

-->

<div class="content">

	<div class="tab-content">

		<div role="tabpanel" class="tab-pane active" id="pending">

			<!--

			<h2>Holiday requests - Herzien!!! Coming soon...</h2>

			<?php

				$sql = "SELECT * FROM `calendar_holliday` WHERE `status` = '0' ORDER BY `user_id` DESC, `date` ASC";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					if ($count == 0) {

						echo '<p class="no-result">There are currently no pending requests</p>';

					} else { ?>

						<table>

							<thead>

								<th width="30%">

									From

								</th>

								<th width="30%">

									Date

								</th>

								<th width="20%">

									Type

								</th>

								<th style="text-align: center;">

									Status

								</th>

							</thead>

						<?php

						while($day_request = $result->fetch_object()) {

							$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $day_request->user_id . "'";

							if ($result2 = $con->query($sql2)) {

								while ($user = $result2->fetch_object()) { ?>

									<tbody>

										<td>

											<?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?>

										</td>

										<td>

											<?php echo date("l", strtotime($day_request->date)); ?> <span class="weekday"><?php echo date("d F Y", strtotime($day_request->date)); ?></span>

										</td>

										<td>

											Holiday

										</td>

										<td align="center">

											<span class="accept accept-holiday" data-toggle="tooltip" data-placement="top" title="Accept this request"><span class="icon icon-checkmark"></span></span>
											<span class="decline decline-holiday" data-toggle="tooltip" data-placement="top" title="Decline this request"><span class="icon icon-cross"></span></span>

										</td>

									</tbody>

								<?php

								}

							}

						}

					}

				}

			?>

			</table>

			-->

			<h2>Remote work requests</h2>

			<?php

				$sql = "SELECT * FROM `calendar_remotework` WHERE `status` = '0' ORDER BY `user_id` DESC, `date` ASC";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					if ($count == 0) {

						echo '<p class="no-result">There are currently no pending requests</p>';

					} else { ?>

						<table>

							<thead>

								<th width="30%">

									From

								</th>

								<th width="30%">

									Date

								</th>

								<th width="20%">

									Type

								</th>

								<th style="text-align: center;">

									Status

								</th>

							</thead>

						<?php

						while($day_request = $result->fetch_object()) { 

							$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $day_request->user_id . "'";

							if ($result2 = $con->query($sql2)) {

								while ($user = $result2->fetch_object()) { ?>

									<tbody>

										<td>

											<?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?>

										</td>

										<td>

											<?php echo date("l", strtotime($day_request->date)); ?> <span class="weekday"><?php echo date("d F Y", strtotime($day_request->date)); ?></span>

										</td>

										<td>

											Remote

										</td>


										<td align="center">

											<span class="accept accept-remote" data-id="<?php echo $day_request->id; ?>" data-toggle="tooltip" data-placement="top" title="Accept this request"><span class="icon icon-checkmark"></span></span>
											<span class="decline decline-remote" data-id="<?php echo $day_request->id; ?>" data-toggle="tooltip" data-placement="top" title="Decline this request"><span class="icon icon-cross"></span></span>

										</td>

									</tbody>

								<?php

								}

							}

						}

					}

				}

			?>

			</table>

		</div>
		
		<div role="tabpanel" class="tab-pane" id="accepted">

			<h2>Accepted remote work requests</h2>

			<?php

				$sql = "SELECT * FROM `calendar_remotework` WHERE `status` = '1' ORDER BY `user_id` DESC, `date` ASC";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					if ($count == 0) {

						echo '<p class="no-result">There are currently no pending requests</p>';

					} else { ?>

						<table>

							<thead>

								<th width="30%">

									From

								</th>

								<th width="30%">

									Date

								</th>

								<th width="20%">

									Type

								</th>

								<th style="text-align: center;">

									Status

								</th>

							</thead>

						<?php

						while($day_request = $result->fetch_object()) { 

							$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $day_request->user_id . "'";

							if ($result2 = $con->query($sql2)) {

								while ($user = $result2->fetch_object()) { ?>

									<tbody>

										<td>

											<?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?>

										</td>

										<td>

											<?php echo date("l", strtotime($day_request->date)); ?> <span class="weekday"><?php echo date("d F Y", strtotime($day_request->date)); ?></span>

										</td>

										<td>

											Remote

										</td>


										<td align="center">

											<span class="accept accept-remote" data-id="<?php echo $day_request->id; ?>" data-toggle="tooltip" data-placement="top" title="Accept this request"><span class="icon icon-checkmark"></span></span>
											<span class="decline decline-remote" data-id="<?php echo $day_request->id; ?>" data-toggle="tooltip" data-placement="top" title="Decline this request"><span class="icon icon-cross"></span></span>

										</td>

									</tbody>

								<?php

								}

							}

						}

					}

				}

			?>

			</table>

		</div>
		<div role="tabpanel" class="tab-pane" id="declined">

			<h2>Accepted remote work requests</h2>

			<?php

				$sql = "SELECT * FROM `calendar_remotework` WHERE `status` = '2' ORDER BY `user_id` DESC, `date` ASC";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					if ($count == 0) {

						echo '<p class="no-result">There are currently no pending requests</p>';

					} else { ?>

						<table>

							<thead>

								<th width="30%">

									From

								</th>

								<th width="30%">

									Date

								</th>

								<th width="20%">

									Type

								</th>

								<th style="text-align: center;">

									Status

								</th>

							</thead>

						<?php

						while($day_request = $result->fetch_object()) { 

							$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $day_request->user_id . "'";

							if ($result2 = $con->query($sql2)) {

								while ($user = $result2->fetch_object()) { ?>

									<tbody>

										<td>

											<?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?>

										</td>

										<td>

											<?php echo date("l", strtotime($day_request->date)); ?> <span class="weekday"><?php echo date("d F Y", strtotime($day_request->date)); ?></span>

										</td>

										<td>

											Remote

										</td>


										<td align="center">

											<span class="accept accept-remote" data-id="<?php echo $day_request->id; ?>" data-toggle="tooltip" data-placement="top" title="Accept this request"><span class="icon icon-checkmark"></span></span>
											<span class="decline decline-remote" data-id="<?php echo $day_request->id; ?>" data-toggle="tooltip" data-placement="top" title="Decline this request"><span class="icon icon-cross"></span></span>

										</td>

									</tbody>

								<?php

								}

							}

						}

					}

				}

			?>

			</table>

		</div>
		<!--<div role="tabpanel" class="tab-pane" id="canceled">yedthg.</div> -->

	</div>
	
</div>