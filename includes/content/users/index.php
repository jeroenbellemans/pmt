<?php

	$user_id = isset($_GET['id']) ? $_GET['id'] : false;

	if (isset($_POST['create-new-user'])) {

		$firstname = escape($_POST['first_name']);
		$lastname = escape($_POST['last_name']);
		$email = escape($_POST['email']);
		$group = $_POST['group'];
		$permission = $_POST['permission'];
		$checked = '';
		$error = '';

		if ($_POST['access'] == '1') {
			$checked = '0';
		} else
		if ($_POST['access'] == '0') {
			$checked = '1';
		}

		if ((!$firstname) || (!$lastname) || (!$email) || (!$group) || (!$permission)) { 

			$error = '<p class="error">All fields are required fields!</p>';

		?>

		<script type="text/javascript">
			var error = '#AddNewUser';
		</script>
		
		<?php

		} else {

			// Generate a random 12 characters long password
			$length = 12;
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
		    $password = substr( str_shuffle( $chars ), 0, $length );
		    $hash = md5($password);

		    // Insert details into the database
		    $sql = "INSERT INTO `users` (`name`, `surname`, `email`, `password`, `branch`, `privileges`, `ban`, `created_at`) VALUES ('" . $firstname . "', '" . $lastname . "', '" . $email . "', '" . $hash . "', '" . $group . "', '" . $permission . "', '" . $checked . "', now())";
		    $con->query($sql);

		    // Send a mail to the user with his details to log in to the platform
		    
		    $to = $email;
			$from = "info@time2change.com";
			$subject = "Your account has been created";
			
			// Write the contents of your e-mail here using HTML code
			$message = '
			<h1>' . $firstname . ' ' . $lastname . '</h1>
			<h2>Welcome to the Project Management Tool</h2>
			<p>Your account has been created. You can now login to the platform using your e-mail address and the following password: <strong>' . $password . '</strong><br>We do strongly advice to change your password as soon as you login to the platform.</p>
			<p><a href="pmt.time2change.com">Login to your account</a></p>
			<br><br>
			<p>PMT &copy; Time2Change</p>
			';
						
			$headers = "From: $from\r\n";
			$headers .= "Content-type: text/html\r\n";
			$to = $to;
			
			// Send the actual mail to the users
			mail($to, $subject, $message, $headers);

			$_SESSION['message'] = 'The user has been successfuly created';

			header("location: index.php?cat=users");
			exit();

		}

	} else

	if (isset($_POST['create-new-group'])) {

		$group = escape($_POST['group_name']);
		$color = escape($_POST['color']);
		$error = '';

		if ((!$group) || (!$color)) { 

			$error = '<p class="error">All fields are required fields!</p>';

		} else {

			// Insert details into the database
		    $sql = "INSERT INTO `users_groups` (`name`, `color`) VALUES ('" . $group . "', '" . $color . "')";
		    $con->query($sql);

			$_SESSION['message'] = 'The group has been successfuly created';

			header("location: index.php?cat=users");
			exit();

		}

	} else

	if (isset($_POST['edit-existing-user'])) {

		$firstname2 = escape($_POST['first_name2']);
		$lastname2 = escape($_POST['last_name2']);
		$email2 = escape($_POST['email2']);
		$group2 = $_POST['group2'];
		$permission2 = $_POST['permission2'];
		$checked2 = '';
		$error = '';

		if ($_POST['access2'] == '1') {
			$checked2 = '0';
		} else {
			$checked2 = '1';
		}

		if ((!$firstname2) || (!$lastname2) || (!$email2) || (!$group2)) { 

			$error = $checked2;

		?>

		<script type="text/javascript">
			var error = '#EditUser';
		</script>
		
		<?php

		} else {

		    // Insert details into the database
		    $sql = "UPDATE `users` SET `name` = '" . $firstname2 . "', `surname` = '" . $lastname2 . "', `email` = '" . $email2 . "', `branch` = '" . $group2 . "', `privileges` = '" . $permission2 . "', `ban` = '" . $checked2 . "' WHERE `id` = '" . $user_id . "'";
		    $con->query($sql);

			$_SESSION['message'] = 'The user has been successfuly edited';

			header("location: index.php?cat=users");
			exit();

		}

	}

?>

<?php

	if (isset($_SESSION['message'])) {

		echo '<div class="success-message">' . $_SESSION['message'] . '</div>';

	}

	if ($user_id !== false) { ?>

		<script type="text/javascript">
			var error = '#EditUser';
		</script>

	<?php

	}

?>

<h1>Users</h1>

<p>Manage every user subscribed to this platform.</p>

<div class="section-divider"></div>

<div class="utilities">

	<a class="success-button left" href="#" data-toggle="modal" data-target="#AddNewUser">+ Add a new user</a>
	<a class="neutral-button right" href="#" data-toggle="modal" data-target="#AddNewGroup">+ Create a new group</a>

</div>

<div class="content">

	<?php

		$sql = "SELECT * FROM `users`";

		if ($result = $con->query($sql)) {

			$count = $result->num_rows;

			if ($count == '0') {

				echo '<p>We could not find any users</p>';

			} else { ?>

			<table>

				<thead>

					<th>

						Name

					</th>

					<th>

						E-mail

					</th>

					<th>

						Group

					</th>

					<th style="text-align: center;">

						Status

					</th>

					<th style="text-align: center;">

						Access

					</th>

					<th>

						Last activity

					</th>

				</thead>

				<?php

				while ($user = $result->fetch_object()) { 

					$status = '';
					switch ($user->status) {
						case "0": $status = '<span class="icon-radio-checked2 grey" data-toggle="tooltip" data-placement="top" title="Offline"></span>'; break;
						case "1": $status = '<span class="icon-radio-checked2 green" data-toggle="tooltip" data-placement="top" title="Online"></span>'; break;
						case "2": $status = '<span class="icon-radio-checked2 orange" data-toggle="tooltip" data-placement="top" title="Away"></span>'; break;
						case "3": $status = '<span class="icon-radio-checked2 red" data-toggle="tooltip" data-placement="top" title="Don\'t disturb"></span>'; break;
					}

					$ban = '';
					switch ($user->ban) {
						case "0": $ban = '<span class="icon-checkmark green" data-toggle="tooltip" data-placement="top" title="Permission allowed"></span>'; break;
						case "1": $ban = '<span class="icon-cross red" data-toggle="tooltip" data-placement="top" title="Permission denied"></span>'; break;
					}

				?>

					<tbody>

						<td>

							<a href="index.php?cat=users&id=<?php echo $user->id; ?>"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></a>

						</td>

						<td>

							<?php echo $user->email; ?>

						</td>

						<td>

							<?php

							$sql2 = "SELECT * FROM `users_groups` WHERE `id` = '" . $user->branch . "'";

							if ($result2 = $con->query($sql2)) {

								$count2 = $result2->num_rows;

								if ($count2 == 0) {

									echo '-';

								} else {

									while ($branch = $result2->fetch_object()) {

										echo ucfirst($branch->name);

									}

								}

							}

							?>

						</td>

						<td align="center">

							<?php echo $status; ?>

						</td>

						<td align="center">

							<?php echo $ban; ?>

						</td>

						<td>

							<?php echo elapsed_time($user->last_seen); ?>

						</td>

					</tbody>

				<?php

				}

			}

		}

	?>

	</table>

</div>

<!-- Create a new user modal -->

<div class="modal fade" id="AddNewUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add a new user</h4>

			</div>

			<form action="" method="post">

			<div class="message"><?php if (isset($error)) : echo $error; endif; ?></div>

			<div class="modal-body">

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="fname">First name</label>
							<input autocomplete="off" id="fname" type="text" name="first_name" required="required" value="<?php if (isset($firstname)) : echo $firstname; endif; ?>">

						</div>

						<div class="form-group">

							<label for="lname">Last name</label>
							<input autocomplete="off" id="lname" type="text" name="last_name" required="required" value="<?php if (isset($lastname)) : echo $lastname; endif; ?>">

						</div>

						<div class="form-group">

							<label for="email">Email</label>
							<input autocomplete="off" id="email" type="email" name="email" required="required" value="<?php if (isset($email)) : echo $email; endif; ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="group">Belongs to</label>
							<select id="group" name="group">
								<option value="">-</option>
								<?php 

								$sql = "SELECT * FROM `users_groups`";

								if ($result = $con->query($sql)) {

									$count = $result->num_rows;

									if ($count == 0) {

										echo '<option value="">-</option>';

									} else {

										while ($group = $result->fetch_object()) {

											echo '<option value="' . $group->id . '">' . ucfirst($group->name) . '</option>';

										}

									}

								}

								?>
							</select>

						</div>

						<div class="form-group">

							<label for="permission">Permission as</label>
							<select id="permission" name="permission">
								<option value="">-</option>
								<option value="0">Super administrator</option>
								<option value="1">Administrator</option>
								<option value="2">Normal user</option>
								<option value="3">Guest</option>
							</select>

						</div>

						<div class="form-group">

							<label for="access">Allow this user to use the platform</label>
							<input type="checkbox" name="access" id="access" checked="checked" value="1"> <span class="label">Yes, allow access</span>

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">
				
				<button type="submit" name="create-new-user" class="submit-button save-new-user" href="#">Save</a>

			</div>

			</form>

		</div>

	</div>

</div>

<!-- Create a new group modal -->

<div class="modal fade" id="AddNewGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Create a new group</h4>

			</div>

			<form action="" method="post">

			<div class="modal-body">

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="gname">Groupname</label>
							<input autocomplete="off" id="gname" type="text" name="group_name" required="required">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="color">Define a color to this group</label>
							<input autocomplete="off" id="color" type="color" name="color" required="required">

						</div>

					</div>

				</div>

				<div class="modal-divider"></div>

				<h4>Existing groups</h4>

				<?php

				$sql = "SELECT * FROM `users_groups`";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					if ($count == 0) {

						echo '<p>There are currently no groups</p>';

					} else {

						while ($group = $result->fetch_object()) { ?>

						<div class="group"><div style="float: left; background-color: <?php echo $group->color; ?>; width: 20px; height: 20px; margin-right: 5px;"></div><p class="group-name"><?php echo ucfirst($group->name); ?><span class="icon-set"><span class="inline-icon icon-bin" data-id="<?php echo $group->id; ?>"></span></span></p></div>

						<?php

						}

					}

				}

				?>

			</div>

			<div class="modal-footer">
				
				<button type="submit" name="create-new-group" class="submit-button" href="#">Save</a>

			</div>

			</form>

		</div>

	</div>

</div>

<!-- Edit an existing user modal -->

<div class="modal fade" id="EditUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit user</h4>

			</div>

			<?php

			$sql = "SELECT * FROM `users` WHERE `id` = '" . $user_id . "'";

			if ($result = $con->query($sql)) {

				$count = $result->num_rows; ?>

			<form action="" method="post">

			<div class="message"><?php if (isset($error)) : echo $error; endif; ?></div>

			<div class="modal-body">

				<?php

				if ($count == 0) {

					echo '<p class="info"></p>';

				} else {

					while ($users = $result->fetch_object()) { ?>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="fname2">First name</label>
							<input autocomplete="off" id="fname2" type="text" name="first_name2" required="required" value="<?php echo ucfirst($users->name); ?>">

						</div>

						<div class="form-group">

							<label for="lname2">Last name</label>
							<input autocomplete="off" id="lname2" type="text" name="last_name2" required="required" value="<?php echo ucfirst($users->surname); ?>">

						</div>

						<div class="form-group">

							<label for="email2">Email</label>
							<input autocomplete="off" id="email2" type="email" name="email2" required="required" value="<?php echo $users->email; ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="group2">Belongs to</label>
							<select id="group2" name="group2">
								<?php 

								$sql2 = "SELECT * FROM `users_groups`";

								if ($result2 = $con->query($sql2)) {

									$count2 = $result2->num_rows;

									if ($count2 == 0) {

										echo '<option value="">-</option>';

									} else {

										while ($group = $result2->fetch_object()) {

											$selected = '';

											if ($group->id == $users->branch) {

												$selected = 'selected="selected"';

											}

											echo '<option value="' . $group->id . '" ' . $selected . '>' . ucfirst($group->name) . '</option>';

										}

									}

								}

								?>
							</select>

						</div>

						<div class="form-group">

							<label for="permission2">Permission as</label>
							<select id="permission2" name="permission2">
								<?php

								switch ($users->privileges) {

									case "0":

									?>

									<option value="0" selected="selected">Super administrator</option>
									<option value="1">Administrator</option>
									<option value="2">Normal user</option>
									<option value="3">Guest</option>

									<?php

									break;

									case "1":

									?>

									<option value="0">Super administrator</option>
									<option value="1" selected="selected">Administrator</option>
									<option value="2">Normal user</option>
									<option value="3">Guest</option>

									<?php

									break;

									case "2":

									?>

									<option value="0">Super administrator</option>
									<option value="1">Administrator</option>
									<option value="2" selected="selected">Normal user</option>
									<option value="3">Guest</option>

									<?php

									break;

									case "3":

									?>

									<option value="0">Super administrator</option>
									<option value="1">Administrator</option>
									<option value="2">Normal user</option>
									<option value="3" selected="selected">Guest</option>

									<?php

									break;

								}

								?>
							</select>

						</div>

						<div class="form-group">

							<?php

							$checked = '';

							if ($users->ban == '0') {

								$checked = 'checked="checked"';

							}

							?>

							<label for="access2">Allow this user to use the platform</label>
							<input type="checkbox" name="access2" id="access2" <?php echo $checked; ?> value="1"> <span class="label">Yes, allow access</span>

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">
				
				<button type="submit" name="edit-existing-user" class="submit-button" href="#">Save</a>

			</div>

			</form>

					<?php

					}

				}

			}

			?>

		</div>

	</div>

</div>