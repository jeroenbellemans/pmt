<?php
	$month = isset($_GET['month']) ? $_GET['month'] : date('n');
	$year = isset($_GET['year']) ? $_GET['year'] : date('Y');
	$user_id = isset($_GET['uid']) ? $_GET['uid'] : $_SESSION['id'];
?>
<div class="special-navigation">

	<div class="browse-calendar">

	<?php

	$previous = $month - 1;
	$previous_year = $year - 1;
	$next = $month + 1;
	$next_year = $year + 1;

	if ($month == '1') {
		echo '<a href="index.php?cat=calendar&year=' . $previous_year . '&month=12&uid=' . $user_id . '"><button class="btn-default enabled first">&larr; </button></a>';
	} else
	if ($month > '1') {
		echo '<a href="index.php?cat=calendar&year=' . $year . '&month=' . $previous . '&uid=' . $user_id . '"><button class="btn-default enabled first">&larr; </button></a>';
	}

	echo '<button class="btn-default middle">' . date("F", mktime(0, 0, 0, $month, 10)) . ' ' . $year . '</button>';

	if ($month == '12') {
		echo '<a href="index.php?cat=calendar&year=' . $next_year . '&month=1&uid=' . $user_id . '"><button class="btn-default enabled last">&rarr; </button></a>';
	} else
	if ($month < '12') {
		echo '<a href="index.php?cat=calendar&year=' . $year . '&month=' . $next . '&uid=' . $user_id . '"><button class="btn-default enabled last">&rarr; </button></a>';
	}

	?>

	</div>

	<div class="choose-user">

		<select class="user-choice" onchange="redirectUser();">

		<?php

		$sql = "SELECT * FROM `users`";

		if ($result = $con->query($sql)) {

			while ($user = $result->fetch_object()) { ?>

			<option value="<?php echo $user->id; ?>" <?=$user->id == $user_id ? ' selected="selected"' : '';?>><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

			<?php

			}

		}

		?>

		</select>

	</div>

</div>

<div class="section-divider"></div>
<!--
<div class="utilities">

	<a class="success-button left" href="#" data-toggle="modal" data-target="#AddNewUser">+ Add a new user</a>
	<a class="neutral-button right" href="#" data-toggle="modal" data-target="#AddNewGroup">+ Create a new group</a>

</div>
-->
<div class="content">

	<?php 

	$day_headings = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'); 

	$running_day = date('N', mktime(0, 0, 0, $month, 1, $year))-1;
	$days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();
	$class = '';

	$today = date("d");
	$tmonth = date("m");

	?>

	<table class="calendar">

		<tr class="calendar-row">

			<td class="calendar-day-headings">

				<?php echo implode('</td><td class="calendar-day-headings">', $day_headings); ?>

			</td>

		</tr>

		<tr class="calendar-row">

			<?php

			for ($x = 0; $x < $running_day; $x++) {

				echo '<td class="calendar-day-np"> </td>';
				$days_in_this_week++;

			}

			if (strlen($month) < 2) {

				$month = '0' . $month;

			}

			for ($list_day = 1; $list_day <= $days_in_month; $list_day++) {

				if ($list_day < 10) {

					$list_day = '0' . $list_day;

				}

				$active_date = $year . '-' . $month . '-' . $list_day;

				$sql = mysqli_query($con, "SELECT * FROM `calendar_remotework` WHERE `date` = '" . $active_date . "' AND `user_id` = '" . $user_id . "'");
				$count_remote = mysqli_num_rows($sql);
				$sql2 = mysqli_query($con, "SELECT * FROM `calendar_holliday` WHERE `date` = '" . $active_date . "' AND `user_id` = '" . $user_id . "'");
				$count_holliday = mysqli_num_rows($sql2);
				$sql3 = mysqli_query($con, "SELECT * FROM `calendar_sickness` WHERE `date` = '" . $active_date . "' AND `user_id` = '" . $user_id . "'");
				$count_sickness = mysqli_num_rows($sql3);

				$sql4 = mysqli_query($con, "SELECT * FROM `calendar_remotework` WHERE `date` = '" . $active_date . "' AND `user_id` = '" . $user_id . "' AND `status` = '1'");
				$count_remote2 = mysqli_num_rows($sql4);
				$sql5 = mysqli_query($con, "SELECT * FROM `calendar_holliday` WHERE `date` = '" . $active_date . "' AND `user_id` = '" . $user_id . "' AND `status` = '1'");
				$count_holliday2 = mysqli_num_rows($sql5);

				$class2 = '';

				if ($count_remote2 == '1') {
					$class2 = 'day-status-remote';
				} else
				if ($count_holliday2 == '1') {
					$class2 = 'day-status-vacation';
				} else
				if ($count_sickness) {
					$class2 = 'day-status-sickness';
				}

				echo '<td class="calendar-day ' . $class2 . '" data-date="' . $year . '-' . $month . '-' . $list_day . '" data-user="' . $user_id . '">';

				if ($list_day == $today && $month == $tmonth) {
					$class = 'highlight';
				} else {
					$class = 'no-highlight';
				}

				if (date("D", strtotime($active_date)) == 'Sun' || date("D", strtotime($active_date)) == 'Sat') {
					$day_class = 'weekend';
				} else {
					$day_class = 'midweek';
				}

				echo '
				<div class="day-number">
					<span class="' . $day_class . ' ' . $class . '">' . $list_day . '</span>
					<span class="summary">';

					if ($count_remote2 == '1') {
						echo '<span class="icon icon-home" data-toggle="tooltip" data-placement="right" title="Works remote"></span>';
					} else
					if ($count_holliday2 == '1') {
						echo '<span class="icon icon-airplane" data-toggle="tooltip" data-placement="right" title="On holiday"></span>';
					} else
					if ($count_sickness) {
						echo '<span class="icon icon-sad2" data-toggle="tooltip" data-placement="right" title="Sick"></span>';
					}

				echo '
					</span>
				</div>';

				$sql10 = "SELECT * FROM `projects_tasks` WHERE DATE(`deadline`) = '" . $year . "-" . $month . "-" . $list_day . "' AND `planned` = '0' AND `responsible` = '" . $user_id . "' AND `completed` = '0'";
				$sql11 = "SELECT * FROM `projects_tasks` INNER JOIN `projects_tasks_schedule` ON projects_tasks_schedule.task_id = projects_tasks.id WHERE projects_tasks_schedule.date = '" . $year . "-" . $month . "-" . $list_day . "' AND `planned` = '1' AND `responsible` = '" . $user_id . "' AND `completed` = '0'";

				$count10 = $con->query($sql10)->num_rows;
				$count11 = $con->query($sql11)->num_rows;

				echo '
				<div class="tasks">

					<ul>
						<li><span class="icon icon-power"></span> <strong>' . $count10 . '</strong></li>
						<li><span class="icon icon-clipboard"></span> <strong>' . $count11 . '</strong></li>
						<li class="open-requests">More...</li>
					</ul>

				</div>
				';

				if ($count_holliday2 == 0 && $count_sickness == 0) {

					$sql8 = "SELECT SUM(TIME_TO_SEC(`estimated_time`)) AS unplanned FROM `projects_tasks` WHERE DATE(`deadline`) = '" . $year . "-" . $month . "-" . $list_day . "' AND `planned` = '0' AND `responsible` = '" . $user_id . "' AND `completed` = '0'";

					if ($result8 = $con->query($sql8)) {

						$sum_unplanned = $result8->fetch_object();

					}

					$sql9 = "SELECT SUM(TIME_TO_SEC(`estimated_time`)) AS planned FROM `projects_tasks` INNER JOIN `projects_tasks_schedule` ON projects_tasks_schedule.task_id = projects_tasks.id WHERE projects_tasks_schedule.date = '" . $year . "-" . $month . "-" . $list_day . "' AND `planned` = '1' AND `responsible` = '" . $user_id . "' AND `completed` = '0'";

					if ($result9 = $con->query($sql9)) {

						$sum_planned = $result9->fetch_object();

					}

					$daily_hours = '43200'; // 12h * 60 minutes * 60 seconds
					$total_sum = ($sum_planned->planned + $sum_unplanned->unplanned);
					$percentage = (100 - (($total_sum / $daily_hours) * 100));
					if ($percentage > 99) {
						$percentage = '99.5';
					}

					echo '
					<div class="workload" data-toggle="tooltip" data-placement="left" title="My workload">
						<div class="bar" style="height: ' . $percentage . '%;"></div>
					</div>

					';

				}

				// Input for days come here

				echo '</td>';

				if ($running_day == 6) {

					echo '</tr>';

					if (($day_counter+1) != $days_in_month) {

						echo '<tr class="calendar-row">';

					}

					$running_day = -1;
					$days_in_this_week = 0;

				}

				$days_in_this_week++; $running_day++; $day_counter++;

			}

			if (($days_in_this_week > 1) && ($days_in_this_week < 8)) {

				for ($x = 1; $x <= (8 - $days_in_this_week); $x++) {

					echo '<td class="calendar-day-np"> </td>';

				}

			}

			?>

		</tr>

	</table>

</div>

<!-- Edit a day using the calendar -->

<div class="modal fade" id="#MyRequests" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Insert your worklog for <span class="my-date"></span></h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="start">I started working around</label>
							<select id="start" class="start-hour" name="start-hour">
								<option value="00">00</option>
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select>

							<select class="start-minute" name="start-minute">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="end">I ended working around</label>
							<select id="end" class="end-hour" name="end-hour">
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select>

							<select class="end-minute" name="end-minute">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>
							
						</div>

					</div>

					<input type="hidden" name="hidden-date" class="hidden-date" value="">

				</div>

				<div class="modal-divider"></div>

				<h4>My logged hours</h4>

				<div class="my-logs"></div>

			</div>

			<div class="modal-footer">
				
				<button type="submit" name="new-log-entry" class="submit-button new-log-entry" href="#">Save</a>

			</div>

		</div>

	</div>

</div>

<!-- My daily schedule -->

<div class="modal fade" id="thisDay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title day-date" id="myModalLabel"></h4>

			</div>

			<div class="modal-body">

				<div class="form-body daily-report"></div>

			</div>

			<div class="modal-footer">

			</div>

		</div>

	</div>

</div>

<!-- View Task -->

<div class="modal fade" id="ViewTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Task details</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="view-task-body"></div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="">
				<input type="hidden" class="task-id" value="">
				<button type="submit" name="save-task" class="submit-button save-task" href="#">Save</button>

			</div>

		</div>

	</div>

</div>