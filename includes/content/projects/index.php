<?php

	$project_id = isset($_GET['pid']) ? $_GET['pid'] : false;
	$month = isset($_GET['month']) ? $_GET['month'] : date('n');
	$year = isset($_GET['year']) ? $_GET['year'] : date('Y');
	$user_id = isset($_GET['uid']) ? $_GET['uid'] : $_SESSION['id'];
	$mode = isset($_GET['mode']) ? $_GET['mode'] : false;
	$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : false;

	// Options
	$sort = isset($_GET['sort']) ? $_GET['sort'] : false;
	$sort2 = isset($_GET['sort2']) ? $_GET['sort2'] : false;
	$order = isset($_GET['order']) ? $_GET['order'] : false;
	$order2 = isset($_GET['order2']) ? $_GET['order2'] : false;
	$filter = isset($_GET['filter']) ? $_GET['filter'] : false;

	$file = isset($_GET['file']) ? $_GET['file'] : false;

	if (!empty($file)) {
		header("location: index.php?pid=" . $project_id . "#files");
		exit();
	}

	if (!empty($redirect)) {
		if ($redirect == 'bugs') {
			header("location: index.php?pid=" . $project_id . "#bugs");
		} else 

		if ($redirect == 'timings') {
			header("location: index.php?pid=" . $project_id . "#timings");
		}
	}

	$nofilter = '';
	$myfilter = '';
	$module_asc = '';
	$module_desc = '';
	$task_asc = '';
	$task_desc = '';

	if (!$filter && !$sort && !$sort2 && !$order && !$order2) {
		$nofilter = '<span class="icon icon-checkbox-checked"></span>';
	}

	if ($filter) {
		$myfilter = '<span class="icon icon-checkbox-checked"></span>';
	}

	if ($sort == 'module' && $order == 'asc') {
		$module_asc = '<span class="icon icon-checkbox-checked"></span>';
	} else

	if ($sort == 'module' && $order == 'desc') {
		$module_desc = '<span class="icon icon-checkbox-checked"></span>';
	}

	if ($sort2 == 'task' && $order2 == 'asc') {
		$task_asc = '<span class="icon icon-checkbox-checked"></span>';
	} else
	if ($sort2 == 'task' && $order2 == 'desc') {
		$task_desc = '<span class="icon icon-checkbox-checked"></span>';
	}

?>

<?php

	if (isset($_POST['create-note'])) {

		$message = $con->real_escape_string($_POST['message']);

		$pattern = array('/#(\w+)/', '/@(\w+)/');
		$replacement = array('<span class="hashtag">#$1</span>', '<span class="mention">@$1</span>');
		$new_message = preg_replace($pattern, $replacement, $message);

		// var_dump($new_message);

		preg_match_all('/(?<!\w)#\w+/', $new_message, $hash);
		preg_match_all('/(?<!\w)@\w+/', $new_message, $ment);

		$hashtags = str_replace(array('.', ',', '?', '!', ';', ':', '=', '(', ')'), "", $hash);
		$mentions = str_replace(array('.', ',', '?', '!', ';', ':', '=', '(', ')'), "", $ment);

		// var_dump($hashtags);
		// var_dump($mentions);

		$insert_comment = "INSERT INTO `projects_notes` (`message`, `project_id`, `created_by`, `created_at`) VALUES ('" . $new_message . "', '" . $project_id . "', '" . $_SESSION['id'] . "', now())";
		$con->query($insert_comment);

		$inserted_id = mysqli_insert_id($con);

		foreach ($hashtags as $match) {
			
			foreach ($match as $tag) {

				$tag = ltrim($tag, '#');

				$sql = "SELECT * FROM `projects` WHERE `name` = '" . $tag . "'";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					if ($count == 1) {

					}

				}

			}

		}

		foreach ($mentions as $match) {

			foreach ($match as $mention) {

				$mention = ltrim($mention, '@');

				$sql = "SELECT * FROM `users` WHERE `name` = '" . $mention . "'";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					if ($count > 0) {

						while ($user = $result->fetch_object()) {

						    $to = $user->email;
							$from = "info@time2change.com";
							$subject = "You have been mentioned in a comment";
							
							// Write the contents of your e-mail here using HTML code
							$message = '
							<h1>Hey ' . ucfirst($user->name) . '</h1>
							<h2>Someone mentioned you</h2>
							<p>We noticed that someone mentioned you in a comment. You can <a href="http://pmt.time2change.com/index.php?pid=' . $project_id . '&redirect=true&uid=' . $user->id . '#notes">click here</a> to view the comment if you\'d like.</p>
							<p>Remember that you can always toggle of the mailing via <a href="http://pmt.time2change.com/index.php?cat=settings">your settings</a></p>
							<br><br>
							<p>PMT &copy; Time2Change</p>
							';
										
							$headers = "From: " . $from . "\r\n";
							$headers .= "Content-type: text/html\r\n";
							$to = $to;
							
							// Send the actual mail to the users
							mail($to, $subject, $message, $headers);

						}

					}

				}

			}

		}

		header("location: index.php?pid=" . $project_id . "#notes");
		exit();

	} else 

	if (isset($_POST['upload-project-files'])) {

		$files = isset($_FILES['upload']) ? $_FILES['upload'] : false;
		$_FILES['upload']['tmp_name'][0] = isset($_FILES['upload']['tmp_name'][0]) ? $_FILES['upload']['tmp_name'][0] : false;
		$level = escape($_POST['level']);
		// $folder = escape($_POST['folder']);
		$type = '';

		$attach_to = explode("/", $level);

		if ($attach_to[0] == 'module') {
			$type = '0';
		} else

		if ($attach_to[0] == 'task') {
			$type = '1';
		}

		// var_dump($attach_to);

		if ((file_exists($_FILES['upload']['tmp_name'][0])) || (is_uploaded_file($_FILES['upload']['tmp_name'][0]))) {

			for ($i = 0; $i < count($_FILES['upload']['name']); $i++) {

				$file_name 	= $_FILES['upload']['name'][$i];
				$file_tmp 	= $_FILES['upload']['tmp_name'][$i];
				$file_size 	= $_FILES['upload']['size'][$i];
				$file_error = $_FILES['upload']['error'][$i];

				 if ($file_error === 0) {

					$sql4 = "SELECT `id` FROM `projects` WHERE `id` = '". $project_id ."'";

					if ($res = $con->query($sql4)) {

						while ($projects = $res->fetch_object()) {

							$directory = 'files/uploads/projects/'. strtolower($projects->id) .'';

							$extension = explode('.', $file_name);

							$extension = strtolower(end($extension));

							if (strlen($extension) > 1) {

								$new_name = uniqid('', false) .'.'. $extension;

							}

							$filename = $directory.'/'. $new_name;

							if (!file_exists($directory)) {

								mkdir($directory, 0777, true);

							}

							move_uploaded_file($file_tmp, $filename);

							$file_path = $filename;

							$sql = "INSERT INTO `projects_files` (`name`, `project_id`, `type`, `type_id`, `href`, `created_at`, `created_by`) VALUES ('" . $_FILES['upload']['name'][$i] . "', '". $project_id ."', '" . $type . "', '" . $attach_to[1] . "', '". $file_path ."', now(), '" . $_SESSION['id'] . "')";

							$con->query($sql);

						}

					}

				}

			}

		}

	} else 

	if (isset($_POST['create-bug'])) {

		$project = $_POST['project-id'];
		$name = htmlentities($_POST['bugname']);
		$responsible = htmlentities($_POST['responsible5']);
		$deadline = date("Y-m-d", strtotime($_POST['deadline5']));
		$category = htmlentities($_POST['category2']);
		$path = htmlentities($_POST['path']);
		$description = mysqli_real_escape_string($con, $_POST['description5']);
		$files = isset($_FILES['choose_file']) ? $_FILES['choose_file'] : false;
		$_FILES['choose_file']['tmp_name'][0] = isset($_FILES['choose_file']['tmp_name'][0]) ? $_FILES['choose_file']['tmp_name'][0] : false;
		$important = '';

		if ($_POST['important3'] != '1') {
			$important = '0';
		} else {
			$important = '1';
		}

		if ($deadline == '1970-01-01') {
			$deadline == '0000-00-00';
		}

		if (empty($name)) {
		} else
		if (empty($responsible)) { 
		} else
		if (empty($description)) {
		} else {

			$sql = "INSERT INTO `projects_bugs` (`name`, `path`, `deadline`, `category`, `description`, `important`, `responsible`, `project_id`, `status`, `created_by`, `created_at`) VALUES ('" . $name . "', '" . $path . "', '" . $deadline . "', '" . $category . "', '" . $description . "', '" . $important . "', '" . $responsible . "', '" . $project . "', '5', '" . $_SESSION['id'] . "', now() )";
			$con->query($sql);

			$inserted_id = mysqli_insert_id($con);

			$sql7 = "INSERT INTO `projects_bugs_log` (`type`, `sender`, `receiver`, `bug_id`, `date`) VALUES ('0', '" . $_SESSION['id'] . "', '" . $responsible . "', '" . $inserted_id . "', now())";
			$con->query($sql7);

			$sql3 = "SELECT * FROM `users` WHERE `id` = '" . $responsible . "'";

			if ($result3 = $con->query($sql3)) {

				while ($user = $result3->fetch_object()) {

					$sql5 = "SELECT * FROM `users` WHERE `id` = '" . $_SESSION['id'] . "'";

					if ($result5 = $con->query($sql5)) {

						while ($assigner = $result5->fetch_object()) {

							$sql6 = "SELECT * FROM `projects` WHERE `id` = '" . $project . "'";

							if ($result6 = $con->query($sql6)) {

								while ($project2 = $result6->fetch_object()) {

									$to = $user->email;
									$from = "info@time2change.com";
									$subject = "#" . $inserted_id . " - New bug created: " . $name . "";
									
									// Write the contents of your e-mail here using HTML code
									$message = '
									<h1>' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . '</h1>
									<h2>You are now responsible for task #' . $inserted_id . '</h2>
									<p>' . ucfirst($assigner->name) . ' ' . ucfirst($assigner->surname) . ' created a new bug in <strong>' . ucfirst($project2->name) . '</strong> and assigned you as responsible.</p>
									<p>View all the <a href="http://pmt.time2change.com/index.php?pid=' . $project . '&redirect=true&uid=' . $user->id . '#bugs">bugs</a> in ' . ucfirst($project2->name) . '</p>
									<br><br>
									<p>PMT &copy; Time2Change</p>
									';
												
									$headers = "From: $from\r\n";
									$headers .= "Content-type: text/html\r\n";
									$to = $to;
									
									// Send the actual mail to the users
									mail($to, $subject, $message, $headers);

								}

							}

						}

					}

				}

			}

			if ((file_exists($_FILES['choose_file']['tmp_name'][0])) || (is_uploaded_file($_FILES['choose_file']['tmp_name'][0]))) {

				for ($i = 0; $i < count($_FILES['choose_file']['name']); $i++) {

					$file_name 	= $_FILES['choose_file']['name'][$i];
					$file_tmp 	= $_FILES['choose_file']['tmp_name'][$i];
					$file_size 	= $_FILES['choose_file']['size'][$i];
					$file_error = $_FILES['choose_file']['error'][$i];

					 if ($file_error === 0) {

						$sql4 = "SELECT `id` FROM `projects` WHERE `id` = '". $project ."'";

						if ($res = $con->query($sql4)) {

							while ($projects = $res->fetch_object()) {

								$directory = 'files/uploads/bugs/projects/'. strtolower($projects->id) .'';

								$extension = explode('.', $file_name);

								$extension = strtolower(end($extension));

								if (strlen($extension) > 1) {

									$new_name = uniqid('', false) .'.'. $extension;

								}

								$filename = $directory.'/'. $new_name;

								if (!file_exists($directory)) {

									mkdir($directory, 0777, true);

								}

								move_uploaded_file($file_tmp, $filename);

								$file_path = $filename;

								$sql2 = "INSERT INTO `projects_bugs_files` (`name`, `path`, `bug_id`, `created_at`, `created_by`) VALUES ('" . $_FILES['choose_file']['name'][$i] . "', '". $file_path ."', '" . $inserted_id . "', now(), '" . $_SESSION['id'] . "')";

								$con->query($sql2);

							}

						}

					}

				}

			}

			header("location: index.php?pid=" . $project_id . "");
			exit();

		}

	} else

	if (isset($_POST['save-edit-bug'])) {
		$bug_id = $_POST['bug-id'];
		$name = htmlentities($_POST['bugname2']);
		$responsible = htmlentities($_POST['responsible6']);
		$deadline = date("Y-m-d", strtotime($_POST['deadline6']));
		$category = htmlentities($_POST['category3']);
		$path = htmlentities($_POST['path2']);
		$description = mysqli_real_escape_string($con, $_POST['description6']);
		$files = isset($_FILES['choose_file2']) ? $_FILES['choose_file2'] : false;
		$_FILES['choose_file2']['tmp_name'][0] = isset($_FILES['choose_file2']['tmp_name'][0]) ? $_FILES['choose_file2']['tmp_name'][0] : false;
		$important = '';

		if ($_POST['important4'] != '1') {
			$important = '0';
		} else {
			$important = '1';
		} 

		if ($deadline == '1970-01-01') {
			$deadline == '0000-00-00';
		} else {
			$deadline = $deadline . '23:59:59';
		}

		if (empty($name)) {
		} else
		if (empty($responsible)) { 
		} else
		if (empty($description)) {
		} else {

			$sql = "UPDATE `projects_bugs` SET `name` = '" . $name . "', `path` = '" . $path . "', `deadline` = '" . $deadline . "', `category` = '" . $category . "', `description` = '" . $description . "', `important` = '" . $important . "', `responsible` = '" . $responsible . "', `project_id` = '" . $project_id . "', `updated_by` = '" . $_SESSION['id'] . "', `updated_at` = now() WHERE `id` = '" . $bug_id . "'";
			$con->query($sql);

			if ((file_exists($_FILES['choose_file2']['tmp_name'][0])) || (is_uploaded_file($_FILES['choose_file2']['tmp_name'][0]))) {

				for ($i = 0; $i < count($_FILES['choose_file2']['name']); $i++) {

					$file_name 	= $_FILES['choose_file2']['name'][$i];
					$file_tmp 	= $_FILES['choose_file2']['tmp_name'][$i];
					$file_size 	= $_FILES['choose_file2']['size'][$i];
					$file_error = $_FILES['choose_file2']['error'][$i];

					 if ($file_error === 0) {

						$sql4 = "SELECT `id` FROM `projects` WHERE `id` = '". $project_id ."'";

						if ($res = $con->query($sql4)) {

							while ($projects = $res->fetch_object()) {

								$directory = 'files/uploads/bugs/projects/'. strtolower($projects->id) .'';

								$extension = explode('.', $file_name);

								$extension = strtolower(end($extension));

								if (strlen($extension) > 1) {

									$new_name = uniqid('', false) .'.'. $extension;

								}

								$filename = $directory.'/'. $new_name;

								if (!file_exists($directory)) {

									mkdir($directory, 0777, true);

								}

								move_uploaded_file($file_tmp, $filename);

								$file_path = $filename;

								$sql2 = "INSERT INTO `projects_bugs_files` (`name`, `path`, `bug_id`, `created_at`, `created_by`) VALUES ('" . $_FILES['choose_file']['name'][$i] . "', '". $file_path ."', '" . $inserted_id . "', now(), '" . $_SESSION['id'] . "')";

								$con->query($sql2);

							}

						}

					}

				}

			}

			$_SESSION['message'] = 'The bug has been successfuly edited';

			header("location: index.php?pid=" . $project_id . "&redirect=bugs");
			exit();

		}

	}

?>
<?php

	if (isset($_SESSION['message'])) {

		echo '<div class="success-message">' . $_SESSION['message'] . '</div>';

	}

?>
<?php if (empty($project_id)) { ?>

	<h1>Projects</h1>

	<p>Browse through every project we are working on. <span class="top-links"><a href="index.php?cat=projects&act=overview&option=finished">Completed projects</a></span></p>

	<div class="section-divider"></div>

	<div class="utilities">

		<a class="success-button left" href="#" data-toggle="modal" data-target="#CreateProject">+ Add a new project</a>

	</div>

<?php } else { 

	if (empty($mode)) { ?>

	<!-- <div class="special-navigation">

		<div class="browse-calendar">

			<a href="index.php"><button class="btn-default enabled single"><span class="icon icon-reply"></span> Return to projects</button></a>

		</div>

	</div>

	<div class="section-divider"></div> -->

<?php

	} else 

	if (!empty($mode)) { 

		$sql = "SELECT `name`, `id` FROM `projects` wHERE `id` = '" . $project_id . "'";

		if ($result = $con->query($sql)) {

			while ($project = $result->fetch_object()) {

			?>

			<div class="special-navigation">

				<div class="browse-calendar">

					<a href="index.php?pid=<?php echo $project_id; ?>"><button class="btn-default enabled single"><span class="icon icon-reply"></span> Return to <?php echo ucfirst($project->name); ?></button></a>

				</div>

				<div class="choose-user">

					<button class="btn-default enabled single show-tasks">Mail planning <span class="icon icon-envelop"></span></button>

					<button class="btn-default enabled single show-tasks"><span class="to-show-tasks">Show</span><span class="to-hide-tasks hide">Hide</span> tasks <span class="icon icon-clipboard"></span></button>

					<select class="user-choice" onchange="redirectUser2();" data-project="<?php echo $project->id; ?>">

					<?php

					$sql = "SELECT * FROM `users`";

					if ($result = $con->query($sql)) {

						while ($user = $result->fetch_object()) { ?>

						<option value="<?php echo $user->id; ?>" <?=$user->id == $user_id ? ' selected="selected"' : '';?>><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

						<?php

						}

					}

					?>

					</select>

					<?php

						$previous = $month - 1;
						$previous_year = $year - 1;
						$next = $month + 1;
						$next_year = $year + 1;

						if ($month == '1') {
							echo '<a href="index.php?pid=' . $project_id . '&year=' . $previous_year . '&month=12&uid=' . $user_id . '&mode=full-screen"><button class="btn-default enabled first">&larr; </button></a>';
						} else
						if ($month > '1') {
							echo '<a href="index.php?pid=' . $project_id . '&year=' . $year . '&month=' . $previous . '&uid=' . $user_id . '&mode=full-screen"><button class="btn-default enabled first">&larr; </button></a>';
						}

						echo '<button class="btn-default middle">' . date("F", mktime(0, 0, 0, $month, 10)) . ' ' . $year . '</button>';

						if ($month == '12') {
							echo '<a href="index.php?pid=' . $project_id . '&year=' . $next_year . '&month=1&uid=' . $user_id . '&mode=full-screen"><button class="btn-default enabled last">&rarr; </button></a>';
						} else
						if ($month < '12') {
							echo '<a href="index.php?pid=' . $project_id . '&year=' . $year . '&month=' . $next . '&uid=' . $user_id . '&mode=full-screen"><button class="btn-default enabled last">&rarr; </button></a>';
						}

					?>

				</div>

			</div>

			<div class="section-divider"></div>

			<?php

			}

		}

	}

}

?>

<div class="content">

<?php

	if (empty($project_id)) { 

		if ($_SESSION['admin'] === true) {
			$sql = "SELECT * FROM `projects` ORDER BY `name` ASC";
		} else {
			$sql = "SELECT * FROM `projects` WHERE `created_by` = '" . $_SESSION['id'] . "' OR `created_by` != '" . $_SESSION['id'] . "' AND `private` = '0' ORDER BY `name` ASC";
		}

		if ($result = $con->query($sql)) {

			$count = $result->num_rows;

			if ($count == 0) { ?>

			<div class="nothing">

				<div class="image">

					<span class="icon icon-bullhorn"></span>

				</div>

				<div class="message">

					<p>Currently there are no projects</p>

				</div>

			</div>

			<?php

			} else {

				while ($project = $result->fetch_object()) { ?>

				<div class="holder">

					<div class="box">

						<span class="title"><h2><a href="index.php?pid=<?php echo $project->id; ?>"><?php echo ucfirst($project->name); ?></a></h2><?php if ($_SESSION['id'] == $project->created_by || $_SESSION['admin'] == true) { echo '<small><span class="icon icon-pencil fake-link edit-project" data-id="' . $project->id . '"></span> <span class="fake-link delete-project" data-id="' . $project->id . '"><span class="icon icon-bin"></span></span></small>'; } ?></span>

						<div class="progression">

							<?php

							$count_all_tasks = "SELECT * FROM `projects_tasks` WHERE `project_id` = '" . $project->id . "'";
							$all_tasks = $con->query($count_all_tasks)->num_rows;

							$count_closed_tasks = "SELECT * FROM `projects_tasks` WHERE `project_id` = '" . $project->id . "' AND `label` = '6' AND `completed` = '1'";
							$closed_tasks = $con->query($count_closed_tasks)->num_rows;

							if ($all_tasks == 0) {
								echo '<div class="finished-tasks-bar"></div>';
								echo '<div class="unfinished-tasks-bar" style="width: 99%;"></div>';
							} else {

								$finished_tasks = round(($closed_tasks / $all_tasks) * 100, PHP_ROUND_HALF_DOWN);
								$rest = 100 - $finished_tasks;

								if ($rest == 100) {
									$rest = 99;
								}

								echo '<div class="finished-tasks-bar" style="width: ' . $finished_tasks . '%;"></div>';
								echo '<div class="unfinished-tasks-bar" style="width: ' . $rest . '%;"></div>';

							}

							?>

						</div>

						<p class="customer2">For <?php echo ucfirst($project->customer); ?></p>

						<p><?php echo nl2br($project->description); ?></p>
						<p class="very-small"><?php if (($project->private == 1)) { ?><span class="private">Private project</span><?php } ?></p>

						<div class="extra">

							<div class="deadline2">

								<span class="icon-calendar"></span> <?php if (($project->deadline != '0000-00-00 00:00:00') && ($project->deadline != null) && ($project->deadline != '1970-01-01 00:00:00')) { echo date("D, d F Y", strtotime($project->deadline)); } else if (($project->deadline == '0000-00-00 00:00:00') || ($project->deadline == null) || ($project->deadline == '1970-01-01 00:00:00')) { echo '-'; } ?>

							</div>

							<div class="label2">

								<?php

								$sql2 = "SELECT * FROM `labels` WHERE `id` = '" . $project->label . "'";

								if ($result2 = $con->query($sql2)) {

									while ($label = $result2->fetch_object()) { ?>

									<span class="label-<?php echo strtolower(str_replace(' ', '-', $label->name)); ?>"><?php echo ucfirst($label->name); ?></label>

									<?php

									}

								}

								?>

							</div>

						</div>

					</div>

				</div>

				<?php

				}

			}

		}

	} else {

		if (empty($mode)) {

			$sql = "SELECT * FROM `projects` WHERE `id` = '" . $project_id . "'";

			if ($result = $con->query($sql)) {

				$count = $result->num_rows;

				if ($count == 0) {

					echo '<p>This project does not exists</p>';

				} else {

					while ($project = $result->fetch_object()) { ?>

					<div class="frame">

						<div class="project-heading">

							<h2><?php echo ucfirst($project->name); ?></h2>

							<div class="extra-button">

								<a href="#" data-toggle="modal" data-target="#NewModule"><button class="btn-default enabled first"><span class="icon icon-tree"></span> New module</button></a><a href="#bugs" data-toggle="modal" data-target="#NewBug"><button class="btn-default enabled middle"><span class="icon icon-bug"></span> Report a bug</button></a><a href="#" data-toggle="modal" data-target="#NewTask"><button class="btn-default enabled last"><span class="icon icon-file-text"></span>New task</button></a>

							</div>

						</div>

						<div class="sub-tab">

							<?php

							$count_open_tasks = "SELECT * FROM `projects_tasks` WHERE `project_id` = '" . $project_id . "' AND `label` != '6'";
							$count_tasks = $con->query($count_open_tasks)->num_rows;

							$count_open_bugs = "SELECT * FROM `projects_bugs` WHERE `project_id` = '" . $project_id . "' AND `status` != '6'";
							$count_bugs = $con->query($count_open_bugs)->num_rows;

							?>

							<ul class="nav nav-tabs" role="tablist">

								<li role="presentation" class="active"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab"><span class="icon2 icon-checkbox-checked"></span> Tasks (<?php echo $count_tasks; ?>)</a></li>
								<li role="presentation"><a href="#bugs" aria-controls="bugs" role="tab" data-toggle="tab"><span class="icon2 icon-bug"></span> Bugs (<?php echo $count_bugs; ?>)</a></li>
								<li><a href="index.php?pid=<?php echo $project->id; ?>&mode=full-screen"><span class="icon2 icon-calendar"></span> Scheduler</a></li>
								<li role="presentation"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab"><span class="icon2 icon-file-empty"></span> Notes</a></li>
								<li role="presentation"><a href="#files" aria-controls="files" role="tab" data-toggle="tab"><span class="icon2 icon-folder"></span> Files</a></li>
								<li role="presentation"><a href="#timings" aria-controls="timings" role="tab" data-toggle="tab"><span class="icon2 icon-clock"></span> Timings</a></li>

							</ul>

						</div>

						<div class="project-content">

							<div class="project-references full-references">

								<div class="tab-content">

									<div role="tabpanel" class="tab-pane active" id="tasks">

										<div class="top-nav">

											<ul class="options">

												<li><a href="#" class="transfer-to"><span class="icon icon-tab"></span> Transfer selected to</a></li>
												<li class="filters"><span class="icon icon-magic-wand"></span> More options</li>

											</ul>

										</div>

										<div class="below_top-nav">

											<div class="toggle-options">

												<ul>

													<li><a href="index.php?pid=<?php echo $project_id; ?>">Default order <?php echo $nofilter; ?></a></li>
													<li><a href="index.php?pid=<?php echo $project_id; ?>&sort=<?php echo $sort; ?>&order=<?php echo $order; ?>&sort2=<?php echo $sort2; ?>&order2=<?php echo $order2; ?>&filter=<?php echo $_SESSION['id']; ?>">Filter my tasks <?php echo $myfilter; ?></a></li>
													<li><a href="index.php?pid=<?php echo $project_id; ?>&sort=module&order=asc&sort2=<?php echo $sort2; ?>&order2=<?php echo $order2; ?>&filter=<?php echo $filter; ?>">Sort Module A-Z <?php echo $module_asc; ?></a></li>
													<li><a href="index.php?pid=<?php echo $project_id; ?>&sort=module&order=desc&sort2=<?php echo $sort2; ?>&order2=<?php echo $order2; ?>&filter=<?php echo $filter; ?>">Sort Module Z-A <?php echo $module_desc; ?></a></li>
													<li><a href="index.php?pid=<?php echo $project_id; ?>&sort2=task&order2=asc&sort=<?php echo $sort; ?>&order=<?php echo $order; ?>&filter=<?php echo $filter; ?>">Sort Tasks A-Z <?php echo $task_asc; ?></a></li>
													<li><a href="index.php?pid=<?php echo $project_id; ?>&sort2=task&order2=desc&sort=<?php echo $sort; ?>&order=<?php echo $order; ?>&filter=<?php echo $filter; ?>">Sort Tasks Z-A <?php echo $task_desc; ?></a></li>

												</ul>

											</div>

										</div>

										<?php

										$module_filter = '';

										if ($sort == 'module') {
											if ($order == 'asc') {
												$module_filter = ' ORDER BY `name` ASC';
											} else
											if ($order == 'desc') {
												$module_filter = ' ORDER BY `name` DESC';
											}
										}

										$task_filter = '';

										if ($sort2 == 'task') {
											if ($order2 == 'asc') {
												$task_filter = ' ORDER BY `name` ASC';
											} else
											if ($order2 == 'desc') {
												$task_filter = ' ORDER BY `name` DESC';
											}
										}

										$user_filter = '';

										if ($filter) {
											$user_filter = " AND `responsible` = '" . $filter . "'";
										}

										$sql = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project_id . "'" . $module_filter . "";

										if ($result = $con->query($sql)) {

											while ($module = $result->fetch_object()) {

												if (($module->created_by == $_SESSION['id']) || ($_SESSION['admin'] === true)) {

													$edit_module = '<span class="icon icon-pencil edit-module" data-id="' . $module->id . '" data-project="' . $project_id . '"></span>';
													$delete_module = '<span class="icon icon-bin delete-module" data-id="' . $module->id . '" data-project="' . $project_id . '"></span>';

												} ?>

												<div class="module-heading">
													<h3>
														<span class="large-icon icon-folder"></span> <?php echo ucfirst($module->name); ?><span class="icons"><?php echo $edit_module; echo $delete_module; ?></span>
													</h3>
												</div>
												<ul class="task-container">

												<?php

												$sql2 = "SELECT * FROM `projects_tasks` WHERE `module_id` = '" . $module->id . "'" . $user_filter . "" . $task_filter . "";

												if ($result2 = $con->query($sql2)) {

													$count2 = $result2->num_rows;

													if ($count2 == 0) {

														echo '<div style="padding: 10px;">There are no tasks set for this module</div>';

													} else {

														while ($task = $result2->fetch_object()) {

														if (($task->responsible == $_SESSION['id']) || ($task->created_by == $_SESSION['id']) || ($_SESSION['admin'] === true)) { 

															$edit = '<span class="icon icon-pencil edit-task" data-id="' . $task->id . '" data-toggle="tooltip" data-placement="top" title="Edit"></span>';
															$delete = '<span class="icon icon-bin delete-task" data-id="' . $task->id . '" data-toggle="tooltip" data-placement="top" title="Delete"></span>';
															$timing = '<span class="icon icon-clock custom-time" data-id="' . $task->id . '" data-toggle="tooltip" data-placement="top" title="Add custom time"></span>';

														} else {

															$edit = '';
															$delete = '';
															$timing = '';

														}

														if ($task->important == '1') {
															$flag = 'subtile-highlight';
														} else {
															$flag = '';
														}

														if (($task->completed == '1')) {
															$style = 'blocked';
															$extra = 'hide-button';
														} else {
															$style = '';
															$extra = '';
														}

														?>

														<li class="holder">

															<div class="task-name <?php echo $style; ?>">

																<span class="medium-icon icon-clipboard"></span> <input type="checkbox" name="task[]" class="transfer-task" value="<?php echo $task->id; ?>"><a href="#" class="view-task" data-id="<?php echo $task->id; ?>"><?php echo '#' . $task->id; ?> <?php if (strlen($task->name) > 30) { echo ucfirst(substr($task->name, 0, 30)). '...'; } else { echo ucfirst($task->name); } ?></a> <span class="icons"><?php echo $timing; ?> <?php echo $edit; ?> <?php echo $delete; ?></span>

															</div>

															<div class="task-category">

																<?php

																$sql3 = "SELECT * FROM `projects_categories` WHERE `id` = '" . $task->category . "'";

																if ($result3 = $con->query($sql3)) {

																	while ($category = $result3->fetch_object()) {

																		echo '<strong>' . ucfirst($category->name) .'</strong>';

																	}

																}

																?>

															</div>

															<div class="task-deadline">

																<?php

																if ($task->completed == '1') { ?>

																	&nbsp;

																<?php } else { ?>

																	<?php echo time_left($task->deadline); ?>

																<?php } ?>

															</div>

															<div class="task-label">

																<?php

																$sql4 = "SELECT * FROM `labels` WHERE `id` = '" . $task->label . "'";

																if ($result4 = $con->query($sql4)) {

																	while ($label = $result4->fetch_object()) {

																		echo '<span class="' . str_replace(' ', '-', $label->name) . '">' . ucfirst($label->name) . '</span>';

																	}

																}

																if (($task->label == '6') && ($_SESSION['id'] == $task->created_by)) {

																	echo '<span class="completed mark-complete ' . $extra . '" data-id="' . $task->id . '" data-project="' . $project->id . '" data-toggle="tooltip" data-placement="top" title="Completed!"><span class="icon icon-checkmark"></span></span> <span class="not-completed mark-incomplete" data-id="' . $task->id . '" data-project="' . $project->id . '" data-toggle="tooltip" data-placement="top" title="Incomplete!"><span class="icon icon-blocked"></span></span>';

																}

																?>

															</div>

															<div class="task-responsible">

																<?php

																$sql5 = "SELECT * FROM `users` WHERE `id` = '" . $task->responsible . "'";

																if ($result5 = $con->query($sql5)) {

																	while ($user = $result5->fetch_object()) {

																		echo ucfirst($user->name) . ' ' . ucfirst($user->surname); 

																	}

																}

																?>

															</div>

															<?php

															$sql6 = "SELECT * FROM `projects_tasks_milestones` WHERE `task_id` = '" . $task->id . "'";

															if ($result6 = $con->query($sql6)) {

																if ($result6->num_rows > 0) {

																	echo '<ul class="milestone" style="clear: left;">';

																}

																while ($milestone = $result6->fetch_object()) {

																	echo '<li><span class="icon icon-flag"></span> <span class="milestone-name">' . ucfirst($milestone->name) . '</span><span class="milestone-deadline">' . time_left($milestone->deadline) . '</span></li>';

																}

																if ($result6->num_rows > 0) {

																	echo '</ul>';

																}

															}

															?>

														</li>

														<?php

														}

													}

												}

												echo '</ul>';

											}

										}

										?>

											<div class="module-heading"><h3>Temporary Module</h3></div>
											<div class="task-container">

											<?php

											$sql2 = "SELECT * FROM `projects_tasks` WHERE `module_id` = '0' AND `project_id` = '" . $project_id . "'";

											if ($result2 = $con->query($sql2)) {

												$count2 = $result2->num_rows;

												if ($count2 == 0) {

													echo '<div style="padding: 10px;">This is a temporary module without options</div>';

												} else {

													while ($task = $result2->fetch_object()) {

													if (($task->responsible == $_SESSION['id']) || ($task->created_by == $_SESSION['id'])) {

														$edit = '<span class="icon icon-pencil edit-task" data-id="' . $task->id . '"></span>';
														$delete = '<span class="icon icon-bin delete-task" data-id="' . $task->id . '"></span>';

													} else {

														$edit = '';
														$delete = '';

													}

													if ($task->important == '1') {
														$flag = 'subtile-highlight';
													} else {
														$flag = '';
													}

													?>

													<div class="holder <?php echo $flag; ?>">

														<div class="task-name">

															<input type="checkbox" name="task[]" class="transfer-task" value="<?php echo $task->id; ?>"><a href="#" class="view-task" data-id="<?php echo $task->id; ?>"><?php echo '#' . $task->id; ?> <?php if (strlen($task->name) > 30) { echo ucfirst(substr($task->name, 0, 30)). '...'; } else { echo ucfirst($task->name); } ?></a> <span class="icons"><?php echo $edit; ?> <?php echo $delete; ?></span>

														</div>

														<div class="task-category">

															<?php

															$sql3 = "SELECT * FROM `projects_categories` WHERE `id` = '" . $task->category . "'";

															if ($result3 = $con->query($sql3)) {

																while ($category = $result3->fetch_object()) {

																	echo '<strong>' . ucfirst($category->name) .'</strong>';

																}

															}

															?>

														</div>

														<div class="task-deadline">

															<?php echo time_left($task->deadline); ?>

														</div>

														<div class="task-label">

															<?php

															$sql4 = "SELECT * FROM `labels` WHERE `id` = '" . $task->label . "'";

															if ($result4 = $con->query($sql4)) {

																while ($label = $result4->fetch_object()) {

																	echo '<span class="' . str_replace(' ', '-', $label->name) . '">' . ucfirst($label->name) . '</span>';

																}

															}

															?>

														</div>

														<div class="task-responsible">

															<?php

															$sql5 = "SELECT * FROM `users` WHERE `id` = '" . $task->responsible . "'";

															if ($result5 = $con->query($sql5)) {

																while ($user = $result5->fetch_object()) {

																	echo ucfirst($user->name) . ' ' . ucfirst($user->surname); 

																}

															}

															?>

														</div>

													</div>

													<?php

													}

												}

											}

											echo '</div>';

										?>

									</div>

									<div role="tabpanel" class="tab-pane" id="bugs">

										<?php

										echo '<div class="module-heading"><h3>Reported bugs</h3></div>';
										echo '<div class="task-container">';

										$sql2 = "SELECT * FROM `projects_bugs` WHERE `project_id` = '" . $project->id . "'";

										if ($result2 = $con->query($sql2)) {

											$count2 = $result2->num_rows;

											if ($count2 == 0) {

												echo '<div style="padding: 10px;">There are no tasks set for this module</div>';

											} else {

												while ($bug = $result2->fetch_object()) {

												if (($bug->created_by == $_SESSION['id']) || ($_SESSION['admin'] === true)) { 

													$edit = '<span class="icon icon-pencil edit-bug" data-id="' . $bug->id . '" data-toggle="tooltip" data-placement="top" title="Edit"></span>';
													$delete = '<span class="icon icon-bin delete-bug" data-id="' . $bug->id . '" data-toggle="tooltip" data-placement="top" title="Delete"></span>';

												} else {

													$edit = '';
													$delete = '';

												}

												if ($bug->important == '1') {
													$flag = 'subtile-highlight';
												} else {
													$flag = '';
												}

												if (($bug->completed == '1')) {
													$first_tag = '<strike>';
													$last_tag = '</strike>';
													$style = 'style="pointer-events: none; cursor: default;"';
													$extra = 'hide-button';
												} else {
													$first_tag = '';
													$last_tag = '';
													$style = '';
													$extra = '';
												}

												?>

												<div class="holder <?php echo $flag; ?>">

													<div class="task-name">

														<input type="checkbox" name="bug[]" class="transfer-bug" value="<?php echo $bug->id; ?>"><?php echo $first_tag; ?><a href="#bugs" <?php echo $style; ?> class="view-bug" data-id="<?php echo $bug->id; ?>"><?php echo '#' . $bug->id; ?> <?php if (strlen($bug->name) > 30) { echo ucfirst(substr($bug->name, 0, 30)). '...'; } else { echo ucfirst($bug->name); } ?></a><?php echo $last_tag; ?> <span class="icons"><?php echo $edit; ?> <?php echo $delete; ?></span>

													</div>

													<div class="task-category">

														<?php echo $first_tag; ?><strong><?php if ($bug->category == '') { echo '-'; } else { echo ucfirst($bug->category); } ?></strong><?php echo $last_tag; ?>

													</div>

													<div class="task-deadline">

														<?php echo $first_tag; ?><?php echo time_left($bug->deadline); ?><?php echo $last_tag; ?>

													</div>

													<div class="task-label">

														<?php

														$sql4 = "SELECT * FROM `labels` WHERE `id` = '" . $bug->status . "'";

														if ($result4 = $con->query($sql4)) {

															while ($label = $result4->fetch_object()) {

																echo '<span class="' . str_replace(' ', '-', $label->name) . '">' . ucfirst($label->name) . '</span>';

															}

														}

														if (($bug->status == '6') && ($_SESSION['id'] == $bug->created_by)) {

															echo '<span class="completed mark-complete-bug ' . $extra . '" data-id="' . $bug->id . '" data-project="' . $project->id . '" data-toggle="tooltip" data-placement="top" title="Completed!"><span class="icon icon-checkmark"></span></span> <span class="not-completed mark-incomplete-bug" data-id="' . $bug->id . '" data-project="' . $project->id . '" data-toggle="tooltip" data-placement="top" title="Incomplete!"><span class="icon icon-blocked"></span></span>';

														}

														?>

													</div>

													<div class="task-responsible">

														<?php

														$sql5 = "SELECT * FROM `users` WHERE `id` = '" . $bug->responsible . "'";

														if ($result5 = $con->query($sql5)) {

															while ($user = $result5->fetch_object()) {

																echo $first_tag . ' ' . ucfirst($user->name) . ' ' . ucfirst($user->surname) .' ' . $last_tag; 

															}

														}

														?>

													</div>

												</div>

												<?php

												}

											}

										}

										echo '</div>';

										?>

									</div>

									<div role="tabpanel" class="tab-pane" id="notes">

										<div class="take-note">

											<form action="index.php?pid=<?php echo $project_id; ?>#notes" method="POST">

												<textarea class="autocomplete-pt" name="message" placeholder="Write a note"></textarea>
												<div class="suggest-box"></div>

												<input type="submit" name="create-note" value="Submit">

											</form>

										</div>

										<div class="notes">

											<?php

											$sql = "SELECT * FROM `projects_notes` WHERE `project_id` = '" . $project_id . "' ORDER BY `created_by` DESC";

											if ($result = $con->query($sql)) {

												$count = $result->num_rows;

												if ($count == 0) { ?>

												<div class="nothing">

													<div class="image">

														<span class="icon icon-bullhorn"></span>

													</div>

													<div class="message">

														<p>Currently there are no notes</p>

													</div>

												</div>

												<?php

												} else {

													while ($note = $result->fetch_object()) {

														$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $note->created_by . "'";

														if ($result2 = $con->query($sql2)) {

															while ($user = $result2->fetch_object()) { ?>

															<div class="comment">

																<p class="details"><strong><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></strong> on <?php echo date("D, d F Y", strtotime($note->created_at)); ?></p>
																<p class="message"><?php echo $note->message; ?></p>

															</div>

															<?php

															}

														}

													}

												}

											}

											?>

										</div>

									</div>

									<div role="tabpanel" class="tab-pane" id="files">

<!--
										<div class="top-nav floating">

											<ul class="options">

												<li><span class="upload-files transfer-file-to"><span class="icon icon-tab"></span> Move selected to</span></li>
												<li><span class="upload-files create-folder"><span class="icon icon-folder-plus"></span> New Folder</span></li>

											</ul>

										</div>
-->

										<div class="upload-form floating2">

											<form action="" method="post" enctype="multipart/form-data">

												<button type="button" id="chooseFile">Choose files... <span class="selected-files"></span></button>
												<span style="display: none;"><input class="hide" id="uploadfiles" name="upload[]" type="file" id="upload" multiple></span>
												<select id="level" name="level" class="level">
													<option value="">Attach file to...</option>
													<option value="">---</option>
												<?php

												$sql = "SELECT * FROM `projects` WHERE `id` = '" . $project_id . "'";

												if ($result = $con->query($sql)) {

													while ($project2 = $result->fetch_object()) {

														echo '<optgroup label="' . ucfirst($project2->name) . '">';

														$sql2 = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project2->id . "'";

														if ($result2 = $con->query($sql2)) {

															while ($module = $result2->fetch_object()) {

																echo '<option value="module/' . $module->id . '">&nbsp; &nbsp; ' . ucfirst($module->name) . '</option>';

																$sql3 = "SELECT * FROM `projects_tasks` WHERE `module_id` = '" . $module->id . "'";

																if ($result3 = $con->query($sql3)) {

																	while ($task = $result3->fetch_object()) {

																		echo '<option value="task/' . $task->id . '">&nbsp; &nbsp; &nbsp; &nbsp; ' . ucfirst($task->name) . '</option>';

																	}

																}

															}

														}

														echo '<option value="module/0">&nbsp; &nbsp; Other</option>';

														$sql4 = "SELECT * FROM `projects_tasks` WHERE `module_id` = '0' AND `project_id` = '" . $project_id . "'";

														if ($result4 = $con->query($sql4)) {

															while ($no_module_tasks = $result4->fetch_object()) {

																echo '<option value="task/' . $no_module_tasks->id . '">&nbsp; &nbsp; &nbsp; &nbsp; ' . ucfirst($no_module_tasks->name) . '</option>';

															}

														}

														echo '</optgroup>';

													}

												}

												?>
												</select>
<!--
												<select name="folder">
													<option value="">Place into...</option>
													<option value="">---</option>

													<?php

													$sql = "SELECT * FROM `projects_folders` WHERE `project_id` = '" . $project_id . "'";

													if ($result = $con->query($sql)) {

														while ($folder = $result->fetch_object()) {

															echo '<option value="' . $folder->id . '">' . ucfirst($folder->name) . '</option>';

														}

													}

													?>

												</select>
-->

												<button class="submit-button" type="submit" name="upload-project-files"><span class="icon icon-upload"></span> Upload</button>

											</form>

										</div>

										<div class="file-folder">
<!--
											<?php

											$sql = "SELECT * FROM `projects_folders` WHERE `project_id` = '" . $project_id . "'";

											if ($result = $con->query($sql)) {

												$count = $result->num_rows;

												while ($folder = $result->fetch_object()) { ?>
												
												<a href="index.php?pid=<?php echo $project_id; ?>#files&folder=<?php echo $folder->id; ?>">
												<div class="folder">

													<div class="folder-icon"></div>

													<div class="folder-name">

														<p><?php echo ucfirst($folder->name); ?></p>

													</div>

												</div>
												</a>

												<?php

												}

											}

											?>

-->

											<?php

											$sql = "SELECT * FROM `projects_files` WHERE `project_id` = '" . $project_id . "'";

											if ($result = $con->query($sql)) {

												$count = $result->num_rows;

												if ($count == 0) { ?>

												<div class="nothing">

													<div class="image">

														<span class="icon icon-bullhorn"></span>

													</div>

													<div class="message">

														<p>Currently there are no files attached to this project</p>

													</div>

												</div>

												<?php

												} else {

													while ($file = $result->fetch_object()) { 

														$filetype = explode(".", $file->name);

													?>

													<div class="file">

														<div class="file-image">

															<?php

															if (($filetype[1] == 'php')) {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label php">PHP</span>';

															} else

															if (($filetype[1] == 'css') || ($filetype[1]) == 'scss') {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label css">CSS</span>';

															} else 

															if (($filetype[1] == 'html')) {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label html">HTML</span>';

															} else 

															if (($filetype[1] == 'js')) {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label js">JS</span>';

															} else

															if (($filetype[1] == 'htaccess')) {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label htaccess">HTACCESS</span>';

															} else 

															if (($filetype[1] == 'sql')) {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label sql">SQL</span>';

															} else

															if (($filetype[1] == 'png')) {

																echo '<span class="icon icon-file-picture"></span><br>';
																echo '<span class="label png">PNG</span>';

															} else

															if (($filetype[1] == 'bmp')) {

																echo '<span class="icon icon-file-picture"></span><br>';
																echo '<span class="label bmp">BMP</span>';

															} else 

															if (($filetype[1] == 'jpeg') || ($filetype[1] == 'jpg')) {

																echo '<span class="icon icon-file-picture"></span><br>';
																echo '<span class="label jpg">JPG</span>';

															} else

															if (($filetype[1] == 'gif')) {

																echo '<span class="icon icon-file-picture"></span><br>';
																echo '<span class="label gif">GIF</span>';

															} else

															if (($filetype[1] == 'ttf')) {

																echo '<span class="icon icon-file-picture"></span><br>';
																echo '<span class="label ttf">TTF</span>';

															} else

															if (($filetype[1] == 'xlsx') || ($filetype[1] == 'xls')) {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label xls">XLS</span>';

															} else

															if (($filetype[1] == 'doc') || ($filetype[1] == 'docx')) {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label doc">DOC</span>';

															} else

															if (($filetype[1] == 'pdf')) {

																echo '<span class="icon icon-file-empty"></span><br>';
																echo '<span class="label pdf">PDF</span>';

															}

															?>

														</div>

														<div class="file-details">

															<p>

																<a href="<?php echo $file->href; ?>" download target="_blank"><?php echo ucfirst($file->name); ?></a>

																<!-- <span class="delete-file" data-id="<?php echo $file->id; ?>"><span class="icon icon-bin"></span></span> -->

															</p>

															<p style="font-size: 12px;">

																<?php 

																$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $file->created_by . "'";

																if ($result2 = $con->query($sql2)) {

																	while ($user = $result2->fetch_object()) {

																		echo '' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . ', ' . date("l, d F Y", strtotime($file->created_at)) . ' | ';

																	}

																}

																?>
																<em><span style="color: #c1c1c1;"><?php echo filesize($file->href); ?> Bytes</span></em>

															</p>

														</div>

													</div>

												<?php

													}

												}

											}

											?>

										</div>

									</div>

									<div role="tabpanel" class="tab-pane" id="timings">

										<table class="timings" width="100%">

											<?php

											$sql = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project_id . "'";

											if ($result = $con->query($sql)) {

												while ($module = $result->fetch_object()) { ?>

												<thead>

													<th colspan="3">

														<span style="text-transform: uppercase;"><?php echo ucfirst($module->name); ?></span>

													</th>
													
													<th colspan="4" style="text-align: right;">

														<?php

														$query2 = "SELECT pt.module_id, SEC_TO_TIME(SUM(TIMESTAMPDIFF(SECOND, ul.start_date, ul.end_date))) as `total_hours` FROM `users_timings_log` ul INNER JOIN `projects_tasks` pt ON ul.type_id = pt.id WHERE `type` = '0' AND pt.module_id = '" . $module->id . "' GROUP BY pt.module_id";

														if ($query_result2 = $con->query($query2)) {

															$count_query2 = $query_result2->num_rows;

															if ($count_query2 != '0') {

																$task_total = $query_result2->fetch_object();
																$total_task = $task_total->total_hours;

															} else {

																$total_task = '00:00:00';

															}

														}

														?>

														<span class="module-total-label">Subtotal: <span style="margin-left: 20px;"><?php echo substr($total_task, 0, -3); ?></span></span>

													</th> 

												</thead>

												<?php

													$sql3 = "SELECT * FROM `projects_tasks` WHERE `module_id` = '" . $module->id . "'";

													if ($result3 = $con->query($sql3)) {

														while ($task = $result3->fetch_object()) { ?>

														<thead class="task">

															<th colspan="3" style="padding-left: 25px;">

																<?php echo ucfirst($task->name); ?>

															</th>

															<th colspan="3" style="text-align: right;">

																<span class="task-estimated-time" data-toggle="tooltip" data-placement="top" title="Estimated"><?php echo $task->estimated_time; ?></span>

																<?php

																	$subquery = "SELECT SUM(TIMESTAMPDIFF(SECOND, `start_date`, `end_date`)) as `effective_time` FROM `users_timings_log` WHERE `type_id` = '" . $task->id . "' AND `type` = '0'";

																	if ($subquery_result = $con->query($subquery)) {

																		while ($effective_time = $subquery_result->fetch_object()) {

																			$total_time = $effective_time->effective_time;

																		}

																	}

																?>

																<span style="color: #000; font-size:12px; font-weight: normal; text-transform: lowercase;"> - </span>

																<span class="task-total-label" data-toggle="tooltip" data-placement="top" title="Effective"><?php echo convert_to_time($total_time); ?></span>

																<span style="color: #000; font-size:12px; font-weight: normal; text-transform: lowercase;"> = </span>

																<?php

																	$subquery = "SELECT SUM(TIMESTAMPDIFF(SECOND, `start_date`, `end_date`)) as `effective_time` FROM `users_timings_log` WHERE `type_id` = '" . $task->id . "' AND `type` = '0'";

																	if ($subquery_result = $con->query($subquery)) {

																		while ($effective_time = $subquery_result->fetch_object()) {

																			$total_time = $effective_time->effective_time;

																		}

																	}

																	$total_estimated = explode(":", $task->estimated_time);

																	$total_estimated = ($total_estimated[0] * 3600) + ($total_estimated[1] * 60);

																	$balance = ($total_estimated - $total_time);

																	echo absolute_value($balance);
																	
																?>

															</th>

														</thead>

														<?php

															$sql4 = "SELECT * FROM `users_timings_log` WHERE `type` = '0' AND `type_id` = '" . $task->id . "' ORDER BY `start_date` ASC";

															if ($result4 = $con->query($sql4)) {

																while ($task_log = $result4->fetch_object()) { ?>

																<tbody class="module-times">

																	<tr class="hover-class">

																		<td class="times indent2" colspan="2" width="40%">

																			<?php echo date("l - d/m/Y", strtotime($task_log->start_date)); ?><?php if ($_SESSION['id'] == $task_log->user_id) : ?><span class="icons"><span class="icon icon-pencil edit-task-log" data-id="<?php echo $task_log->id; ?>"></span> <span class="icon icon-bin delete-task-log" data-id="<?php echo $task_log->id; ?>"></span></span><?php endif; ?>

																		</td>

																		<td align="right" class="times" width="45%">

																			<?php

																			$query = "SELECT * FROM `users` WHERE `id` = '" . $task_log->user_id . "'";

																			if ($query_result = $con->query($query)) {

																				while ($user = $query_result->fetch_object()) {

																					echo ucfirst($user->name) . ' ' . ucfirst($user->surname);

																				}

																			}

																			?>

																		</td>

																		<td align="center" class="times" width="5%">

																			<?php echo date("H:i", strtotime($task_log->start_date)); ?>

																		</td>

																		<td align="center" class="times" width="5%">

																			<?php echo date("H:i", strtotime($task_log->end_date)); ?>

																		</td>

																		<td align="right" class="times" width="5%">

																			<?php

																			$query = "SELECT TIMESTAMPDIFF(SECOND, `start_date`, `end_date`) as `difference` FROM `users_timings_log` WHERE `id` = '" . $task_log->id . "'";

																			if ($query_result = $con->query($query)) {

																				while ($log = $query_result->fetch_object()) {

																					echo '<span class="log-label">' . gmdate("H:i", $log->difference) . '</span>';

																				}

																			}

																			?>

																		</td>

																	</tr>

																</tbody>

																<?php

																}

															}

														}

													}

												}

											}

											?>

										</table>

									</div>

								</div>

							</div>

						</div>

					</div>

					<?php

					}

				}

			}

		} else 

		if (!empty($mode)) { ?>

		<div class="all-my-tasks">

			<?php

			$sql = "SELECT * FROM `projects` WHERE `created_by` = '" . $_SESSION['id'] . "' OR `private` = '0' AND `created_by` != '" . $_SESSION['id'] . "'";

			if ($result = $con->query($sql)) {

				echo '<h3>Tasks</h3>';

				while ($project = $result->fetch_object()) {

					echo '<h4>' . ucfirst($project->name) . '</h4>';

					$sql2 = "SELECT * FROM `projects_tasks` WHERE `responsible` = '" . $user_id . "' AND `label` != '6' AND `planned` = '0' AND `project_id` = '" . $project->id . "'";

					if ($result2 = $con->query($sql2)) {

						$count2 = $result2->num_rows;

						if ($count2 == 0) {

							echo '<ul class="list-tasks"></ul>';

						} else {

							echo '<ul class="list-tasks">';

							while ($task = $result2->fetch_object()) {

								$sql3 = "SELECT * FROM `labels` WHERE `id` = '" . $task->label . "'";

								if ($result3 = $con->query($sql3)) {

									while ($label = $result3->fetch_object()) {

										echo '<li data-task="' . $task->id . '" data-toggle="toopltip" data-placement="top" data-class="' . strtolower(str_replace(" ", "-", $label->name)) . '" title="' . ucfirst($task->name) . '"><div class="task">'; if (strlen($task->name) > 20) { echo ucfirst(substr($task->name, 0, 20)). '...'; } else { echo ucfirst($task->name); }  echo '<span style="float: right;"><span class="' . strtolower(str_replace(" ", "-", $label->name)) . '">' . first_char($label->name) . '</span></span></div></li>';

									}

								}

							}

							echo '</ul>';

						}

					}

				}
			}

			?>

		</div>

		<div class="my-calendar">

			<?php 

			$day_headings = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'); 

			$running_day = date('N', mktime(0, 0, 0, $month, 1, $year))-1;
			$days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
			$days_in_this_week = 1;
			$day_counter = 0;
			$dates_array = array();
			$class = '';

			$today = date("d");
			$tmonth = date("m");

			?>

			<table class="calendar">

				<tr class="calendar-row">

					<td class="calendar-day-headings">

						<?php echo implode('</td><td class="calendar-day-headings">', $day_headings); ?>

					</td>

				</tr>

				<tr class="calendar-row">

					<?php

					for ($x = 0; $x < $running_day; $x++) {

						echo '<td class="calendar-day-np"> </td>';
						$days_in_this_week++;

					}

					if (strlen($month) < 2) {

						$month = '0' . $month;

					}

					for ($list_day = 1; $list_day <= $days_in_month; $list_day++) {

						if ($list_day < 10) {

							$list_day = '0' . $list_day;

						}

						$active_date = $year . '-' . $month . '-' . $list_day;

						$class2 = '';

						echo '<td class="calendar-day ' . $class2 . '">';

						if ($list_day == $today && $month == $tmonth) {
							$class = 'highlight';
						} else {
							$class = 'no-highlight';
						}

						if (date("D", strtotime($active_date)) == 'Sun' || date("D", strtotime($active_date)) == 'Sat') {
							$day_class = 'weekend';
						} else {
							$day_class = 'midweek';
						}

						echo '<div class="day-number ' . $class . '"><span class="' . $day_class . '">' . $list_day . '</span></div>';
						echo '<ul class="calendar-tasks" data-day="' . $list_day . '" data-month="' . $month . '" data-year="' . $year . '">';

						$sql = "SELECT * FROM `projects_tasks_schedule` WHERE `date` = '" . $active_date . "' AND `user_id` = '" . $user_id . "'";

						if ($result = $con->query($sql)) {

							while ($task_schedule = $result->fetch_object()) {

								$sql2 = "SELECT * FROM `projects_tasks` WHERE `id` = '" . $task_schedule->task_id . "'";

								if ($result2 = $con->query($sql2)) {

									while ($task = $result2->fetch_object()) {

										$sql3 = "SELECT * FROM `labels` WHERE `id` = '" . $task->label . "'";

										if ($result3 = $con->query($sql3)) {

											while ($label = $result3->fetch_object()) {

												echo '
												<li data-task="' . $task->id . '" data-toggle="popover" data-placement="top" data-class="' . strtolower(str_replace(' ', '-', $label->name)) . '" title="' . $task->name . '" data-content="' . mb_strimwidth($task->description, 0, 100, "...") . '" class="has-task">

												<div class="task">
												' . $task->name . '
												<span style="float: right;">
													<span class="' . strtolower(str_replace(' ', '-', $label->name)) . '">' . first_char($label->name) . '</span>
												</span>
												</div>
												<div data-task="' . $task->id . '" class="listed-task ' . strtolower(str_replace(' ', '-', $label->name)) . '">' . mb_strimwidth($task->name, 0, 20, "...") . '</div>

												</li>';

											}

										}

									}

								}

							}

						}

						echo '</ul>';

						// Input for days come here

						echo '</td>';

						if ($running_day == 6) {

							echo '</tr>';

							if (($day_counter+1) != $days_in_month) {

								echo '<tr class="calendar-row">';

							}

							$running_day = -1;
							$days_in_this_week = 0;

						}

						$days_in_this_week++; $running_day++; $day_counter++;

					}

					if (($days_in_this_week > 1) && ($days_in_this_week < 8)) {

						for ($x = 1; $x <= (8 - $days_in_this_week); $x++) {

							echo '<td class="calendar-day-np"> </td>';

						}

					}

					?>

				</tr>

			</table>

		</div>

		<?php

		}

	}

?>

</div>

<?php if (empty($project_id)) { ?>

<!-- Create project -->

<div class="modal fade" id="CreateProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Create a new project</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="name">What's the projectname</label>
							<input autocomplete="off" class="name" name="name" type="text" id="name" value="">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="responsible">Who's responsible for the project</label>
							<select id="responsible" class="responsible" name="responsible">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `users`";

									if ($result = $con->query($sql)) {

										while ($user = $result->fetch_object()) { ?>

										<option value="<?php echo $user->id; ?>"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="datepicker">Is there a deadline</label>
							<input autocomplete="off" class="deadline calendar" name="deadline" type="text" id="datepicker" value="">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="label">This project is</label>
							<select name="label" class="label" id="label">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `labels` WHERE `type` = '1'";

									if ($result = $con->query($sql)) {

										while ($label = $result->fetch_object()) { ?>

										<option value="<?php echo $label->id; ?>"><?php echo ucfirst($label->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="customer">Who's the customer</label>
							<input autocomplete="off" class="customer" name="customer" type="text" id="customer" value="">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="channel">Through what channel did we get it</label>
							<select name="channel" class="channel" id="channel">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `users_groups`";

									if ($result = $con->query($sql)) {

										while ($group = $result->fetch_object()) { ?>

										<option value="<?php echo $group->id; ?>"><?php echo ucfirst($group->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

				</div>

				<div class="modal-divider"></div>

				<div class="form-body">

					<div class="full">

						<div class="form-group">

							<label for="description">Maybe describe the project a little</label>
							<textarea name="description" class="description" id="description"></textarea>

						</div>

					</div>

					<div class="full">

						<div class="form-group">
							
							<label for="private">Mark this project as private</label>
							<label><input type="checkbox" name="private" id="private"> <span class="label">Yes, mark as private</span></label>

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">
				
				<button type="submit" name="create-project" class="submit-button create-project" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Edit Project -->

<div class="modal fade" id="EditProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit project</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="edit-project-body"></div>

			</div>

			<div class="modal-footer">
				<button type="submit" name="save-edit-project" class="submit-button save-edit-project" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<?php } else { ?>

<!-- Assign user to project -->

<div class="modal fade" id="AssignUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Choose assignees for this project</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="full">

						<div class="form-group">

							<p class="label-alike"><span class="label">Which users should be assigned</span></p>

							<?php

							$sql = "SELECT * FROM `users`";

							if ($result = $con->query($sql)) {

								while ($user = $result->fetch_object()) {

									$sql2 = "SELECT * FROM `projects_users` WHERE `user_id` = '" . $user->id . "' AND `project_id` = '" . $project_id . "'";

									if ($result2 = $con->query($sql2)) {

										echo '<div id="assignee">';

										$count2 = $result2->num_rows;

										if ($count2 == 0) {

											echo '<label><input type="checkbox" name="assignee[]" value="' . $user->id . '"> <span class="label">' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . '</span></label>';

										}

										echo '</div>';

									}

								}

							}

							?>

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<button type="submit" name="choose-assignees" class="submit-button choose-assignees" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- New Module -->

<div class="modal fade" id="NewModule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Create a new module</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="name2">How should the module be named</label>
							<input type="text" name="name2" class="name2" id="name2">

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<button type="submit" name="create-module" class="submit-button create-module" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Create Task -->

<div class="modal fade" id="NewTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Create a new task</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="name">The name of the task</label>
							<input type="text" name="name" class="name" id="name">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="responsible">Who's responsible for this task</label>
							<select id="responsible" class="responsible" name="responsible">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `users`";

									if ($result = $con->query($sql)) {

										while ($user = $result->fetch_object()) { ?>

										<option value="<?php echo $user->id; ?>"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="datepicker">Is there a deadline</label>
							<input type="text" name="deadline" class="deadline calendar" id="datepicker">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="category">Where would you classify it</label>
							<select id="category" class="category" name="category">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `projects_categories` ORDER BY `name` ASC";

									if ($result = $con->query($sql)) {

										while ($category = $result->fetch_object()) { ?>

										<option value="<?php echo $category->id; ?>"><?php echo ucfirst($category->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="estimated">How long would this task take</label>
							<select id="estimated" class="estimated_hour" name="estimated_hour">
							<?php

							for ($i = 0; $i < 201; $i++) {

								echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

							}

							?>
							</select>

							<select class="estimated_minute" name="estimated_minute">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="module">Link to</label>
							<select name="module" class="module" id="module">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project_id . "'";

									if ($result = $con->query($sql)) {

										while ($module = $result->fetch_object()) { ?>

										<option value="<?php echo $module->id; ?>"><?php echo ucfirst($module->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="full">

						<div class="form-group">

							<label for="description">Maybe describe the task a little</label>
							<textarea name="description" class="description" id="description"></textarea>

						</div>

					</div>

					<div class="full">

						<div class="form-group">
							
							<label for="important">Mark this task as important</label>
							<label><input type="checkbox" name="important" id="important"> <span class="label">Yes, mark as important</span></label>

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<button type="submit" name="create-task" class="submit-button create-task" href="#">Save</a>

			</div>

		</div>

	</div>

</div>

<!-- Transfer To -->

<div class="modal fade" id="TransferTo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Transfer tasks to</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="full">

						<div class="form-group">

							<label for="newmodule">Link the <strong><span class="task-amount"></span></strong> selected tasks to</label>
							<select name="newmodule" class="newmodule" id="newmodule">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project_id . "'";

									if ($result = $con->query($sql)) {

										while ($module = $result->fetch_object()) { ?>

										<option value="<?php echo $module->id; ?>"><?php echo ucfirst($module->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<button type="submit" name="transfer-task" class="submit-button transfer-task" href="#">Save</a>

			</div>

		</div>

	</div>

</div>

<!-- View Task -->

<div class="modal fade" id="ViewTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Task details</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="view-task-body"></div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<input type="hidden" class="task-id" value="">
				<button type="submit" name="save-task" class="submit-button save-task" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Module Timing -->

<div class="modal fade" id="ModuleTiming" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Module Details</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="view-module-body"></div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<input type="hidden" class="module-id" value="">
				<button type="submit" name="save-module-timing" class="submit-button save-module-timing" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Edit Task -->

<div class="modal fade" id="EditTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Task</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="edit-task-body"></div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<input type="hidden" class="task-id" value="">
				<button type="submit" name="save-edit-task" class="submit-button save-edit-task" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Edit Module -->

<div class="modal fade" id="EditModule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Module</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body edit-module-body">

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<input type="hidden" class="module-id" value="">
				<button type="submit" name="save-edit-module" class="submit-button save-edit-module" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Create Folder -->

<div class="modal fade" id="CreateFolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">New Folder</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="full">

						<div class="form-group">

							<label for="foldername">The name of the folder</label>
							<input type="text" name="foldername" class="foldername" id="foldername">

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<button type="submit" name="save-new-folder" class="submit-button save-new-folder" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Custom Timing -->

<div class="modal fade" id="CustomTiming" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Custom Timing</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="when">When did you work on this task</label>
							<input type="text" name="when" class="when calendar" id="when" value="<?php echo date("d-m-Y"); ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="description98">Give a brief description</label>
							<input type="text" name="description98" class="description98" id="description98">

						</div>

					</div>

					<div class="half" style="clear: both;">

						<div class="form-group">

							<label for="estimated2">I started around</label>
							<select id="estimated2" class="estimated_hour2" name="estimated_hour2">
								<?php

								for ($i = 0; $i < 24; $i++) {

									echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

								}

								?>
							</select>

							<select class="estimated_minute2" name="estimated_minute2">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="estimated4">I ended around</label>
							<select id="estimated4" class="estimated_hour4" name="estimated_hour4">
								<?php

								for ($i = 0; $i < 24; $i++) {

									echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

								}

								?>
							</select>

							<select class="estimated_minute4" name="estimated_minute4">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

						</div>

					</div>

					<!-- Custom logs -->

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<input type="hidden" class="task-id" value="">
				<button type="submit" name="save-custom-timing" class="submit-button save-custom-timing" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Create Bug -->

<div class="modal fade" id="NewBug" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

		<form id="SubmitBugForm" name="SubmitBugForm" action="" method="post" enctype="multipart/form-data">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Report a bug</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="bugname">The name of the bug</label>
							<input type="text" name="bugname" class="bugname" id="bugname" required>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="responsible5">Who's responsible for this bug</label>
							<select id="responsible5" class="responsible5" name="responsible5" required>
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `users`";

									if ($result = $con->query($sql)) {

										while ($user = $result->fetch_object()) { ?>

										<option value="<?php echo $user->id; ?>"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="deadline5">Is there a deadline</label>
							<input type="text" name="deadline5" class="deadline5 calendar" id="deadline5">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="choose_file">Attach file(s)</label>
							<input type="file" id="choose_file" class="choose_file" name="choose_file[]" multiple style="margin-bottom: 20px; margin-top: 5px;">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="path">Path/link to the bug</label>
							<input type="text" name="path" class="path" id="path">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="category2">Module</label>
							<input type="text" name="category2" class="category2" id="category2">

						</div>

					</div>

					<div class="full">

						<div class="form-group">

							<label for="description5">Please give a detailed description of the bug</label>
							<textarea name="description5" class="description5" id="description5" required></textarea>

						</div>

					</div>

					<div class="full">

						<div class="form-group">
							
							<label for="important3">Mark this bug as important</label>
							<label><input type="checkbox" name="important3" id="important3" value="1"> <span class="label">Yes, mark as important</span></label>

						</div>

					</div>

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" name="project-id" class="project-id" value="<?php echo $project_id; ?>">
				<button type="submit" name="create-bug" class="submit-button create-bug" href="#">Save</button>

			</div>

		</div>

		</form>

	</div>

</div>

<!-- View Bug -->

<div class="modal fade" id="ViewBug" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Bug details</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="view-bug-body"></div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<input type="hidden" class="bug-id" value="">
				<button type="submit" name="save-bug" class="submit-button save-bug" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<!-- Edit Bug -->

<div class="modal fade" id="EditBug" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Bug</h4>

			</div>

			<form action="" method="post" enctype="multipart/form-data">

			<div class="modal-body">

				<div class="response"></div>

				<div class="edit-bug-body"></div>

			</div>

			<div class="modal-footer">
				<button type="submit" name="save-edit-bug" class="submit-button save-edit-bug" href="#">Save</button>

			</div>

			</form>

		</div>

	</div>

</div>

<!-- Edit Custom Timing -->

<div class="modal fade" id="EditCustomTiming" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Custom Timing</h4>

			</div>

			<div class="modal-body">

				<div class="response"></div>

				<div class="form-body edit-custom-timing">

				</div>

			</div>

			<div class="modal-footer">

				<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">
				<input type="hidden" class="task-log-id" value="">
				<button type="submit" name="save-custom-timing" class="submit-button save-edit-custom-timing" href="#">Save</button>

			</div>

		</div>

	</div>

</div>

<?php } ?>