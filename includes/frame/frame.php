<nav>
	<div class="align-nav"><?php include 'includes/frame/navigation.php'; ?></div>
	<!--<?php include 'includes/frame/timer.php'; ?>-->
</nav>

<div class="filler"></div>

<section>
	
	<!--
	<?php include 'includes/frame/tools.php'; ?>
	-->

	<?php

	$cat = isset($_GET['cat']) ? $_GET['cat'] : false;

	if (empty($cat) || $cat == 'projects') {

		include 'includes/content/projects/index.php';

	} else

	if ($cat == 'users') {

		include 'includes/content/users/index.php';

	} else

	if ($cat == 'settings') {

		include 'includes/content/settings/index.php';

	} else

	if ($cat == 'calendar') {

		include 'includes/content/calendar/index.php';

	} else

	if ($cat == 'requests') {

		include 'includes/content/requests/index.php';

	} else

	if ($cat == 'reports') {

		include 'includes/content/reports/index.php';

	} else

	if ($cat == 'work') {

		include 'includes/content/work/index2.php';

	}

	?>

</section>