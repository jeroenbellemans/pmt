<?php

	$page = isset($_GET['page']) ? $_GET['page'] : false;

?>

<?php

	$sql = "SELECT `privileges` FROM `users` WHERE `id` = '" . $_SESSION['id'] . "'";

	if ($res = $con->query($sql)) {

		while ($privilege = $res->fetch_object()) {

			$class = '';

			// Navigation for super-admin
			if ($privilege->privileges == '0') { 

				$sql2 = "SELECT * FROM `calendar_holliday` WHERE `status` = '0'";

				if ($result2 = $con->query($sql2)) {

					$count2 = $result2->num_rows;

				}

				$sql3 = "SELECT * FROM `calendar_remotework` WHERE `status` = '0'";

				if ($result3 = $con->query($sql3)) {

					$count3 = $result3->num_rows;

				}

				$total_requests = $count3; // + $count2 -> holidays

				if ($total_requests == 0) {
					$class = 'neutral';
				}

			?>

			<ul id="main-navigation">

				<li><a href="index.php?cat=work"><span class="summary-icon">My Work</span></a></li>
				<li><a href="index.php?cat=projects"><span class="summary-icon">Projects</span></a></li>
				<li><a href="index.php?cat=calendar"><span class="summary-icon">Planning</span></a></li>
				<li><a href="index.php?cat=invoices"><span class="summary-icon">Invoices</span></a></li>
				<li><a href="index.php?cat=users"><span class="summary-icon">Users</span></a></li>
				<li><a href="index.php?cat=requests"><span class="summary-icon">Requests <span class="number-tag <?php echo $class; ?>"><?php echo $total_requests; ?></span></span></a></li>
				<li><a href="index.php?cat=settings"><span class="summary-icon">Settings</span></a></li>
				<li><a href="logout.php"><span class="summary-icon">Logout</span></a></li>

			</ul>
			
			<?php

			} else

			// Navigation for admin
			if ($privilege->privileges >= '1' && $privilege->privileges < '4') { ?>

			<ul id="main-navigation">

				<li><a href="index.php?cat=work"><span class="summary-icon">My Work</span></a></li>
				<li><a href="index.php?cat=projects"><span class="summary-icon">Projects</span></a></li>
				<li><a href="index.php?cat=calendar"><span class="summary-icon">Planning</span></a></li>
				<li><a href="index.php?cat=settings"><span class="summary-icon">Settings</span></a></li>
				<li><a href="logout.php"><span class="summary-icon">Logout</span></a></li>

			</ul>

			<?php

			} else

			if ($privilege->privileges == '4') { ?>

			<ul id="main-navigation">

				<li><a href="index.php?cat=work"><span class="summary-icon">My Work</span></a></li>
				<li><a href="index.php?cat=projects"><span class="summary-icon">Projects</span></a></li>
				<li><a href="logout.php"><span class="summary-icon">Logout</span></a></li>

			</ul>

			<?php

			}

		}

	}

?>