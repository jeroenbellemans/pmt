<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['name'])) {

		$project = htmlentities($_POST['project']);
		$name = htmlentities($_POST['name']);
		$responsible = htmlentities($_POST['responsible']);
		$responsible2 = htmlentities($_POST['responsible2']);
		$deadline = date("Y-m-d", strtotime($_POST['deadline']));
		$label = htmlentities($_POST['label']);
		$customer = htmlentities($_POST['customer']);
		$channel = htmlentities($_POST['channel']);
		$description = mysqli_real_escape_string($con, $_POST['description']);

		if ($_POST['private'] == 'false') {
			$private = '0';
		} else
		if ($_POST['private'] == 'true') {
			$private = '1';
		}

		if ($deadline == '1970-01-01') {
			$deadline == '0000-00-00';
		} else {
			$deadline = $deadline . ' 23:59:59';
		}

		if (empty($name)) {
			echo 'name';
		} else
		if (empty($customer)) {
			echo 'customer';
		} else
		if (empty($responsible)) {
			echo 'responsible';
		} else
		if (empty($label)) {
			echo 'label';
		} else
		if (empty($channel)) {
			echo 'channel';
		} else {

			$sql = "UPDATE `projects` SET `name` = '" . $name . "', `responsible` = '" . $responsible . "', `deadline` = '" . $deadline . "', `private` = '" . $private . "', `label` = '" . $label . "', `customer` = '" . $customer . "', `channel` = '" . $channel . "', `description` = '" . $description . "', `updated_by` = '" . $_SESSION['id'] . "', `updated_at` = now() WHERE `id` = '" . $project . "'";
			$con->query($sql);

			$sql2 = "UPDATE `projects_users` SET `responsible` = '" . $responsible . "' WHERE `project_id` = '" . $project . "' AND `responsible` = '" . $responsible2 . "'";
			$con->query($sql2);

			echo 'success';

		}

	}

?>