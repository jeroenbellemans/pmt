<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['user'])) {

		$user_id = $_POST['user'];
		$date = $_POST['date'];

	?>

		<!--

		<ul class="nav nav-tabs" role="tablist">

			<li role="presentation" class="active"><a href="#planning" aria-controls="planning" role="tab" data-toggle="tab"><span class="icon2 icon-clipboard"></span> My planning</a></li>
			<li role="presentation"><a href="#requests" aria-controls="requests" role="tab" data-toggle="tab"><span class="icon2 icon-loop"></span> Request</a></li>

		</ul>

		-->

		<div class="tab-content" style="clear: both;">

			<div role="tabpanel" class="tab-pane active" id="planning">

				<?php

				$sql = "SELECT * FROM `projects_tasks_schedule` WHERE `date` = '" . $date . "' AND `user_id` = '" . $user_id . "'";

				if ($result = $con->query($sql)) {

					$count = $result->num_rows;

					echo '<p class="title">Planned tasks <small>Total: <strong>' . $count . '</strong></small></p>';

					if ($count == 0) {

						echo '<p class="err-message">No tasks have been planned on this day</p>';

					}

					echo '<ul class="task-holder-holder">';

					while ($schedule = $result->fetch_object()) {

						$sql2 = "SELECT * FROM `projects_tasks` WHERE `id` = '" . $schedule->task_id . "'";

						if ($result2 = $con->query($sql2)) {

							while ($task = $result2->fetch_object()) { ?>

							<li class="task-holder">

								<p class="task-name"><a href="#" class="view-task" data-id="<?php echo $task->id; ?>" data-project="<?php echo $task->project_id; ?>"><?php echo ucfirst($task->name); ?></strong></a></p>

								<?php

								$sql3 = "SELECT * FROM `projects` WHERE `id` = '" . $task->project_id . "'";

								if ($result3 = $con->query($sql3)) {

									while ($project = $result3->fetch_object()) { ?>


								<p class="linked-to"><?php echo ucfirst($project->name) ;?></p>

							</li>

									<?php

									}

								}

								?>

							<?php

							}

						} 

					}

					echo '</ul>';

				}

				?>

				<?php

				$sql2 = "SELECT * FROM `projects_tasks` WHERE DATE(`deadline`) = '" . $date . "' AND `planned` = '0' AND `responsible` = '" . $user_id . "'";

				if ($result2 = $con->query($sql2)) {

					$count2 = $result2->num_rows;

					echo '<p class="title" style="margin-top: 20px;">Ending deadlines <small>Total: <strong>' . $count2 . '</strong></small></p>';

					if ($count2 == 0) {

						echo '<p class="err-message">There are no deadlines on this day</p>';

					}

					echo '<ul class="task-holder-holder">';

					while ($task = $result2->fetch_object()) { ?>

						<li class="task-holder">

							<p class="task-name"><a href="#" class="view-task" data-id="<?php echo $task->id; ?>" data-project="<?php echo $task->project_id; ?>"><?php echo ucfirst($task->name); ?></a></p>

							<?php

							$sql3 = "SELECT * FROM `projects` WHERE `id` = '" . $task->project_id . "'";

							if ($result3 = $con->query($sql3)) {

								while ($project = $result3->fetch_object()) { ?>


							<p class="linked-to"><?php echo ucfirst($project->name) ;?></p>

						</li>

							<?php

							}

						} 

					}

					echo '</ul>';

				}

				?>

			</div>

		</div>

		<?php
	}

?>