<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['hour1'])) {

		$starthour = $_POST['hour1'];
		$startminute = $_POST['minute1'];
		$endhour = $_POST['hour2'];
		$endminute = $_POST['minute2'];
		$date = $_POST['date'];
		$module_id = $_POST['mid'];
		$description = htmlentities($_POST['description']);

		if (empty($date)) {

			echo 'date'; // Data response jquery -> missing date

		} else

		if (empty($description)) {

			echo 'description'; // Data response jquery -> missing description

		} else

		if (!empty($starthour) || !empty($startminute) || !empty($endhour) || !empty($endminute) || !empty($date) || !empty($description)) {

			if ($endhour < $starthour) {

				echo 'negative'; // Data response jquery -> negative hours

			} else {

				$start_date = $date . ' ' . $starthour .':' . $startminute . ':00';
				$end_date = $date . ' ' . $endhour .':' . $endminute . ':00';

				$start_date = strtotime($start_date);
				$end_date = strtotime($end_date);

				$difference = gmdate("H:i", ($end_date - $start_date));
				$difference = explode(":", $difference);

				$start_date = date("Y-m-d H:i:s", $start_date);
				$end_date = date("Y-m-d H:i:s", $end_date);

				$sql = "INSERT INTO `users_timings_log` (`start_date`, `end_date`, `description`, `type`, `type_id`, `user_id`, `date`) VALUES ('" . $start_date . "', '" . $end_date . "', '" . $description . "', '1', '" . $module_id . "', '" . $_SESSION['id'] . "', now())";
				$con->query($sql);

				echo 'success';

			}

		}

	}

?>