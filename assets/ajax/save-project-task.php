<?php
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['label'])) {

		$label = $_POST['label'];
		$id = $_POST['task'];

		if ($label) {

			$sql2 = "UPDATE `projects_tasks` SET `label` = '" . $label . "' WHERE `id` = '" . $id . "'";
			$con->query($sql2);

			if ($label == '6') {

				$sql4 = "SELECT * FROM `users` WHERE `id` = '" . $task->created_by . "'";

				if ($result4 = $con->query($sql4)) {

					while ($user = $result4->fetch_object()) {

						$to = $user->email;
						$from = "info@time2change.com";
						$subject = "#" . $id . " " . $task->name . ": Marked as \"For Review\"";
						
						// Write the contents of your e-mail here using HTML code
						$message = '
						<h1>' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . '</h1>
						<h2>#' . $id . ' ' . $task->name . ' - has been marked as <strong>For review</strong></h2>
						<p>Please review this task and mark it as completed or as incompleted! You can do this <a href="http://pmt.time2change.com/index.php?pid=' . $task->project_id . '&redirect=true&uid=' . $user->id . '">right here</a>.</p>
						<br><br>
						<p>PMT &copy; Time2Change</p>
						';
									
						$headers = "From: $from\r\n";
						$headers .= "Content-type: text/html\r\n";
						$to = $to;
						
						// Send the actual mail to the users
						mail($to, $subject, $message, $headers);

					}

				}

			}

		}

	}
?>