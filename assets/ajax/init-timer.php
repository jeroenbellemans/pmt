<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['task_id'])) {

		$task_id = $_POST['task_id'];
		$project_id = $_POST['project'];

		if ($task_id) { ?>

		<div class="real-time-clock">

			<p class="small-title"><span class="left"><span class="icon icon-clock"></span> Timer</span> <span class="buttons"><span class="iconset icon-play3" id="sw_start" data-toggle="tooltip" data-placement="top" title="Start timer"></span><span class="iconset icon-pause2" id="sw_pause" data-toggle="tooltip" data-placement="top" title="Pause timer"></span><span class="iconset icon-stop2" id="sw_stop" data-toggle="tooltip" data-placement="top" title="Stop timer"></span></span></p>

			<p class="update-clock"><span id="sw_h">00</span>:<span id="sw_m">00</span>:<span id="sw_s">00</span></p>
			<p class="clock-status"><span id="sw_status">Idle</span></p>

		</div>

		<?php

		}

	}

?>