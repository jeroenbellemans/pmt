<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['id'])) {

		$module_id = $_POST['id'];
		$project_id = $_POST['project'];

		if (!empty($module_id)) { ?>

		<div class="form-body">

			<div class="half">

				<div class="form-group">

					<label for="when2">When did you work on this task</label>
					<input type="text" name="when2" class="when2 calendar" id="when2" value="<?php echo date("d-m-Y"); ?>">

				</div>

			</div>

			<div class="half">

				<div class="form-group">

					<label for="description8">Describe what you did</label>
					<input type="text" name="description8" class="description8" id="description8" value="">

				</div>

			</div>

			<div class="half">

				<div class="form-group">

					<label for="estimated3">When did you start on it</label>
					<select id="estimated3" class="estimated_hour3" name="estimated_hour3">
						<?php

						for ($i = 0; $i < 24; $i++) {

							echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

						}

						?>
					</select>

					<select class="estimated_minute3" name="estimated_minute3">
						<option value="00">00</option>
						<option value="05">05</option>
						<option value="10">10</option>
						<option value="15">15</option>
						<option value="20">20</option>
						<option value="25">25</option>
						<option value="30">30</option>
						<option value="35">35</option>
						<option value="40">40</option>
						<option value="45">45</option>
						<option value="50">50</option>
						<option value="55">55</option>
					</select>

				</div>

			</div>

			<div class="half">

				<div class="form-group">

					<label for="estimated5">When did you end working</label>
					<select id="estimated5" class="estimated_hour5" name="estimated_hour5">
						<?php

						for ($i = 0; $i < 24; $i++) {

							echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

						}

						?>
					</select>

					<select class="estimated_minute5" name="estimated_minute5">
						<option value="00">00</option>
						<option value="05">05</option>
						<option value="10">10</option>
						<option value="15">15</option>
						<option value="20">20</option>
						<option value="25">25</option>
						<option value="30">30</option>
						<option value="35">35</option>
						<option value="40">40</option>
						<option value="45">45</option>
						<option value="50">50</option>
						<option value="55">55</option>
					</select>

				</div>

			</div>

		</div>

		<?php

		}

	}

?>