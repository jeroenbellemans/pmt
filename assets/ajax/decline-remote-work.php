<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['id'])) {

		$id = htmlentities($_POST['id']);

		if (!empty($id)) {

			$sql = "UPDATE `calendar_remotework` SET `status` = '2' WHERE `id` = '" . $id . "'";
			$con->query($sql);

			$sql2 = "SELECT * FROM `calendar_remotework` WHERE `id` = '" . $id . "'";

			if ($result2 = $con->query($sql2)) {

				while ($calendar = $result2->fetch_object()) {

					$sql3 = "SELECT * FROM `users` WHERE `id` = '" . $calendar->user_id . "'";

					if ($result3 = $con->query($sql3)) {

						while ($user = $result3->fetch_object()) {

							$to = $user->email;
							$from = "info@time2change.com";
							$subject = "Request declined";
							
							// Write the contents of your e-mail here using HTML code
							$message = '
							<h1>Hello ' . ucfirst($user->name) . '</h1>
							<h2>Request for remote working declined</h2>
							<p>On <strong>' . date("l d F Y", strtotime($calendar->date)) . '</strong> you are expected at the office!</p>
							<p>Don\'t forget to <a href="http://pmt.time2change.com/index.php?cat=calendar&redirect=true&uid=' . $user->id . '">register your worklog</a> for that day!</p>
							<br><br>
							<p>PMT &copy; Time2Change</p>
							';
										
							$headers = "From: $from\r\n";
							$headers .= "Content-type: text/html\r\n";
							$to = $to;
							
							// Send the actual mail to the users
							mail($to, $subject, $message, $headers);

						}

					}

				}

			}

		}

	}

?>