<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['date'])) {

		$date = $_POST['date'];

		$sql = "SELECT * FROM `calendar_worklog` WHERE `user_id` = '" . $_SESSION['id'] . "' AND DATE(`date`) = '" . $date . "' ORDER BY `started_at`";

		if ($result = $con->query($sql)) {

			$count = $result->num_rows;

			if ($count == 0) {

				echo '<div class="group"><p class="group-name">You have no log entries for this day</p></div>';

			} else {

				while ($logs = $result->fetch_object()) { ?>

					<div class="group">
						<p class="group-name"><?php echo date("H:i", strtotime($logs->started_at)); ?> - <?php echo date("H:i", strtotime($logs->ended_at)); ?><span class="icon-set"><span class="inline-icon icon-bin remove-log-entry" data-id="<?php echo $logs->id; ?>"></span></span></p>
					</div>

				<?php

				}

			}

		}

	}

?>