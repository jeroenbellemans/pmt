<?php
	
	session_start();
	require '../../../database/my-connection.php';
	require '../../../config/functions/time_left.php';

	if (isset($_POST['id'])) {

		$bug_id = htmlentities($_POST['id']);

		if (!empty($bug_id)) {

			$sql = "SELECT 
						*,
						projects_bugs.id AS task_id,
						projects_bugs.name AS task_name,
						projects_bugs.deadline AS task_deadline,
						projects_bugs.important AS important_task,
						projects_bugs.description AS task_description,
						projects_bugs.completed AS task_completed,
						projects_bugs.status AS task_label,
						users.name AS task_creator_name,
						users.surname AS task_creator_surname,
						users.id AS task_creator_id,
						users2.name AS task_responsible_name,
						users2.surname AS task_responsible_surname,
						users2.id AS task_responsible_id,
						projects.id AS project_id,
						projects.name AS project_name, 
						labels.name AS label_name,
						labels.id AS label_id
					FROM `projects_bugs` 
						INNER JOIN `projects` ON projects.id = projects_bugs.project_id
						INNER JOIN `users` ON users.id = projects_bugs.created_by
						INNER JOIN `users` AS users2 ON users2.id = projects_bugs.responsible
						INNER JOIN `labels` ON labels.id = projects_bugs.status
					WHERE 
						projects_bugs.id = '" . $bug_id . "'
					";

			if ($result = $con->query($sql)) {

				while ($task = $result->fetch_object()) {

					?>

					<div class="general-info">

						<h2>Bug #<?php echo $task->task_id; ?> - <?php echo ucfirst($task->task_name); ?> <span style="float: right;"><small>[ <span class="the-label"><?php echo strtoupper($task->label_name); ?></span> ]</small></span></h2>

						<?php

						if ($task->task_completed == 0) {

							if (($task->task_creator_id == $_SESSION['id']) && ($task->task_responsible_id == $_SESSION['id'])) {

								echo '<p class="for-review-banner"><span class="text">You created and assigned this task to yourself</span> <span class="my-tasks-complete" data-id="' . $task->task_id . '" data-type="bug"><span class="icon icon-checkmark" style="margin-right: 2px; font-size: 10px;"></span> mark this as completed</span></p>';

							} else

							if (($task->task_completed == 0) && ($task->task_label == 6) && ($task->task_creator_id == $_SESSION['id'])) {

								echo '<p class="for-review-banner"><span class="text">This task awaits revision</span> <span class="my-tasks-complete" data-id="' . $task->task_id . '" data-type="bug"><span class="icon icon-checkmark" style="margin-right: 2px; font-size: 10px;"></span> mark this as completed</span><span class="my-tasks-incomplete" data-id="' . $task->task_id . '" data-type="task"><span class="icon icon-cross" style="margin-right: 2px; font-size: 10px;"></span> mark this as incomplete</span></p>';

							}

						} else
						if ($task->task_completed == 1) {

							echo '<p class="for-review-banner"><span class="text">This task is confirmed as completed</span> <span class="my-tasks-incomplete" data-id="' . $task->task_id . '" data-type="bug"><span class="icon icon-cross" style="margin-right: 2px; font-size: 10px;"></span> mark this as incomplete again</span></p>';

						}

						if ($task->important_task == '1') {

							$border = 'style="border-top: 3px solid #ff0000;"';

						} else {

							$border = '';

						}

						?>

						<table width="100%" <?php echo $border; ?>>

							<tr>

								<td class="left">Assigned by</td>
								<td class="right"><?php echo ucfirst($task->task_creator_name); ?> <?php echo ucfirst($task->task_creator_surname); ?></td>

							</tr>

							<tr>

								<td class="left">Responsible</td>
								<td class="right"><?php echo ucfirst($task->task_responsible_name); ?> <?php echo ucfirst($task->task_responsible_surname); ?></td>

							</tr>

							<tr>

								<td class="left">Status</td>
								<td class="right">

									<?php

									if ($task->task_responsible_id != $_SESSION['id']) {

										echo $task->label_name;

									} else {

									?>

										<select id="change-task-label" onchange="changeLabelBug();">

										<?php

										$sql2 = "SELECT
													*
												FROM `labels`
												WHERE `type` = '2'
												";

										if ($result2 = $con->query($sql2)) {

											while ($label = $result2->fetch_object()) {

												if ($label->id == $task->label_id) {

													$selected = 'selected="selected"';

												} else {

													$selected = '';

												}

												echo '<option value="' . $label->id . '" ' . $selected . '>' . ucfirst($label->name) . '</option>';

											}

										}

										?>

										</select>

									<?php

									}

									?>

									<span class="status-message"></span>

								</td>

							</tr>

							<tr>

								<td class="left">Deadline</td>
								<td class="right"><?php echo time_left($task->task_deadline); ?> <span style="color: #a1a1a1; float: right;"><?php if (($task->task_deadline != '0000-00-00 00:00:00') && ($task->task_deadline != null)) { echo date("D, d F Y", strtotime($task->task_deadline)); } else if (($task->task_deadline == '0000-00-00 00:00:00') || ($task->task_deadline == null)) { echo '-'; } ?></span></td>

							</tr>

							<tr>

								<td class="left">Module</td>
								<td class="right"><?php echo $task->category; ?></td>

							</tr>

							<tr>

								<td class="left">Path</td>
								<td class="right"><?php echo $task->path; ?></td>

							</tr>

						</table>

						<table width="100%">

							<tr>

								<td class="left">Description</td>
								<td class="right"><?php echo nl2br($task->task_description); ?></td>

							</tr>

						</table>

					</div>

					<div class="general-more">

						<h2>Timelogs</h2>

						<div class="general-logs">

							<label for="when">Insert new entry for</label>
							<input type="text" name="when" class="when calendar" id="when" value="<?php echo date("d-m-Y"); ?>">

							<label for="description98">Short description</label>
							<input type="text" name="description98" class="description98" id="description98">

							<label for="estimated2">Started at</label>
							<select id="estimated2" class="estimated_hour2" name="estimated_hour2">
								<?php

								for ($i = 0; $i < 24; $i++) {

									echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

								}

								?>
							</select>

							<select class="estimated_minute2 minutes" name="estimated_minute2">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

							<label for="estimated4">Ended at</label>
							<select id="estimated4" class="estimated_hour4" name="estimated_hour4">
								<?php

								for ($i = 0; $i < 24; $i++) {

									echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

								}

								?>
							</select>

							<select class="estimated_minute4 minutes" name="estimated_minute4">
								<option value="00">00</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

							<input type="hidden" class="project-id" value="<?php echo $task->project_id; ?>">
							<input type="hidden" class="task-id" value="<?php echo $task->task_id; ?>">

							<p style="padding: 0;"><button class="save-custom-timing">Save entry</button><span class="worklog-message" style="float: right; margin-top: 10px;"></span></p>


						</div>

						<h2>Files</h2>

						<div class="general-logs">

							<?php

							$sql3 = "SELECT * FROM `projects_bugs_files` WHERE `bug_id` = '" . $bug_id . "'";

							if ($con->query($sql3)->num_rows == 0) {

								echo '<p style="padding: 0 5px;">No files attached</p>';

							} else {

								if ($result3 = $con->query($sql3)) {

									echo '<ol>';

									while ($file = $result3->fetch_object()) {

										echo '<li><a download href="' . $file->path . '">' . $file->name . '</a></li>';

									}

									echo '</ol>';

								}

							}

							?>

						</div>

					</div>

					<?php

				}

			}

		}

	}

?>