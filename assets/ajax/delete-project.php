<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['project'])) {

		$project_id = $_POST['project'];

		if ($project_id) {

			$sql = "DELETE FROM `projects` WHERE `id` = '" . $project_id . "'";
			$con->query($sql);

			$sql2 = "DELETE FROM `projects_modules` WHERE `project_id` = '" . $project_id . "'";
			$con->query($sql2);

			$sql3 = "SELECT `id` FROM `projects_tasks` WHERE `project_id` = '" . $project_id . "'";

			if ($result3 = $con->query($sql3)) {

				while ($task = $result3->fetch_object()) {

					$sql4 = "DELETE FROM `projects_tasks_schedule` WHERE `task_id` = '" . $task->id . "'";
					$con->query($sql4);

				}

			}

			$sql5 = "DELETE FROM `projects_tasks` WHERE `project_id` = '" . $project_id . "'";
			$con->query($sql5);

			$sql6 = "DELETE FROM `projects_tasks_users` WHERE `project_id` = '" . $project_id . "'";
			$con->query($sql6);

			$sql7 = "DELETE FROM `projects_users` WHERE `project_id` = '" . $project_id . "'";
			$con->query($sql7);

		}

	}

?>