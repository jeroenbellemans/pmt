<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['name'])) {

		$task = $_POST['task'];
		$name = htmlentities($_POST['name']);
		$responsible = htmlentities($_POST['responsible']);
		$responsible2 = htmlentities($_POST['responsible2']);
		$deadline = date("Y-m-d", strtotime($_POST['deadline']));
		$estimate_hour = htmlentities($_POST['estimated_hour']);
		$estimate_minute = htmlentities($_POST['estimated_minute']);
		$category = htmlentities($_POST['category']);
		$module = htmlentities($_POST['module']);
		$description = mysqli_real_escape_string($con, $_POST['description']);

		if ($_POST['important'] == 'false') {
			$important = '0';
		} else
		if ($_POST['important'] == 'true') {
			$important = '1';
		}

		if ($deadline == '1970-01-01') {
			$deadline == '0000-00-00';
		} else {
			$deadline = $deadline . ' 23:59:59';
		}

		if (empty($name)) {
			echo 'name';
		} else
		if (empty($responsible)) {
			echo 'responsible';
		} else
		if (empty($category)) {
			echo 'category';
		} else {

			$estimated = $estimate_hour . ':' . $estimate_minute;

			$sql = "UPDATE `projects_tasks` SET `name` = '" . $name . "', `description` = '" . $description . "', `category` = '" . $category . "', `responsible` = '" . $responsible . "', `deadline` = '" . $deadline . "', `estimated_time` = '" . $estimated . "', `important` = '" . $important . "', `module_id` = '" . $module . "', `updated_by` = '" . $_SESSION['id'] . "', `updated_at` = now() WHERE `id` = '" . $task . "'";
			$con->query($sql);

			$sql2 = "UPDATE `projects_tasks_users` SET `user_id` = '" . $responsible . "' WHERE `task_id` = '" . $task . "' AND `user_id` = '" . $responsible2 . "'";
			$con->query($sql2);

			echo 'success';
		}

	}

?>