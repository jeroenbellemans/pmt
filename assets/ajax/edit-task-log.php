<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['id'])) {

		$id = $_POST['id'];

		$sql = "SELECT * FROM `users_timings_log` WHERE `id` = '" . $id . "'";

		if ($result = $con->query($sql)) {

			while ($log = $result->fetch_object()) {

				$start_date = explode(" ", $log->start_date);
				$start_hour = explode(":", $start_date[1]);

				$end_date = explode(" ", $log->end_date);
				$end_hour = explode(":", $end_date[1]);

?>			

<div class="half">

	<div class="form-group">

		<label for="when3">When did you work on this task</label>
		<input type="text" name="when3" class="when3 calendar" id="when3" value="<?php echo date("d-m-Y", strtotime($start_date[0])); ?>">

	</div>

</div>

<div class="half" style="clear: both;">

	<div class="form-group">

		<label for="estimated20">I started around</label>
		<select id="estimated20" class="estimated_hour20" name="estimated_hour20">
			<?php

			for ($i = 0; $i < 24; $i++) {

				if ($start_hour[0] == sprintf("%02d", $i)) {
					$selected = 'selected="selected"';
				} else {
					$selected = '';
				}

				echo '<option value="' . sprintf("%02d", $i) . '" ' . $selected . '>' . sprintf("%02d", $i) . '</option>';

			}

			?>
		</select>

		<select class="estimated_minute20" name="estimated_minute20">
			<?php

			for ($i = 0; $i < 56; ($i+=5)) {

				if ($start_hour[1] == sprintf("%02d", $i)) {
					$selected = 'selected="selected"';
				} else {
					$selected = '';
				}

				echo '<option value="' . sprintf("%02d", $i) . '" ' . $selected . '>' . sprintf("%02d", $i) . '</option>';

			}

			?>
		</select>

	</div>

</div>

<div class="half">

	<div class="form-group">

		<label for="estimated40">I ended around</label>
		<select id="estimated40" class="estimated_hour40" name="estimated_hour40">
			<?php

			for ($i = 0; $i < 24; $i++) {

				if ($end_hour[0] == sprintf("%02d", $i)) {
					$selected = 'selected="selected"';
				} else {
					$selected = '';
				}

				echo '<option value="' . sprintf("%02d", $i) . '" ' . $selected . '>' . sprintf("%02d", $i) . '</option>';

			}

			?>
		</select>

		<select class="estimated_minute40" name="estimated_minute40">
			<?php

			for ($i = 0; $i < 56; ($i+=5)) {

				if ($end_hour[1] == sprintf("%02d", $i)) {
					$selected = 'selected="selected"';
				} else {
					$selected = '';
				}

				echo '<option value="' . sprintf("%02d", $i) . '" ' . $selected . '>' . sprintf("%02d", $i) . '</option>';

			}

			?>
		</select>

	</div>

</div>

<?php

			}
	
		}

	}

?>	