<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['task'])) {
		
		$task_id = $_POST['task'];
		$user_id = $_POST['user'];

		$date = $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'];

		if ($task_id && $user_id && $date) {

			// Mark this task as planned

			$check_existance = "SELECT * FROM `projects_tasks_schedule` WHERE `task_id` = '" . $task_id . "'";

			if ($check = $con->query($check_existance)) {

				$count = $check->num_rows;

				if ($count == 0) {

					$sql = "UPDATE `projects_tasks` SET `planned` = '1' WHERE `id` = '" . $task_id . "'";
					$con->query($sql);

					// Insert the task into the schedule

					$sql2 = "INSERT INTO `projects_tasks_schedule` (`task_id`, `user_id`, `date`) VALUES ('" . $task_id . "', '" . $user_id . "', '" . $date . "')";
					$con->query($sql2);

					// Return the total estimated hours for that day, for that user

					$sql3 = "SELECT * FROM `projects_tasks_schedule` WHERE `date` = '" . $date . "' AND `user_id` = '" . $user_id . "'";

					if ($result3 = $con->query($sql3)) {

						while ($schedule_task = $result3->fetch_object()) {

							$sql4 = "SELECT `estimated_time` FROM `projects_tasks` WHERE `id` = '" . $schedule_task->task_id . "'";

							if ($result4 = $con->query($sql4)) {

								while ($task = $result4->fetch_object()) {

									echo $task->estimated_time;

								}

							}

						}

					}

				} else

				if ($count > 0) {

					$sql = "DELETE FROM `projects_tasks_schedule` WHERE `task_id` = '" . $task_id . "'";
					$con->query($sql);

					$sql2 = "INSERT INTO `projects_tasks_schedule` (`task_id`, `user_id`, `date`) VALUES ('" . $task_id . "', '" . $user_id . "', '" . $date . "')";
					$con->query($sql2);

					// Return the total estimated hours for that day, for that user

					$sql3 = "SELECT * FROM `projects_tasks_schedule` WHERE `date` = '" . $date . "' AND `user_id` = '" . $user_id . "'";

					if ($result3 = $con->query($sql3)) {

						while ($schedule_task = $result3->fetch_object()) {

							$sql4 = "SELECT `estimated_time` FROM `projects_tasks` WHERE `id` = '" . $schedule_task->task_id . "'";

							if ($result4 = $con->query($sql4)) {

								while ($task = $result4->fetch_object()) {

									echo $task->estimated_time;

								}

							}

						}

					}

				}

			}

		}

	}

?>