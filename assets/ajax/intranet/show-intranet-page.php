<?php
	
	session_start();
	require '../../../database/my-connection.php';

	if (!isset($_POST['project'])) {

	?>

	<div class="messages">
	</div>

	<div class="notifications">

		<?php

		$curdate = date("Y-m-d");

		$query_16 = "SELECT
						*,
						users.name AS name, 
						users.surname AS surname 
					FROM `calendar_holliday` 
						INNER JOIN users ON users.id = calendar_holliday.user_id 
					WHERE 
						`start_date` <= '" . $curdate . "' 
					AND 
						`end_date` >= '" . $curdate . "' 
					AND 
						calendar_holliday.status = '1' 
					ORDER BY 
						`end_date` 
					ASC
					";

		$query_17 = "SELECT 
						*, 
						users.name AS name, 
						users.surname AS surname 
					FROM `calendar_remotework` 
						INNER JOIN users ON users.id = calendar_remotework.user_id 
						WHERE 
							`start_date` <= '" . $curdate . "' 
						AND 
							`end_date` >= '" . $curdate . "' 
						AND 
							calendar_remotework.status = '1' 
						ORDER BY `end_date` 
						ASC
						";

		$query_18 = "SELECT 
						*, 
						users.name AS name, 
						users.surname AS surname 
					FROM `calendar_sickness` 
						INNER JOIN users ON users.id = calendar_sickness.user_id 
					WHERE 
						`start_date` <= '" . $curdate . "' 
					AND 
						`end_date` >= '" . $curdate . "'
					";

		if (($con->query($query_16)->num_rows == 0)) {

			echo '<p class="no-event">No people are on holiday <span class="request request-holiday">Request</span></p>';

		} else {

			if ($result = $con->query($query_16)) {

				echo '<div class="holidays">';

				echo '<h2>Currently on holiday <span class="request request-holiday">Request</span></h2>';

				while ($holiday = $result->fetch_object()) {

					echo '<p>' . ucfirst($holiday->name) . ' ' . ucfirst($holiday->surname) . '  <span class="returning"><strong>' . date("d-M-Y", (strtotime($holiday->end_date) + 86400)) . '</strong></span></p>';

				}

				echo '</div>';

			}

		}

		if (($con->query($query_17)->num_rows == 0)) {

			echo '<p class="no-event">No people are working remote <span class="request request-remote">Request</span></p>';

		} else {

			if ($result2 = $con->query($query_17)) {

				echo '<div class="holidays">';

				echo '<h2>Working remote <span class="request request-holiday">Request</span></h2>';

				while ($remote = $result2->fetch_object()) {

					echo '<p>' . ucfirst($remote->name) . ' ' . ucfirst($remote->surname) . '</p>';

				}

				echo '</div>';

			}

		}

		if (($con->query($query_18)->num_rows == 0)) {

			echo '<p class="no-event">No people are sick <span class="request request-sickness">Report</span></p>';

		} else {

			if ($result3 = $con->query($query_18)) {

				echo '<div class="holidays">';

				echo '<h2>Currently sick <span class="request request-holiday">Report</span></h2>';

				while ($remote = $result2->fetch_object()) {

					echo '<p>' . ucfirst($remote->name) . ' ' . ucfirst($remote->surname) . '</p>';

				}

				echo '</div>';

			}

		}

		?>

	</div>

	<?php 

	} 

?>