<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['task'])) {

		$task_id = $_POST['task'];

		if ($task_id) {

			$sql = "SELECT * FROM `projects_tasks` WHERE `id` = '" . $task_id . "'";

			if ($result = $con->query($sql)) {

				while ($task = $result->fetch_object()) { 

					if ($task->important == '1') {
						$checked = 'checked="checked"';
					} else {
						$checked = '';
					}

				?>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="name3">The name of the task</label>
							<input type="text" name="name" class="name3" id="name3" value="<?php echo $task->name; ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="responsible2">Who's responsible for this task</label>
							<select id="responsible2" class="responsible2" name="responsible">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `users`";

									if ($result = $con->query($sql)) {

										while ($user = $result->fetch_object()) { 

											if ($user->id == $task->responsible) {
												$selected = 'selected="selected"';
											} else {
												$selected = '';
											}

										?>

										<option value="<?php echo $user->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="datepicker2">Is there a deadline</label>
							<input type="text" name="deadline" class="deadline2 calendar" id="datepicker2" value="<?php echo str_replace('30-11--0001', '', date("d-m-Y", strtotime($task->deadline))); ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="category2">Where would you classify it</label>
							<select id="category2" class="category2" name="category">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `projects_categories` ORDER BY `name` ASC";

									if ($result = $con->query($sql)) {

										while ($category = $result->fetch_object()) { 

											if ($category->id == $task->category) {
												$selected = 'selected="selected"';
											} else {
												$selected = '';
											}

										?>

										<option value="<?php echo $category->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($category->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<?php $strings = explode(":", $task->estimated_time); ?>

							<label for="estimated2">How long would this task take</label>
							<select id="estimated2" class="estimated_hour2" name="estimated_hour">
								<option value="<?php echo $strings[0]; ?>"><?php echo $strings[0]; ?></option>
								<option value="">-</option>
									<?php

									for ($i = 0; $i < 201; $i++) {

										echo '<option value="' . sprintf("%02d", $i) . '">' . sprintf("%02d", $i) . '</option>';

									}

									?>
							</select>

							<select class="estimated_minute2" name="estimated_minute">
								<option value="<?php echo $strings[1]; ?>"><?php echo $strings[1]; ?></option>
								<option value="">-</option>
								<option value="05">05</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="module2">Link to</label>
							<select name="module" class="module2" id="module2">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $task->project_id . "'";

									if ($result = $con->query($sql)) {

										while ($module = $result->fetch_object()) { 

											if ($module->id == $task->module_id) {
												$selected = 'selected="selected"';
											} else {
												$selected = '';
											}

										?>

										<option value="<?php echo $module->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($module->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="full">

						<div class="form-group">

							<label for="description2">Maybe describe the task a little</label>
							<textarea name="description" class="description2" id="description2"><?php echo $task->description; ?></textarea>

						</div>

					</div>

					<div class="full">

						<div class="form-group">
							
							<label for="important">Mark this task as important</label>
							<label><input type="checkbox" <?php echo $checked; ?> name="important" id="important2" value="1"> <span class="label">Yes, mark as important</span></label>
							<input type="hidden" name="task" class="project-task-id2" value="<?php echo $task->id; ?>"> 
							<input type="hidden" name="resp" class="former_resp2" value="<?php echo $task->id; ?>">

						</div>

					</div>

				</div>

				<?php
					
				}

			}

		}

	}

?>