<?php
	
	session_start();
	require '../../database/my-connection.php';
	require '../../config/functions/time_left.php';

	$sql2 = "SELECT 
				*,
				projects_bugs.name AS bug_name,
				projects_bugs.deadline AS bug_deadline,
				projects_bugs.id AS bug_id,
				projects_bugs.important AS important_bug,
				users.name AS bug_creator_name,
				users.surname AS bug_creator_surname,
				users2.name AS bug_responsible_name,
				users2.surname AS bug_responsible_surname,
				projects.name AS project_name,
				labels.name AS label_name
			FROM `projects_bugs` 
				INNER JOIN `projects` ON projects.id = projects_bugs.project_id
				INNER JOIN `users` ON users.id = projects_bugs.created_by
				INNER JOIN `users` AS users2 ON users2.id = projects_bugs.responsible
				INNER JOIN `labels` ON labels.id = projects_bugs.status
			WHERE 
				projects_bugs.status != '6' 
			AND 
				projects_bugs.responsible = '" . $_SESSION['id'] . "'
			AND 
				projects_bugs.completed != '1'
			ORDER BY 
				projects_bugs.deadline = '0000-00-00 00:00:00', 
				projects_bugs.deadline ASC, 
				projects_bugs.created_at ASC
			";

	if ($result2 = $con->query($sql2)) {

		if ($result2->num_rows == 0) {

			echo '<p class="no-tasks">You do not have any bugs to fix</p>';

		} else {

			?>

			<table width="100%">

				<thead>

					<th width="3%">Ref</th>
					<th width="18%">Name</th>
					<th width="3%"></th>
					<th width="3%"></th>
					<th width="18%">Project</th>
					<th width="9%">Category</th>
					<th width="8%">Deadline</th>
					<th width="10%">Label</th>
					<th width="14%">Responsible</th>
					<th width="14%">Assigned by</th>

				</thead>

				<tbody>

			<?php

			while ($bug = $result2->fetch_object()) {

				?>

				<tr class="no-divider" data-id="<?php echo $bug->bug_id; ?>" data-type="bug">

					<td class="<?php echo define_priority($bug->bug_deadline); ?>" width="3%">#<?php echo sprintf("%04d", $bug->bug_id); ?></td>
					<td width="18%"><?php echo ucfirst($bug->bug_name); ?></td>
					<td width="3%"><?php if ($bug->important_bug == 1) : echo '<span class="icon icon-notification"></span>'; endif; ?></td>
					<td width="3%">&nbsp;</td>
					<td width="18%"><?php echo ucfirst($bug->project_name); ?></td>
					<td width="9%"><?php echo ucfirst($bug->category); ?></td>
					<td width="8%"><?php echo time_left($bug->bug_deadline); ?></td>
					<td class="the-label" data-id="<?php echo $bug->bug_id; ?>" width="10%"><span class="<?php echo str_replace(' ', '-', $bug->label_name); ?>"><?php echo ucfirst($bug->label_name); ?></span></td>
					<td width="14%"><?php echo ucfirst($bug->bug_responsible_name); ?> <?php echo ucfirst($bug->bug_responsible_surname); ?></td>
					<td width="14%"><?php echo ucfirst($bug->bug_creator_name); ?> <?php echo ucfirst($bug->bug_creator_surname); ?></td>

				</tr>

				<?php

			}

			echo '</tbody>';

		echo '</table>';

		}

	}

	?>