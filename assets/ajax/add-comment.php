<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['task'])) {

		$task_id = $_POST['task'];
		$project_id = $_POST['project'];
		$comment = $con->real_escape_string($_POST['comment']);

		if ($task_id && $comment) {

			$sql = "INSERT INTO `projects_tasks_comments` (`message`, `task_id`, `created_at`, `created_by`) VALUES ('" . $comment . "', '" . $task_id . "', now(), '" . $_SESSION['id'] . "')";
			$con->query($sql);

			$inserted_id = mysqli_insert_id($con);

			$sql2 = "SELECT * FROM `projects_tasks_comments` WHERE `id` = '" . $inserted_id . "'";

			if ($result = $con->query($sql2)) {

				while ($comment = $result->fetch_object()) { 

					$sql3 = "SELECT * FROM `users` WHERE `id` = '" . $comment->created_by . "'";

					if ($result2 = $con->query($sql3)) {

						while ($user = $result2->fetch_object()) { ?>

						<div class="actual-message">

							<div class="post-details">

								<p><span class="author-name"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></span> <span class="post-date"><?php echo date("D d/m/Y", strtotime($comment->created_at)); ?></span></p>

							</div>

							<div class="post-message">

								<?php echo nl2br($comment->message); ?>

							</div>

						</div>

						<?php

						}

					}

				}

			}

			$sql4 = "SELECT * FROM `projects_tasks` WHERE `id` = '" . $task_id . "'";

			if ($result4 = $con->query($sql4)) {

				while ($task = $result4->fetch_object()) {

					if ($task->responsible != $_SESSION['id']) {

						$sql5 = "SELECT * FROM `users` WHERE `id` = '" . $task->responsible . "'";

						if ($result5 = $con->query($sql5)) {

							while ($user2 = $result5->fetch_object()) {

								$sql6 = "SELECT * FROM `users` WHERE `id` = '" . $_SESSION['id'] . "'";

								if ($result6 = $con->query($sql6)) {

									while ($sender = $result6->fetch_object()) {

									$to = $user2->email;
									$from = "info@time2change.com";
									$subject = "New comment in #" . $task->id . " " . ucfirst($task->name) . "";
									
									// Write the contents of your e-mail here using HTML code
									$message = '
									<h1>Hello ' . ucfirst($user2->name) . '</h1>
									<h2>' . ucfirst($sender->name) . ' ' . ucfirst($sender->surname) . ' left a new comment</h2>
									<p>There is a new comment available in #' . $task->id . ' ' . ucfirst($task->name) . '. Make sure to read it and if necessary respond to it. <a href="pmt.time2change.com/index.php?pid=' . $project_id . '&task=' . $task_id . '&redirect=true&uid=' . $user2->id . '">Click here</a> to view the comment.</p>
									<br><br>
									<p>PMT &copy; Time2Change</p>
									';
												
									$headers = "From: $from\r\n";
									$headers .= "Content-type: text/html\r\n";
									$to = $to;
									
									// Send the actual mail to the users
									mail($to, $subject, $message, $headers);

									}

								}

							}

						}

					}

				}

			}

		}

	}

?>