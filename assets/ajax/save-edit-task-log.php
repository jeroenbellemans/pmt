<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['hour1'])) {

		$starthour = $_POST['hour1'];
		$startminute = $_POST['min1'];
		$endhour = $_POST['hour2'];
		$endminute = $_POST['min2'];
		$date = $_POST['date'];
		$task_id = $_POST['id'];

		if (empty($date)) {

			echo 'date'; // Data response jquery -> missing date

		} else

		if (!empty($starthour) || !empty($startminute) || !empty($endhour) || !empty($endminute) || !empty($date)) {

			if ($endhour < $starthour) {

				echo 'negative'; // Data response jquery -> negative hours

			} else {

				$start_date = $date . ' ' . $starthour .':' . $startminute . ':00';
				$end_date = $date . ' ' . $endhour .':' . $endminute . ':00';

				$sql2 = "SELECT * FROM `projects_tasks` WHERE `id` = '" . $task_id . "'";

				if ($result2 = $con->query($sql2)) {

					while ($task = $result2->fetch_object()) {

						$start_date = strtotime($start_date);
						$end_date = strtotime($end_date);

						$difference = gmdate("H:i", ($end_date - $start_date));
						$difference = explode(":", $difference);

						$start_date = date("Y-m-d H:i:s", $start_date);
						$end_date = date("Y-m-d H:i:s", $end_date);


						if ($task->effective_time != '' || $task->effective_time === null) {

							$time = explode(":", $task->effective_time);

							$new_hour = $time[0] + $difference[0];
							$new_minute = $time[1] + $difference[1];

							if ($new_minute >= 60) {
								$new_hour = $new_hour + 1;
								$new_minute = $new_minute - 60;
							}

							$new_time = sprintf("%02d", $new_hour) . ':' . sprintf("%02d", $new_minute);

							$sql = "UPDATE `users_timings_log` SET `start_date` = '" . $start_date . "', `end_date` = '" . $end_date . "' WHERE `id` = '" . $task_id . "'";
							$con->query($sql);

							echo 'success';

						}

					}

				}

			}

		}

	}

?>