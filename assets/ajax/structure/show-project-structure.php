<?php
	
	session_start();
	require '../../../database/my-connection.php';

	if (isset($_POST['project'])) {

		$project_id = $con->real_escape_string($_POST['project']);

	?>

	<div class="messages">

		<?php

		$sql = "SELECT * FROM `projects` WHERE `id` = '" . $project_id . "'";

		if ($result = $con->query($sql)) {

			echo '<ul class="top-level">';

			while ($project = $result->fetch_object()) {

				echo '<li class="folder"><span class="hover-text">' . ucfirst($project->name) . '</span>';

				$sql2 = "SELECT * FROM `projects_modules` WHERE `project_id` = '" . $project->id . "'";

				if ($result2 = $con->query($sql2)) {

					echo '<ul class="second-level">';

					while ($module = $result2->fetch_object()) {

						echo '<li class="subfolder"><span class="hover-text">' . ucfirst($module->name) . '</span>';

						$sql3 = "SELECT 
									*,
									labels.name AS label_name,
									projects_tasks.id AS task_id,
									projects_tasks.name AS task_name,
									users.name AS creator_name,
									users.surname AS creator_surname,
									users2.name AS responsible_name,
									users2.surname AS responsible_surname
								 FROM `projects_tasks`
								 	INNER JOIN `labels` ON labels.id = projects_tasks.label
								 	INNER JOIN `users` ON users.id = projects_tasks.created_by
								 	INNER JOIN `users` AS `users2` ON users2.id = projects_tasks.responsible
								 WHERE 
								 	`module_id` = '" . $module->id . "'";

						if ($result3 = $con->query($sql3)) {

							echo '<ul class="third-level">';

							while ($task = $result3->fetch_object()) {

								echo '
									<li class="subfolder-item">
										<span class="name treeview-task" data-id="' . $task->task_id . '" data-type="task">' . ucfirst($task->task_name) . '</span>
										<span class="label">' . $task->label_name . '</span>
										<span class="creator">' . ucfirst($task->responsible_name) . ' ' . ucfirst($task->responsible_surname) . '</span>
										<span class="arrow"><span class="icon icon-arrow-right"></span></span>
										<span class="responsible">' . ucfirst($task->creator_name) . ' ' . ucfirst($task->creator_surname) . '</span>
									</li>
									';

							}

							echo '</ul>';

						}

						echo '</li>';

					}

					echo '</ul>';

					$sql3 = "SELECT 
								*,
								labels.name AS label_name,
								projects_tasks.name AS task_name,
								users.name AS creator_name,
								users.surname AS creator_surname,
								users2.name AS responsible_name,
								users2.surname AS responsible_surname
							 FROM `projects_tasks`
							 	INNER JOIN `labels` ON labels.id = projects_tasks.label
							 	INNER JOIN `users` ON users.id = projects_tasks.created_by
							 	INNER JOIN `users` AS `users2` ON users2.id = projects_tasks.responsible
							 WHERE 
							 	`module_id` = '0'";

					if ($con->query($sql3)->num_rows > 0) {

						echo '<ul class="second-level">';

							echo '<li class="subfolder"><span class="hover-text">Temporary Module</span>';

							if ($result3 = $con->query($sql3)) {

								echo '<ul class="third-level">';

								while ($task = $result3->fetch_object()) {

									echo '
										<li class="subfolder-item">
											<span class="name">' . ucfirst($task->task_name) . '</span>
											<span class="label">' . $task->label_name . '</span>
											<span class="creator">' . ucfirst($task->responsible_name) . ' ' . ucfirst($task->responsible_surname) . '</span>
											<span class="arrow"><span class="icon icon-arrow-right"></span></span>
											<span class="responsible">' . ucfirst($task->creator_name) . ' ' . ucfirst($task->creator_surname) . '</span>
										</li>
										';

								}

								echo '</ul>';

							}

							echo '</li>';

						echo '</ul>';

					}

				}

				echo '</li>';

			}

			echo '</ul>';

		}

		?>

	</div>

	<div class="notifications">

		<?php

		$sql = "SELECT 
					*,
					projects.name AS project_name,
					users.name AS firstname,
					users.surname AS surname,
					users2.name AS resp_firstname,
					users2.surname AS resp_surname,
					labels.name AS label_name
				FROM `projects`
					INNER JOIN `users` ON users.id = projects.created_by
					INNER JOIN `users` AS users2 ON users2.id = projects.responsible
					INNER JOIN `labels` ON labels.id = projects.label
				WHERE
					projects.id = '" . $project_id . "'
				";

		if ($result = $con->query($sql)) {

			echo '<div class="holidays">';

			while ($project = $result->fetch_object()) { ?>

			<h2><?php echo ucfirst($project->project_name); ?> <span class="request edit-project">Edit</span></h2>

			<p>Deadline <span class="returning"><strong><?php echo date("D, d F Y", strtotime($project->deadline)); ?></strong></span></p>
			<p>Customer <span class="returning"><strong><?php echo ucfirst($project->customer); ?></strong></span></p>
			<p>Responsible <span class="returning"><strong><?php echo ucfirst($project->resp_firstname); ?> <?php echo ucfirst($project->resp_surname); ?></strong></span></p>

			<p class="description"><?php echo nl2br($project->description); ?></p>

			<?php

			}

			echo '</div>';

		}

		?>

	</div>

	<?php

	}

?>