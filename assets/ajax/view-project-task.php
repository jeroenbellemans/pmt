<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['task-id'])) {

		$id = $_POST['task-id'];
		$project_id = $_POST['project'];

		if (!empty($id)) {

			$sql = "SELECT * FROM `projects_tasks` WHERE `id` = '" . $id . "'";

			if ($result = $con->query($sql)) {

				$count = $result->num_rows;

				if ($count == 0) {

					echo '<p>This task does not exists</p>';

				} else {

					while ($task = $result->fetch_object()) { 

						$estimated = explode(":", $task->estimated_time);
						$effective = explode(":", $task->effective_time);

					?>

					<div class="form-body">

						<div class="half">

							<div class="form-group">

								<h2>General</h2>

								<table class="task-info" width="100%">

									<tr>

										<td class="q">

											Taskname

										</td>

										<td class="a">

											<?php echo ucfirst($task->name); ?>

										</td>

									</tr>

									<tr>

										<td class="q">

											Created by

										</td>

										<td class="a">

											<?php

											$sql = "SELECT * FROM `users` WHERE `id` = '" . $task->created_by . "'";

											if ($result = $con->query($sql)) {

												while ($user = $result->fetch_object()) {

													echo ucfirst($user->name) . ' ' . ucfirst($user->surname);

												}

											}

											?>

										</td>

									</tr>

									<tr>

										<td class="q">

											Current status

										</td>

										<td class="a">

											<select name="label3" class="label3" id="label3">

												<?php

													$sql = "SELECT * FROM `labels` WHERE `type` = '2'";

													if ($result = $con->query($sql)) {

														while ($label = $result->fetch_object()) { 

															if ($task->label == $label->id) {
																$selected = 'selected="selected"';
															} else {
																$selected = '';
															}

														?>

														<option <?php echo $selected; ?> value="<?php echo $label->id; ?>"><?php echo ucfirst($label->name); ?></option>

														<?php

														}

													}

												?>

											</select>

										</td>

									</tr>

									<tr>

										<td class="q">

											Deadline

										</td>

										<td class="a">

											<?php if (($task->deadline != '0000-00-00 00:00:00') && ($task->deadline != null)) { echo date("D, d F Y", strtotime($task->deadline)); } else if (($task->deadline == '0000-00-00 00:00:00') || ($task->deadline == null)) { echo '-'; } ?>

										</td>

									</tr>

									<tr>

										<td class="q">

											Estimated hours

										</td>

										<td class="a">

											<?php echo sprintf("%02d", $estimated[0]) . ':' . sprintf("%02d", $estimated[1]); ?>

										</td>

									</tr>

									<tr>

										<td class="q">

											Total logged hours

										</td>

										<td class="a">

											<?php echo sprintf("%02d", $effective[0]) . ':' . sprintf("%02d", $effective[1]); ?>

										</td>

									</tr>

									<tr>

										<td class="q" valign="top">

											Description

										</td>

										<td class="a">

											<?php echo nl2br($task->description); ?>

										</td>

									</tr>

								</table>

							</div>

							<div class="form-group">

								<h2 style="margin-top: 20px;">Logs</h2>

								<div class="task-logs">

									<?php

									$sql = "SELECT * FROM `users_timings_log` WHERE `type` = '0' AND `type_id` = '" . $id . "'";

									if ($result = $con->query($sql)) {

										$count = $result->num_rows;

										if ($count == 0) { ?>

										<p class="notification">No logs have been registered yet</p>

										<?php

										} else {

											echo '<table class="logs-only" width="100%">';

											while ($logs = $result->fetch_object()) { ?>

											<tr>

												<td>

													<?php echo date("D, d/m/Y", strtotime($logs->date)); ?>

												</td>

												<td>

													<?php

													$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $logs->user_id . "'";

													if ($result2 = $con->query($sql2)) {

														while ($user = $result2->fetch_object()) {

															echo ucfirst($user->name) . ' ' . ucfirst($user->surname);

														}

													}

													?>

												</td>

												<td>

													<?php echo date("H:i", strtotime($logs->start_date)); ?>

												</td>

												<td align="right">

													<?php echo date("H:i", strtotime($logs->end_date)); ?>

												</td>

											</tr>

											<?php

											}

											echo '</table>';

										}

									}

									?>

								</div>

							</div>

						</div>

						<div class="half2">

							<div class="form-group">

								<h2>Comments</h2>

								<div class="post-messages">

								<?php

								$sql = "SELECT * FROM `projects_tasks_comments` WHERE `task_id` = '" . $task->id . "' ORDER BY `created_at` DESC";

								if ($result = $con->query($sql)) {

									$count = $result->num_rows;

									if ($count == 0) { ?>

									<p class="notification">No comments have been posted yet</p>

									<?php

									} else {

										while ($comment = $result->fetch_object()) { 

											$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $comment->created_by . "'";

											if ($result2 = $con->query($sql2)) {

												while ($user = $result2->fetch_object()) { ?>

												<div class="actual-message">

													<div class="post-details">

														<p><span class="author-name"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></span> <span class="post-date"><?php echo date("D d/m/Y", strtotime($comment->created_at)); ?></span></p>

													</div>

													<div class="post-message">

														<p><?php echo nl2br($comment->message); ?></p>

													</div>

												</div>

												<?php

												}

											}

										}

									}

								}

								?>

								</div>

								<div class="write-message">

									<textarea class="add-comment" placeholder="Write a message"></textarea>
									<input type="hidden" class="task-id" value="<?php echo $task->id; ?>">
									<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">

									<input type="submit" name="submit-comment" class="submit-comment" value="Post message">

								</div>

							</div>

							<div class="form-group">

								<h2 style="margin-top: 50px;">Files</h2>

								<ol>

								<?php

								$sql = "SELECT * FROM `projects_files` WHERE `type` = '0' AND `type_id` = '" . $task->module_id . "' OR `type` = '1' AND `type_id` = '" . $task->id . "'";

								if ($result = $con->query($sql)) {

									while ($file = $result->fetch_object()) { ?>

									<li><a href="<?php echo $file->href; ?>" download><?php echo ucfirst($file->name); ?></a></li>

									<?php

									}

								}

								?>

								</ol>

							</div>

						</div>

					</div>

					<?php

					}

				}

			}

		}

	}

?>