<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['date'])) {

		$date = $_POST['date'];

		if ($date) {

			$sql = "INSERT INTO `calendar_holliday` (`date`, `status`, `user_id`) VALUES ('" . $date . "', '0', '" . $_SESSION['id'] . "')";
			$con->query($sql);
			$inserted_request = mysqli_insert_id($con);

			$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $_SESSION['id'] . "'";

			if ($result2 = $con->query($sql2)) {

				while ($user = $result2->fetch_object()) {

					$email = "mario.vervaeke@time2change.com";

				    // Send a mail to the user with his details to log in to the platform

				    $to = $email;
					$from = "pmt@time2change.com";
					$subject = ucfirst($user->name) . ' ' . ucfirst($user->surname) . ' has send a request';
					
					// Write the contents of your e-mail here using HTML code
					$message = '
					<h1>Hi Mario</h1>
					<h2>' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . ' requested a day off for ' . date("d, d F Y") . '</h2>
					<p>If you <strong>accept</strong> this request, <a href="index.php?cat=requests&id=' . $inserted_request . '&accept=true">click here</a>.</p>
					<p>If you do <strong>not accept</strong> this request, <a href="index.php?cat=requests&id=' . $inserted_request . '&accept=false">click here</a>.</p>
					<p>Check <a href="index.php?cat=calendar&option=match">the schedule</a> if you are not sure if it is possible.</p>
					<br><br>
					<p>PMT &copy; Time2Change</p>
					';
					
					$headers = "From: $from\r\n";
					$headers .= "Content-type: text/html\r\n";
					$to = $to;
					
					// Send the actual mail to the users
					mail($to, $subject, $message, $headers);

				}

			}

		}

	}

?>