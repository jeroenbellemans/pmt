<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['bug'])) {

		$bug_id = $_POST['bug'];

		if ($bug_id) {

			$sql = "SELECT * FROM `projects_bugs` WHERE `id` = '" . $bug_id . "'";

			if ($result = $con->query($sql)) {

				while ($bug = $result->fetch_object()) { 

					if ($bug->important == '1') {
						$checked = 'checked="checked"';
					} else {
						$checked = '';
					}

				?>

				<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="bugname2">The name of the bug</label>
							<input type="text" name="bugname2" class="bugname2" id="bugname2" required value="<?php echo ucfirst($bug->name); ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="responsible6">Who's responsible for this bug</label>
							<select id="responsible6" class="responsible6" name="responsible6" required>
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `users`";

									if ($result = $con->query($sql)) {

										while ($user = $result->fetch_object()) {

											if ($user->id == $bug->responsible) {
												$selected = 'selected="selected"';
											} else {
												$selected = '';
											}

										?>

										<option <?php echo $selected; ?> value="<?php echo $user->id; ?>"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="deadline6">Is there a deadline</label>
							<input type="text" name="deadline6" class="deadline6 calendar" id="deadline6" value="<?php echo str_replace('30-11--0001', '', date("d-m-Y", strtotime($bug->deadline))); ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="choose_file2">Attach file(s)</label>
							<input type="file" id="choose_file2" class="choose_file2" name="choose_file2[]" multiple style="margin-bottom: 20px; margin-top: 5px;">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="path2">Path/link to the bug</label>
							<input type="text" name="path2" class="path2" id="path2" value="<?php echo $bug->path; ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="category3">Module</label>
							<input type="text" name="category3" class="category3" id="category3" value="<?php echo $bug->category; ?>">

						</div>

					</div>

					<div class="full">

						<div class="form-group">

							<label for="description6">Please give a detailed description of the bug</label>
							<textarea name="description6" class="description6" id="description6" required><?php echo $bug->description; ?></textarea>

						</div>

					</div>

					<div class="full">

						<div class="form-group">
							
							<label for="important4">Mark this bug as important</label>
							<label><input type="checkbox" <?php echo $checked; ?> name="important4" id="important4" value="1"> <span class="label">Yes, mark as important</span></label>
							<input type="hidden" name="bug-id" class="bug-id" value="<?php echo $bug->id; ?>">

						</div>

					</div>

				</div>

				<?php
					
				}

			}

		}

	}

?>