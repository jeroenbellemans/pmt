<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['user'])) {

		$user_id = $_POST['user'];
		$date = $_POST['year'] .'-' . $_POST['month'] . '-' . $_POST['day'];

		if ($user_id) {

			$sql = "SELECT * FROM `projects_tasks_schedule` WHERE `date` = '" . $date . "' AND `user_id` = '" . $user_id . "'";

			if ($result = $con->query($sql)) {

				$count = $result->num_rows;

				if ($count == 0) {

					echo '<p>There are no planned tasks on that day</p>';

				} else { ?>

				<table>

					<thead>

						<th>

							Name

						</th>

						<th>

							Project

						</th>

						<th>

							Type

						</th>

						<th>

							Label

						</th>

						<th>

							Estimated

						</th>

						<th>

							Effective

						</th>

					</thead>

					<tbody>

				<?php

					while ($task_schedule = $result->fetch_object()) {

						$sql2 = "SELECT * FROM `projects_tasks` WHERE `id` = '" . $task_schedule->task_id . "'";

						if ($result2 = $con->query($sql2)) {

							while ($task = $result2->fetch_object()) {

								$sql3 = "SELECT * FROM `projects` WHERE `id` = '" . $task->project_id . "'";

								if ($result3 = $con->query($sql3)) {

									while ($project = $result3->fetch_object()) {

										$sql4 = "SELECT * FROM `labels` WHERE `id` = '" . $task->label . "'";

										if ($result4 = $con->query($sql4)) {

											while ($label = $result4->fetch_object()) {

												$sql5 = "SELECT * FROM `projects_categories` WHERE `id` = '" . $task->category . "'";

												if ($result5 = $con->query($sql5)) {

													while ($category = $result5->fetch_object()) {

														echo '
														<tr>

															<td>

																' . ucfirst($task->name) . '

															</td>

															<td>

																' . ucfirst($project->name) . '

															</td>

															<td>

																' . ucfirst($category->name) . '

															</td>

															<td>

																<span class="' . strtolower(str_replace(' ', '-', $label->name)) . '">' . ucfirst($label->name) . '</span>

															</td>

															<td>';

																if ($task->estimated_time == '') {
																	echo '-';
																} else {
																	echo $task->estimated_time;
																}

															echo '
															</td>

															<td width="80px">

																<div class="fill-in-box">

																</div>

															</td>

														</tr>
														';

													}

												}

											}

										}

									}

								}

							}

						}

					} ?>

					</tbody>

				</table>

				<?php

				}

			}

		}

	}

?>