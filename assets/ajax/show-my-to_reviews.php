<?php
	
	session_start();
	require '../../database/my-connection.php';
	require '../../config/functions/time_left.php';

	// SELECT ALL TASKS AWAITING REVISION

	$sql = "SELECT 
				*,
				projects_tasks.id AS task_id,
				projects_tasks.name AS task_name,
				projects_tasks.deadline AS task_deadline,
				projects_tasks.important AS important_task,
				users.name AS task_creator_name,
				users.surname AS task_creator_surname,
				users2.name AS task_responsible_name,
				users2.surname AS task_responsible_surname,
				projects.name AS project_name, 
				projects_categories.name AS category_name,
				labels.name AS label_name
			FROM `projects_tasks` 
				INNER JOIN `projects` ON projects.id = projects_tasks.project_id
				INNER JOIN `projects_categories` ON projects_categories.id = projects_tasks.category
				INNER JOIN `users` ON users.id = projects_tasks.created_by
				INNER JOIN `users` AS users2 ON users2.id = projects_tasks.responsible
				INNER JOIN `labels` ON labels.id = projects_tasks.label
			WHERE 
				projects_tasks.label = '6'  
			AND
				projects_tasks.created_by = '" . $_SESSION['id'] . "'
			AND
				projects_tasks.completed = '0'
			ORDER BY 
				projects_tasks.deadline = '0000-00-00 00:00:00', 
				projects_tasks.deadline ASC, 
				projects_tasks.created_at ASC
			";

	// SELECT ALL BUGS AWAITING REVISION

	$sql2 = "SELECT 
				*,
				projects_bugs.name AS bug_name,
				projects_bugs.deadline AS bug_deadline,
				projects_bugs.id AS bug_id,
				projects_bugs.important AS important_bug,
				users.name AS bug_creator_name,
				users.surname AS bug_creator_surname,
				users2.name AS bug_responsible_name,
				users2.surname AS bug_responsible_surname,
				projects.name AS project_name,
				labels.name AS label_name
			FROM `projects_bugs` 
				INNER JOIN `projects` ON projects.id = projects_bugs.project_id
				INNER JOIN `users` ON users.id = projects_bugs.created_by
				INNER JOIN `users` AS users2 ON users2.id = projects_bugs.responsible
				INNER JOIN `labels` ON labels.id = projects_bugs.status
			WHERE 
				projects_bugs.status = '6' 
			AND
				projects_bugs.created_by = '" . $_SESSION['id'] . "'
			AND
				projects_bugs.completed = '0'
			ORDER BY 
				projects_bugs.deadline = '0000-00-00 00:00:00', 
				projects_bugs.deadline ASC, 
				projects_bugs.created_at ASC
			";

	if (($con->query($sql)->num_rows == 0) && ($con->query($sql2)->num_rows == 0)) {

		echo '<p class="no-tasks">There are no tasks/bugs awaiting your revision</p>';

	} else {

		if ($result = $con->query($sql)) { ?>

		<table width="100%">

				<thead>

					<th width="3%">Ref</th>
					<th width="18%">Name</th>
					<th width="3%"></th>
					<th width="3%"></th>
					<th width="18%">Project</th>
					<th width="9%">Category</th>
					<th width="8%">Deadline</th>
					<th width="10%">Label</th>
					<th width="14%">Responsible</th>
					<th width="14%">Assigned by</th>

				</thead>

				<?php

			while ($task = $result->fetch_object()) {

				$sql3 = "SELECT 
							*,
							projects_tasks_milestones.name AS milestone_name,
							projects_tasks_milestones.id AS milestone_id,
							users3.name AS milestone_firstname,
							users3.surname AS milestone_lastname
						FROM `projects_tasks_milestones` 
							INNER JOIN `users` AS users3 ON users3.id = projects_tasks_milestones.created_by
						WHERE 
							`task_id` = '" . $task->task_id . "'
						ORDER BY `deadline` ASC
						";

				?>

				<tr class="no-divider" data-id="<?php echo $task->task_id; ?>" data-type="task">

					<td class="<?php echo define_priority($task->task_deadline); ?>">#<?php echo sprintf("%04d", $task->task_id); ?></td>
					<td><?php echo ucfirst($task->task_name); ?></td>
					<td><?php if ($task->important_task == 1) : echo '<span class="icon icon-notification"></span>'; endif; ?></td>
					<td><?php if ($con->query($sql3)->num_rows > 0) : echo '<span class="icon icon-flag" style="font-size: 9px; color: #c1c1c1;"></span>'; endif; ?></td>
					<td><?php echo ucfirst($task->project_name); ?></td>
					<td><?php echo ucfirst($task->category_name); ?></td>
					<td><?php echo time_left($task->task_deadline); ?></td>
					<td class="the-label" data-id="<?php echo $task->task_id; ?>"><span class="<?php echo str_replace(' ', '-', $task->label_name); ?>"><?php echo ucfirst($task->label_name); ?></span></td>
					<td><?php echo ucfirst($task->task_responsible_name); ?> <?php echo ucfirst($task->task_responsible_surname); ?></td>
					<td><?php echo ucfirst($task->task_creator_name); ?> <?php echo ucfirst($task->task_creator_surname); ?></td>

				</tr>

				<?php

				if ($result3 = $con->query($sql3)) {

					while ($milestone = $result3->fetch_object()) {

						if ($milestone->completed == 1) {
							$milestone_class = 'complete icon-checkmark';
						} else {
							$milestone_class = 'incomplete icon-checkmark';
						}

						?>

						<tr class="milestone">

							<td class="milestone-bullet"><span class="<?php echo $milestone_class; ?>" data-id="<?php echo $milestone->milestone_id; ?>"></span></td>
							<td class="milestone-check" colspan="5"><?php echo $milestone->milestone_name; ?></td>

							<td class="milestone-deadline" colspan="4"><?php echo time_left($milestone->deadline); ?></td>

						</tr>

						<?php

					}

				}

			}

			echo '</table>';

		}

	?>

	<?php

		if ($result2 = $con->query($sql2)) { ?>

		<table width="100%">

			<tr>

				<td class="view-divider" colspan="10"></td>

			</tr>

		</table>

		<table width="100%">

			<?php

			while ($bug = $result2->fetch_object()) {

				?>

				<tr class="no-divider" data-id="<?php echo $bug->bug_id; ?>" data-type="bug">

					<td class="<?php echo define_priority($bug->bug_deadline); ?>" width="3%">#<?php echo sprintf("%04d", $bug->bug_id); ?></td>
					<td width="18%"><?php echo ucfirst($bug->bug_name); ?></td>
					<td width="3%"><?php if ($bug->important_bug == 1) : echo '<span class="icon icon-notification"></span>'; endif; ?></td>
					<td width="3%">&nbsp;</td>
					<td width="18%"><?php echo ucfirst($bug->project_name); ?></td>
					<td width="9%"><?php echo ucfirst($bug->category); ?></td>
					<td width="8%"><?php echo time_left($bug->bug_deadline); ?></td>
					<td class="the-label" data-id="<?php echo $bug->bug_id; ?>" width="10%"><span class="<?php echo str_replace(' ', '-', $bug->label_name); ?>"><?php echo ucfirst($bug->label_name); ?></span></td>
					<td width="14%"><?php echo ucfirst($bug->bug_responsible_name); ?> <?php echo ucfirst($bug->bug_responsible_surname); ?></td>
					<td width="14%"><?php echo ucfirst($bug->bug_creator_name); ?> <?php echo ucfirst($bug->bug_creator_surname); ?></td>

				</tr>

				<?php

			}

			?>

			</tbody>


		</table>

			<?php

		}

	}

	?>