<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['bug-id'])) {

		$id = $_POST['bug-id'];
		$project_id = $_POST['project'];

		if (!empty($id)) {

			$sql = "SELECT * FROM `projects_bugs` WHERE `id` = '" . $id . "'";

			if ($result = $con->query($sql)) {

				$count = $result->num_rows;

				if ($count == 0) {

					echo '<p>This bug does not exists</p>';

				} else {

					while ($bug = $result->fetch_object()) { ?>

					<div class="form-body">

						<div class="half">

							<div class="form-group">

								<h2>General</h2>

								<table class="task-info" width="100%">

									<tr>

										<td class="q">

											Bugname

										</td>

										<td class="a">

											<?php echo ucfirst($bug->name); ?>

										</td>

									</tr>

									<tr>

										<td class="q">

											Reported by

										</td>

										<td class="a">

											<?php

											$sql = "SELECT * FROM `users` WHERE `id` = '" . $bug->created_by . "'";

											if ($result = $con->query($sql)) {

												while ($user = $result->fetch_object()) {

													echo ucfirst($user->name) . ' ' . ucfirst($user->surname);

												}

											}

											?>

										</td>

									</tr>

									<tr>

										<td class="q">

											Module

										</td>

										<td class="a">

											<?php echo ucfirst($bug->category); ?>

										</td>

									</tr>

									<tr>

										<td class="q">

											Location

										</td>

										<td class="a">

											<a href="<?php echo $bug->path; ?>" target="_blank"><?php echo $bug->path; ?></a>

										</td>

									</tr>

									<tr>

										<td class="q">

											Order

										</td>

										<td class="a">

											<input type="number" min="0" name="custom_order" class="custom_order" value="<?php echo $bug->custom_order; ?>">

										</td>

									</tr>

									<tr>

										<td class="q">

											Current status

										</td>

										<td class="a">

											<select name="label6" class="label6" id="label6">

												<?php

													$sql = "SELECT * FROM `labels` WHERE `type` = '2'";

													if ($result = $con->query($sql)) {

														while ($label = $result->fetch_object()) { 

															if ($bug->status == $label->id) {
																$selected = 'selected="selected"';
															} else {
																$selected = '';
															}

														?>

														<option <?php echo $selected; ?> value="<?php echo $label->id; ?>"><?php echo ucfirst($label->name); ?></option>

														<?php

														}

													}

												?>

											</select>

										</td>

									</tr>

									<tr>

										<td class="q">

											Deadline

										</td>

										<td class="a">

											<?php if (($bug->deadline != '0000-00-00 00:00:00') && ($bug->deadline != null)) { echo date("D, d F Y", strtotime($bug->deadline)); } else if (($bug->deadline == '0000-00-00 00:00:00') || ($bug->deadline == null)) { echo '-'; } ?>

										</td>

									</tr>

									<tr>

										<td class="q">

											Redirect to

										</td>

										<td class="a">

											<select name="assignedto" class="assignedto" id="assignedto">

											<?php

												$sql = "SELECT * FROM `users`";

													if ($result = $con->query($sql)) {

														while ($user = $result->fetch_object()) { 

															if ($bug->responsible == $user->id) {
																$selected = 'selected="selected"';
															} else {
																$selected = '';
															}

														?>

														<option <?php echo $selected; ?> value="<?php echo $user->id; ?>"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

														<?php

														}

													}

											?>

											</select>

										</td>

									</tr>

									<tr>

										<td class="q" valign="top">

											Description

										</td>

										<td class="a">

											<?php echo nl2br($bug->description); ?>

										</td>

									</tr>

								</table>

							</div>

						</div>

						<div class="half2">

							<div class="form-group">

								<h2>Comments</h2>

								<div class="post-messages">

								<?php

								$sql = "SELECT * FROM `projects_bugs_comments` WHERE `bug_id` = '" . $bug->id . "' ORDER BY `created_at` DESC";

								if ($result = $con->query($sql)) {

									$count = $result->num_rows;

									if ($count == 0) { ?>

									<p class="notification">No comments have been posted yet</p>

									<?php

									} else {

										while ($comment = $result->fetch_object()) { 

											$sql2 = "SELECT * FROM `users` WHERE `id` = '" . $comment->created_by . "'";

											if ($result2 = $con->query($sql2)) {

												while ($user = $result2->fetch_object()) { ?>

												<div class="actual-message">

													<div class="post-details">

														<p><span class="author-name"><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></span> <span class="post-date"><?php echo date("D d/m/Y", strtotime($comment->created_at)); ?></span></p>

													</div>

													<div class="post-message">

														<p><?php echo nl2br($comment->message); ?></p>

													</div>

												</div>

												<?php

												}

											}

										}

									}

								}

								?>

								</div>

								<div class="write-message">

									<textarea class="add-comment" placeholder="Write a message"></textarea>
									<input type="hidden" class="bug-id" value="<?php echo $bug->id; ?>">
									<input type="hidden" class="project-id" value="<?php echo $project_id; ?>">

									<input disabled type="submit" name="submit-comment" class="submit-comment" value="Post message">

								</div>

							</div>

							<div class="form-group">

								<h2 style="margin-top: 50px;">Files</h2>

								<ol>

								<?php

								$sql = "SELECT * FROM `projects_bugs_files` WHERE `bug_id` = '" . $bug->id . "'";

								if ($result = $con->query($sql)) {

									while ($file = $result->fetch_object()) { ?>

									<li><a href="<?php echo $file->path; ?>" download><?php echo ucfirst($file->name); ?></a></li>

									<?php

									}

								}

								?>

								</ol>

							</div>

						</div>

					</div>

					<?php

					}

				}

			}

		}

	}

?>