<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['project'])) {

		$project_id = $_POST['project'];

		if ($project_id) {

			$sql = "SELECT * FROM `projects` WHERE `id` = '" . $project_id . "'";

			if ($result = $con->query($sql)) {

				while ($project = $result->fetch_object()) {

					if ($project->private == '1') {
						$checked = 'checked="checked"';
					} else {
						$checked = '';
					}
				?>

					<div class="form-body">

					<div class="half">

						<div class="form-group">

							<label for="name2">What's the projectname</label>
							<input autocomplete="off" class="name2" name="name" type="text" id="name2" value="<?php echo $project->name; ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="responsible2">Who's responsible for the project</label>
							<select id="responsible2" class="responsible2" name="responsible">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `users`";

									if ($result = $con->query($sql)) {

										while ($user = $result->fetch_object()) { 

											if ($user->id == $project->responsible) {
												$selected = 'selected="selected"';
											} else {
												$selected = '';
											}

										?>

										<option value="<?php echo $user->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($user->name); ?> <?php echo ucfirst($user->surname); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="datepicker2">Is there a deadline</label>
							<input autocomplete="off" class="deadline3 calendar" name="deadline" type="text" id="datepicker2" value="<?php echo str_replace('30-11--0001', '', date("d-m-Y", strtotime($project->deadline))); ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="label3">This project is</label>
							<select name="label" class="label3" id="label3">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `labels` WHERE `type` = '1'";

									if ($result = $con->query($sql)) {

										while ($label = $result->fetch_object()) { 

											if ($label->id == $project->label) {
												$selected = 'selected="selected"';
											} else {
												$selected = '';
											}

										?>

										<option value="<?php echo $label->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($label->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="customer3">Who's the customer</label>
							<input autocomplete="off" class="customer3" name="customer" type="text" id="customer3" value="<?php echo $project->customer; ?>">

						</div>

					</div>

					<div class="half">

						<div class="form-group">

							<label for="channel2">Through what channel did we get it</label>
							<select name="channel" class="channel2" id="channel2">
								<option value="">-</option>
								<?php

									$sql = "SELECT * FROM `users_groups`";

									if ($result = $con->query($sql)) {

										while ($group = $result->fetch_object()) { 

											if ($group->id == $project->channel) {
												$selected = 'selected="selected"';
											} else {
												$selected = '';
											}

										?>

										<option value="<?php echo $group->id; ?>" <?php echo $selected; ?>><?php echo ucfirst($group->name); ?></option>

										<?php

										}

									}

								?>
							</select>

						</div>

					</div>

					</div>

					<div class="modal-divider"></div>

					<div class="form-body">

						<div class="full">

							<div class="form-group">

								<label for="description2">Maybe describe the project a little</label>
								<textarea name="description" class="description2" id="description2"><?php echo $project->description; ?></textarea>
								<input name="responsible2" class="responsible3" type="hidden" value="<?php echo $project->responsible; ?>">
								<input name="project-id" class="project-id" type="hidden" value="<?php echo $project->id; ?>">

							</div>

						</div>

						<div class="full">

							<div class="form-group">
								
								<label for="private2">Mark this project as private</label>
								<label><input type="checkbox" <?php echo $checked; ?> name="private2" id="private2"> <span class="label">Yes, mark as private</span></label>

							</div>

						</div>

					</div>

				<?php
					
				}

			}

		}

	}

?>