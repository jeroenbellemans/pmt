<?php
	
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['name'])) {

		$project = $_POST['project'];
		$name = htmlentities($_POST['name']);
		$responsible = htmlentities($_POST['responsible']);
		$deadline = date("Y-m-d", strtotime($_POST['deadline']));
		$estimate_hour = htmlentities($_POST['estimated_hour']);
		$estimate_minute = htmlentities($_POST['estimated_minute']);
		$category = htmlentities($_POST['category']);
		$module = htmlentities($_POST['module']);
		$description = mysqli_real_escape_string($con, $_POST['description']);

		if ($_POST['important'] == 'false') {
			$important = '0';
		} else
		if ($_POST['important'] == 'true') {
			$important = '1';
		}

		if ($deadline == '1970-01-01') {
			$deadline = '0000-00-00';
		} else {
			$deadline = $deadline . ' 23:59:59';
		}

		if (empty($name)) {
			echo 'name';
		} else
		if (empty($responsible)) {
			echo 'responsible';
		} else
		if (empty($category)) {
			echo 'category';
		} else {

			$estimated = $estimate_hour . ':' . $estimate_minute;

			$sql = "INSERT INTO `projects_tasks` (`name`, `description`, `category`, `responsible`, `deadline`, `label`, `estimated_time`, `important`, `project_id`, `module_id`, `created_by`, `created_at`) VALUES ('" . $name . "', '" . $description . "', '" . $category . "', '" . $responsible . "', '" . $deadline . "', '5', '" . $estimated . "', '" . $important . "', '" . $project . "', '" . $module . "', '" . $_SESSION['id'] . "', now())";
			$con->query($sql);

			$inserted_task_id = mysqli_insert_id($con);

			$sql2 = "INSERT INTO `projects_tasks_users` (`project_id`, `task_id`, `user_id`) VALUES ('" . $project . "', '" . $inserted_task_id . "', '" . $responsible . "')";
			$con->query($sql2);

			$sql3 = "SELECT * FROM `users` WHERE `id` = '" . $responsible . "'";

			if ($result3 = $con->query($sql3)) {

				while ($user = $result3->fetch_object()) {

					$sql4 = "SELECT * FROM `users` WHERE `id` = '" . $_SESSION['id'] . "'";

					if ($result4 = $con->query($sql4)) {

						while ($assigner = $result4->fetch_object()) {

							$sql5 = "SELECT * FROM `projects` WHERE `id` = '" . $project . "'";

							if ($result5 = $con->query($sql5)) {

								while ($project2 = $result5->fetch_object()) {

									$to = $user->email;
									$from = "info@time2change.com";
									$subject = "#" . $inserted_task_id . " - New task created: " . $name . "";
									
									// Write the contents of your e-mail here using HTML code
									$message = '
									<h1>' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . '</h1>
									<h2>You are now responsible for task #' . $inserted_task_id . '</h2>
									<p>' . ucfirst($assigner->name) . ' ' . ucfirst($assigner->surname) . ' created a new task in <strong>' . ucfirst($project2->name) . '</strong> and assigned you as responsible.</p>
									<p>View all the <a href="http://pmt.time2change.com/index.php?pid=' . $project . '&redirect=true&uid=' . $user->id . '#tasks">tasks</a> in ' . ucfirst($project2->name) . '</p>
									<br><br>
									<p>PMT &copy; Time2Change</p>
									';
												
									$headers = "From: $from\r\n";
									$headers .= "Content-type: text/html\r\n";
									$to = $to;
									
									// Send the actual mail to the users
									mail($to, $subject, $message, $headers);

								}

							}

						}

					}

				}

			}

			echo 'success';
		}

	}

?>