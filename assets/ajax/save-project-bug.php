<?php
	session_start();
	require '../../database/my-connection.php';

	if (isset($_POST['label'])) {

		$label = $_POST['label'];
		$bug_id = $_POST['bug_id'];
		$order = $_POST['order'];
		$responsible = $_POST['responsible'];

		if ($label && $order && $responsible) {

			$sql = "SELECT * FROM `projects_bugs` WHERE `id` = '" . $bug_id . "'";

			if ($result = $con->query($sql)) {

				while ($bug = $result->fetch_object()) {

					$query = "UPDATE `projects_bugs` SET `custom_order` = '" . $order . "', `status` = '" . $label . "', `responsible` = '" . $responsible . "', `updated_by` = '" . $_SESSION['id'] . "', `updated_at` = now() WHERE `id` = '" . $bug_id . "'";
					$con->query($query);

					if ($bug->responsible != $responsible) {

						$sql2 = "INSERT INTO `projects_bugs_log` (`type`, `sender`, `receiver`, `bug_id`, `date`) VALUES ('1', '" . $bug->responsible . "', '" . $responsible . "', '" . $bug_id . "', now())";
						$con->query($sql2);

						$query2 = "UPDATE `projects_bugs` SET `custom_order` = '0' WHERE `id` = '" . $bug_id . "'";
						$con->query($query2);

						$sql3 = "SELECT * FROM `users` WHERE `id` = '" . $bug->responsible . "'";

						if ($result3 = $con->query($sql3)) {

							while ($from2 = $result3->fetch_object()) {

								$sql4 = "SELECT * FROM `users` WHERE `id` = '" . $responsible . "'";

								if ($result4 = $con->query($sql4)) {

									while ($to2 = $result4->fetch_object()) {

										$to = $to2->email;
										$from = "info@time2change.com";
										$subject = "BUG REDIRECT: #" . $bug_id . " " . $bug->name . " ";
										
										// Write the contents of your e-mail here using HTML code
										$message = '
										<h1>' . ucfirst($to2->name) . ' ' . ucfirst($to2->surname) . '</h1>
										<h2>' . ucfirst($from2->name) . ' ' . $from2->surname . ' redirected a bug to you</h2>
										<p>You are now responsible for #' . $bug_id . ' ' . $bug->name . '. You can <a href="pmt.time2change.com/index.php?pid=' . $bug->project_id . '&redirect=true&uid=' . $to2->id . '#bugs">view the bugs here</a>.</p>
										<p>Don\'t forget to mark a bug as finished if finished. If you have done your part, don\'t forget to target another person afterwards...</p>
										<br><br>
										<p>PMT &copy; Time2Change</p>
										';
													
										$headers = "From: $from\r\n";
										$headers .= "Content-type: text/html\r\n";
										$to = $to;
										
										// Send the actual mail to the users
										mail($to, $subject, $message, $headers);

									}

								}

							}

						}

					} 

					if ($label == '6') {

						$sql5 = "SELECT * FROM `users` WHERE `id` = '" . $bug->created_by . "'";

						if ($result5 = $con->query($sql5)) {

							while ($user = $result5->fetch_object()) {

								$to = $user->email;
								$from = "info@time2change.com";
								$subject = "#" . $bug_id . " " . $bug->name . ": Marked as \"For Review\"";
								
								// Write the contents of your e-mail here using HTML code
								$message = '
								<h1>' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . '</h1>
								<h2>#' . $bug_id . ' ' . $bug->name . ' - has been marked as <strong>For review</strong></h2>
								<p>Please review this bug and mark it as completed or as incompleted! You can do this <a href="http://pmt.time2change.com/index.php?pid=' . $bug->project_id . '&redirect=true&uid=' . $user->id . '#bugs">right here</a>.</p>
								<br><br>
								<p>PMT &copy; Time2Change</p>
								';
											
								$headers = "From: $from\r\n";
								$headers .= "Content-type: text/html\r\n";
								$to = $to;
								
								// Send the actual mail to the users
								mail($to, $subject, $message, $headers);

							}

						}

					}

				}

			}

		}

	}

?>