<?php
	
	session_start();
	require '../../../database/my-connection.php';
	require '../../../config/functions/time_left.php';
	require '../../../config/functions/escape.php';

	/*echo $_SESSION['view'];
	echo $_SESSION['project'];
	echo $_SESSION['user'];
	echo $_SESSION['filter'];
	echo $_SESSION['start_date'];
	echo $_SESSION['end_date'];
	echo $_SESSION['orderby'];
	echo $_SESSION['orderdirection'];*/

?>
	<div class="filter-results"></div>

	<!-- FILTER -->

	<div class="fixed-filter">

		<select id="view" name="view">

			<option value="x">Show logs</option>
			<option value="y">Show estimates</option>

		</select>

		<select id="project-filter" name="project">

			<option value="x">For all projects</option>

			<?php

			$sql = "SELECT * FROM `projects`";

			if ($result = $con->query($sql)) {

				while ($project = $result->fetch_object()) {

					if ($project->id == $_SESSION['project']) {

						$selected = 'selected="selected"';

					} else {

						$selected = '';

					}

					echo '<option value="' . $project->id . '" ' . $selected . '>' . ucfirst($project->name) . '</option>';

				}

			}

			?>

		</select>

		<select id="user" name="user">

			<option value="x">For all users</option>

			<?php

			$sql = "SELECT * FROM `users`";

			if ($result = $con->query($sql)) {

				while ($user = $result->fetch_object()) {

					if ($user->id == $_SESSION['user']) {

						$selected = 'selected="selected"';

					} else {

						$selected = '';

					}

					echo '<option value="' . $user->id . '" ' . $selected . '>' . ucfirst($user->name) . ' ' . ucfirst($user->surname) . '</option>';

				}

			}

			?>

		</select>

		<select id="date-filter" name="date" onchange="initPeriod();">

			<option value="x">For any date</option>
			<option value="y">During period</option>

		</select>

		<div class="inline-hidden-period-selector">

			<input name="start_date" class="start_date calendar" placeholder="Starting at">
			<input name="end_date" class="end_date calendar" placeholder="Ending at">

		</div>

		<select id="orderby" name="orderby">

			<option value="project">Order by project</option>
			<option value="task">Order by taskname</option>
			<option value="user">Order by username</option>
			<option value="date">Order by date</option>

		</select>

		<select id="orderdirection" name="orderdirection">

			<option value="asc">Ascending</option>
			<option value="desc">Descending</option>

		</select>

		<button class="filter-this">Apply filter</button>

	</div>