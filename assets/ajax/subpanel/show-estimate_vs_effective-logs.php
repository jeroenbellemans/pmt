<?php
	
	session_start();
	require '../../../database/my-connection.php';
	require '../../../config/functions/time_left.php';
	require '../../../config/functions/escape.php';

	if (isset($_POST['project'])) {

		$project_id = htmlentities($_POST['project']);

		if ($project_id != 0) {
			
			$project = "WHERE projects.id = '" . $project_id . "'";

		} else {

			$project = '';

		}

		$sql = "SELECT
					*,
					COUNT(*) AS total_logs,
					SUM(TIMESTAMPDIFF(SECOND, users_timings_log.start_date, users_timings_log.end_date)) as `time_difference`,
					projects_tasks.created_at AS created_at,
					projects.name AS project_name,
					projects_tasks.name AS task_name,
					projects_tasks.estimated_time AS estimated,
					(TIME_TO_SEC(projects_tasks.estimated_time) - SUM(TIMESTAMPDIFF(SECOND, users_timings_log.start_date, users_timings_log.end_date))) AS total_difference
				FROM `users_timings_log`
					INNER JOIN `projects_tasks` ON projects_tasks.id = users_timings_log.type_id
					LEFT JOIN `projects` ON projects.id = projects_tasks.project_id
				" . $project . "
				GROUP BY 
					users_timings_log.type_id
				";

		if ($con->query($sql)->num_rows == 0) {

			?>

			<div class="full-width">

				<p><?php echo $con->query($sql)->num_rows; ?> results found</p>

			</div>

			<?php

		} else {

			if ($result = $con->query($sql)) {

				?>

				<div class="full-width">

					<table width="100%">

						<thead>

							<th>Date</th>
							<th>Project</th>
							<th>Task</th>
							<th style="text-align: right;">Logs</th>
							<th style="text-align: right;">Estimated</th>
							<th style="text-align: right;">Effective</th>
							<th>Result</th>

						</thead>

						<tbody>

						<?php

						while ($log = $result->fetch_object()) {

							?>

							<tr>

								<td><?php echo date("D, d M Y", strtotime($log->start_date)); ?></td>
								<td><?php echo ucfirst($log->project_name); ?></td>
								<td><?php echo ucfirst($log->task_name); ?></td>
								<td align="right"><?php echo $log->total_logs; ?></td>
								<td align="right"><?php echo $log->estimated; ?></td>
								<td align="right"><?php echo convert_to_time($log->time_difference); ?></td>
								<td><?php echo absolute_value($log->total_difference); ?></td>

							</tr>

							<?php

						}

						?>

						</tbody>

					</table>

				</div>

				<?php

			}

		}

	}

?>