<?php
	
	session_start();
	require '../../../database/my-connection.php';
	require '../../../config/functions/time_left.php';
	require '../../../config/functions/escape.php';

	if (isset($_POST['project'])) {

		$project_id = htmlentities($_POST['project']);

		if ($project_id != 0) {
			
			$project = "WHERE projects.id = '" . $project_id . "'";

		} else {

			$project = '';

		}

		$sql = "SELECT 
					*,
				TIMESTAMPDIFF(SECOND, users_timings_log.start_date, users_timings_log.end_date) as `time_difference`,
					users_timings_log.description AS timings_description,
					users.name AS user_firstname,
					users.surname AS user_lastname,
					projects.name AS project_name,
					projects_tasks.name AS task_name,
					projects_categories.name AS category_name,
					labels.name AS billable
				FROM `users_timings_log`
					INNER JOIN `users` ON users.id = users_timings_log.user_id
					INNER JOIN `projects_tasks` ON projects_tasks.id = users_timings_log.type_id
					LEFT JOIN `projects` ON projects.id = projects_tasks.project_id
					LEFT JOIN `projects_categories` ON projects_categories.id = projects_tasks.category
					LEFT JOIN `labels` ON labels.id = projects_tasks.billable
				" . $project . "
				ORDER BY
					`start_date`
				ASC
				";

		if ($con->query($sql)->num_rows == 0) {

			?>

			<div class="full-width">

				<p><?php echo $con->query($sql)->num_rows; ?> results found</p>

			</div>

			<?php

		} else {

			if ($result = $con->query($sql)) {

				?>

				<div class="full-width">

					<table width="100%">

						<thead>

							<th>Date</th>
							<th>Value</th>
							<th>User</th>
							<th>Description</th>
							<th>Billable</th>
							<th>Project</th>

						</thead>

						<tbody>

						<?php

						while ($log = $result->fetch_object()) {

							?>

							<tr>

								<td><?php echo date("D, d M Y", strtotime($log->start_date)); ?></td>
								<td><?php echo gmdate("H:i", $log->time_difference); ?></td>
								<td><?php echo ucfirst($log->user_firstname); ?> <?php echo ucfirst($log->user_lastname); ?></td>
								<td><span style="color: #232528; font-weight: bold;"><?php echo ucfirst($log->task_name); ?></span>: <?php echo ucfirst($log->timings_description); ?></td>
								<td><?php echo ucfirst($log->billable); ?></td>
								<td><span style="color: #232528; font-weight: bold;"><?php echo ucfirst($log->project_name); ?></span></td>

							</tr>

							<?php

						}

						?>

						</tbody>

					</table>

				</div>

				<?php

			}

		}

	}

?>