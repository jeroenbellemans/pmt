<?php
	
	session_start();
	require '../../../database/my-connection.php';
	require '../../../config/functions/time_left.php';
	require '../../../config/functions/escape.php';

	if (isset($_POST['project'])) {

		$_SESSION['view'] = $con->real_escape_string($_POST['view']);
		$_SESSION['project'] = $con->real_escape_string($_POST['project']);
		$_SESSION['user'] = $con->real_escape_string($_POST['user']);
		$_SESSION['filter'] = $con->real_escape_string($_POST['filter']);
		$_SESSION['start_date'] = $con->real_escape_string($_POST['start_date']);
		$_SESSION['end_date'] = $con->real_escape_string($_POST['end_date']);
		$_SESSION['orderby'] = $con->real_escape_string($_POST['orderby']);
		$_SESSION['orderdirection'] = $con->real_escape_string($_POST['orderdirection']);

		// If period has been selected

		if (!empty($_SESSION['start_date']) && !empty($_SESSION['end_date'])) {

			$start_date = explode("-", $_SESSION['start_date']);
			$end_date = explode("-", $_SESSION['end_date']);

			$start_date = $start_date[2] . '-' . $start_date[1] . '-' . $start_date[0];
			$end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0];

		}

		// Based on selection narrow search on project or all

		if ($_SESSION['project'] == 'x') {
			$project = ''; // empty to search on all projects
		} else 
		if ($_SESSION['project'] > 0) {
			$project = 'WHERE projects.id = "' . $_SESSION['project'] . '"'; // narrow on project.id
		} else {
			$project = '';
		}

		// Based on selection narrow search on users or all

		if ($_SESSION['user'] == 'x') {
			$user = ''; // empty to search on all users
		} else 
		if ($_SESSION['user'] == 'x' && $_SESSION['project'] == 'x') {
			$user  = 'WHERE users.id = "' . $_SESSION['user'] . '"';
		} else
		if ($_SESSION['user'] != 'x' && $_SESSION['project'] != 'x') {
			$user = ' AND users.id = "' . $_SESSION['user'] . '"';
		} else {
			$user = '';
		}

		// Based on selection order by table.column

		if ($_SESSION['orderby'] == 'project') {
			$orderby = 'projects.name';
		} else 
		if ($_SESSION['orderby'] == 'task') {
			$orderby = 'tasks.name';
		} else
		if ($_SESSION['orderby'] == 'user') {
			$orderby = 'users.name';
		} else
		if ($_SESSION['orderby'] == 'date') {
			$orderby = 'users_timings_log.start_date';
		} else {
			$orderby = '';
		}

		// Based on selection - if period is selected set a period

		if ($_SESSION['filter'] == 'y') {
			if ($_SESSION['user'] == 'x' && $_SESSION['project'] == 'x') {
				$period = 'WHERE users_timings_log.start_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
			} else
			if (($_SESSION['filter'] != 'x' || $_SESSION['project'] != 'x') || ($_SESSION['filter'] != 'x' && $_SESSION['project'] != 'x')) {
				$period = ' AND users_timings_log.start_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
			}
		} else {
			$period = '';
		}

		// Based on selected - set group by parameter

		if ($_SESSION['view'] == 'x') {
			$groupby = 'users_timings_log.id';
		} else 
		if ($_SESSION['view'] == 'y') {
			$groupby = 'users_timings_log.type_id';
		}

		$sql = "SELECT 
					*,
					COUNT(*) AS total_logs,
					TIMESTAMPDIFF(SECOND, users_timings_log.start_date, users_timings_log.end_date) as `time_difference`,
					(TIME_TO_SEC(projects_tasks.estimated_time) - SUM(TIMESTAMPDIFF(SECOND, users_timings_log.start_date, users_timings_log.end_date))) AS total_difference,
					users_timings_log.description AS timings_description,
					users_timings_log.start_date AS timings_startdate,
					users.name AS user_firstname,
					users.surname AS user_lastname,
					projects.name AS project_name,
					projects_tasks.name AS task_name,
					projects_tasks.estimated_time AS estimated,
					projects_categories.name AS category_name,
					labels.name AS billable
				FROM `users_timings_log`
					INNER JOIN `users` ON users.id = users_timings_log.user_id
					INNER JOIN `projects_tasks` ON projects_tasks.id = users_timings_log.type_id
					LEFT JOIN `projects` ON projects.id = projects_tasks.project_id
					LEFT JOIN `projects_categories` ON projects_categories.id = projects_tasks.category
					LEFT JOIN `labels` ON labels.id = projects_tasks.billable
				" . $project . "
				" . $user . "
				" . $period . "
				GROUP BY 
					" . $groupby . "
				ORDER BY 
					" . $orderby . "
				" . $_SESSION['orderdirection'] . "
				";

		if ($result = $con->query($sql)) {

			if ($result->num_rows == 0) {

				echo '<p class="no-results-found">For this query no results could be found</p>';

			} else {

				// x = show the logs
				// y = show the estimates vs effectives

				if ($_SESSION['view'] == 'x') { // Show logs

					?>

					<div class="full-width">

						<table width="100%">

							<thead>

								<th>Date</th>
								<th>Value</th>
								<th>User</th>
								<th>Description</th>
								<th>Billable</th>
								<th>Project</th>

							</thead>

							<tbody>

							<?php

							while ($log = $result->fetch_object()) {

								?>

								<tr>

									<td><?php echo date("D, d M Y", strtotime($log->start_date)); ?></td>
									<td><?php echo gmdate("H:i", $log->time_difference); ?></td>
									<td><?php echo ucfirst($log->user_firstname); ?> <?php echo ucfirst($log->user_lastname); ?></td>
									<td><span style="color: #232528; font-weight: bold;"><?php echo ucfirst($log->task_name); ?></span>: <?php echo ucfirst($log->timings_description); ?></td>
									<td><?php echo ucfirst($log->billable); ?></td>
									<td><span style="color: #232528; font-weight: bold;"><?php echo ucfirst($log->project_name); ?></span></td>

								</tr>

								<?php

							}

							?>

							</tbody>

						</table>

					</div>

					<?php

				} else { // Show estimates vs effectives

					?>

					<div class="full-width">

						<table width="100%">

							<thead>

								<th>Date</th>
								<th>Project</th>
								<th>Task</th>
								<th style="text-align: right;">Logs</th>
								<th style="text-align: right;">Estimated</th>
								<th style="text-align: right;">Effective</th>
								<th>Result</th>

							</thead>

							<tbody>

							<?php

							while ($log = $result->fetch_object()) {

								?>

								<tr>

									<td><?php echo date("D, d M Y", strtotime($log->start_date)); ?></td>
									<td><?php echo ucfirst($log->project_name); ?></td>
									<td><?php echo ucfirst($log->task_name); ?></td>
									<td align="right"><?php echo $log->total_logs; ?></td>
									<td align="right"><?php echo $log->estimated; ?></td>
									<td align="right"><?php echo convert_to_time($log->time_difference); ?></td>
									<td><?php echo absolute_value($log->total_difference); ?></td>

								</tr>

								<?php

							}

							?>

							</tbody>

						</table>

					</div>

					<?php

				}

			}

		}

	}

?>