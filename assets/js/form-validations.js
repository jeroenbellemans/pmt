$(document).ready(function() {
	init();
	milestoneComplete();
	milestoneIncomplete();
	if (typeof error === 'undefined') {
		error = false;
	} else
	if (error == '#AddNewUser') {
		$(error).modal("show");
	} else
	if (error == '#EditUser') {
		$(error).modal("show");
	}
	setTimeout(function() {
		$("p.success").slideUp(1000);
	}, 1500);
	setTimeout(function() {
		$(".success-message").slideUp(1000);
	}, 1500);
	setTimeout(function() {
		$(".error-message").slideUp(1000);
	}, 1500);
	$(".icon-bin").click(function() {
		var id = $(this).attr("data-id");
		$.ajax({
			type: "post",
			url: "assets/ajax/delete-group.php",
			data: { 'id': id },
			context: this,
			success: function() {
				$(this).closest(".group").slideUp(500);
			}
		});
	});
	if(location.hash) {
        $('a[href=' + location.hash + ']').tab('show');
    }
    $(".show-tasks").click(function() {
    	$(".all-my-tasks").toggleClass("all-my-tasks-larger");
    	$(".my-calendar").toggleClass("my-calendar-smaller");
    	$(".to-show-tasks").toggleClass("hide");
    	$(".to-hide-tasks").toggleClass("hide");
    });
    $(document.body).on("click", "a[data-toggle]", function(event) {
        location.hash = this.getAttribute("href");
    });
    $(".inline-hidden-period-selector").hide();
	$(".worklog").click(function() {
		$("span.my-date").empty();
		$(".my-logs").empty();
		$(".hidden-date").empty();
		var day = $(this).attr("data-day"),
			month = $(this).attr("data-month"),
			year = $(this).attr("data-year"),
			date1 = day + '/' + month + '/' + year,
			date2 = year + '-' + month + '-' + day;
		$("span.my-date").append(date1);
		$(".hidden-date").val(date2);
		$("#Worklog").modal("show");
		$.ajax({
			type: "post",
			url: "assets/ajax/my-worklogs.php",
			data: { 'date': date2 },
			success: function(data) {
				$(".my-logs").append(data);
				removeAfterAjax();
			}
		});
	});
	$(".new-log-entry").click(function() {
		var starthour = $(".start-hour").val(),
		 	startminute = $(".start-minute").val(),
			endhour = $(".end-hour").val(),
			endminute = $(".end-minute").val(),
			date = $(".hidden-date").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/insert-log-entry.php",
			data: { 'starthour': starthour, 'startminute': startminute, 'endhour': endhour, 'endminute': endminute, 'date': date },
			success: function(data) {
				if (data === 'false') {
					$(".response").append('<p class="error-message">Overlapping hours is impossible</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'true') {
					$(".response").append('<p class="success-message">You worklog has been registered</p>');
					setTimeout(function() {
						window.location = 'index.php?cat=calendar';
					}, 1000);
				} else
				if (data === 'false2') {
					$(".response").append('<p class="error-message">Please enter valid working hours</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				}
			}
		});
	});
	$(".remote-work").click(function() {
		var day = $(this).attr("data-day"),
			month = $(this).attr("data-month"),
			year = $(this).attr("data-year"),
			date2 = year + '-' + month + '-' + day;
		$.ajax({
			type: "post",
			url: "assets/ajax/request-remote-work.php",
			data: { 'date': date2 },
			success: function(data) {
				alert("Your request has been send! Either accepted or declined, you will be notified by e-mail");
				window.location = 'index.php?cat=calendar';
			}
		});
	});
	$(".undo-remote-work").click(function() {
		if (confirm("Are you sure you want to cancel this request?")) {
			var day = $(this).attr("data-day"),
				month = $(this).attr("data-month"),
				year = $(this).attr("data-year"),
				date2 = year + '-' + month + '-' + day;
			$.ajax({
				type: "post",
				url: "assets/ajax/undo-request-remote-work.php",
				data: { 'date': date2 },
				success: function(data) {
					window.location = 'index.php?cat=calendar';
				}
			});
		} else {
			// No actions to be taken
		}
	});
	$(".holliday").click(function() {
		var day = $(this).attr("data-day"),
			month = $(this).attr("data-month"),
			year = $(this).attr("data-year"),
			date2 = year + '-' + month + '-' + day;
		$.ajax({
			type: "post",
			url: "assets/ajax/request-day-off.php",
			data: { 'date': date2 },
			success: function(data) {
				alert("Your request has been send! Either accepted or declined, you will be notified by e-mail");
				window.location = 'index.php?cat=calendar';
			}
		});
	});
	$(".undo-holliday").click(function() {
		if (confirm("Are you sure you want to cancel this request?")) {
			var day = $(this).attr("data-day"),
				month = $(this).attr("data-month"),
				year = $(this).attr("data-year"),
				date2 = year + '-' + month + '-' + day;
			$.ajax({
				type: "post",
				url: "assets/ajax/undo-request-day-off.php",
				data: { 'date': date2 },
				success: function(data) {
					window.location = 'index.php?cat=calendar';
				}
			});
		} else {
			// No actions to be taken
		}
	});
	$(".illness").click(function() {
		var day = $(this).attr("data-day"),
			month = $(this).attr("data-month"),
			year = $(this).attr("data-year"),
			date2 = year + '-' + month + '-' + day;
		$.ajax({
			type: "post",
			url: "assets/ajax/confirm-illness.php",
			data: { 'date': date2 },
			success: function(data) {
				window.location = 'index.php?cat=calendar';
			}
		});
	});
	$(".undo-illness").click(function() {
		if (confirm("Are you sure you want to unmark this?")) {
			var day = $(this).attr("data-day"),
				month = $(this).attr("data-month"),
				year = $(this).attr("data-year"),
				date2 = year + '-' + month + '-' + day;
			$.ajax({
				type: "post",
				url: "assets/ajax/undo-confirm-illness.php",
				data: { 'date': date2 },
				success: function(data) {
					window.location = 'index.php?cat=calendar';
				}
			});
		} else {
			// No actions to be taken
		}
	});
	$(".choose-assignees").click(function() {
		if ($("#assignee input[type='checkbox']:checked").length == 0) {
			$(".response").append('<p class="error-message">Select at least one assignee</p>');
			setTimeout(function() {
				$(".error-message").slideUp(1000);
			}, 1000);
		} else {
			var assignees = new Array(),
				project_id = $(".project-id").val();
			$("#assignee input[type='checkbox']:checked").each(function() {
				assignees.push($(this).val());
			});
			$.ajax({
				type: "post",
				url: "assets/ajax/set-assignees.php",
				data: { 'project_id': project_id, 'assignees': assignees },
				success: function() {
					window.location = 'index.php?cat=projects&pid='+project_id;
				}
			});
		}
	});
	$("label.undo-checkmark").click(function(e) {
		e.preventDefault();
		if (confirm("Are you sure you want to delete this person from this project?")) {
			var assignee = $(this).attr("data-id"),
				project = $(".project-id").val();
			$.ajax({
				type: "post",
				url: "assets/ajax/undo-assignees.php",
				context: this,
				data: { 'project': project, 'assignee': assignee },
				success: function() {
					window.location = 'index.php?cat=projects&pid='+project;
				}
			});
		} else {

		}
	});
	$(".delete-project").click(function() {
		if (confirm("You are about to delete a whole project. Proceed?")) {
			var project = $(this).attr("data-id");
			$.ajax({
				type: "post",
				url: "assets/ajax/delete-project.php",
				data: { 'project': project },
				success: function() {
					window.location = 'index.php?cat=projects';
				}
			});
		} else {
			// No action
		}
	});
	$(".create-task").click(function() {
		var file_data = $('#taskfiles').prop('files')[0];   
		var form_data = new FormData();
		form_data.append('upload', file_data);
		console.log(form_data);
		$.ajax({
			url: 'assets/ajax/upload-task-files.php', 
			dataType: 'text', 
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post'
		});
		var name = $(".name").val(),
			responsible = $(".responsible").val(),
			deadline = $(".deadline").val(),
			category = $(".category").val(),
			estimated_hour = $(".estimated_hour").val(),
			estimated_minute = $(".estimated_minute").val(),
			important = $("#important").is(":checked"),
			description = $(".description").val(),
			module = $(".module").val(),
			project = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/create-new-task.php",
			context: this,
			data: { 'name': name, 'responsible': responsible, 'deadline': deadline, 'category': category, 'estimated_hour': estimated_hour, 'estimated_minute': estimated_minute, 'important': important, 'description': description, 'project': project, 'module': module },
			success: function(data) {
				if (data === 'name') {
					$(".response").append('<p class="error-message">The taskname is a required field</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'responsible') {
					$(".response").append('<p class="error-message">Please select the responsible person</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'category') {
					$(".response").append('<p class="error-message">The category is a required field</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'deadline') {
					$(".response").append('<p class="error-message">Please enter a valid deadline</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'success') {
					$(".response").append('<p class="success-message">The task has been successfully created</p>');
					setTimeout(function() {
						window.location = 'index.php?pid='+project;
					}, 1000);
				} else {
					$(".response").append(data);
				}
			}
		});
	});
	$(".save-edit-task").click(function() {
		var name = $(".name3").val(),
			responsible = $(".responsible2").val(),
			responsible2 = $(".former_resp2").val(),
			deadline = $(".deadline2").val(),
			category = $(".category2").val(),
			estimated_hour = $(".estimated_hour2").val(),
			estimated_minute = $(".estimated_minute2").val(),
			important = $("#important2").is(":checked"),
			description = $(".description2").val(),
			module = $(".module2").val(),
			task = $(".project-task-id2").val(),
			project = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/save-edit-task.php",
			context: this,
			data: { 'name': name, 'responsible': responsible, 'responsible2': responsible2, 'deadline': deadline, 'category': category, 'estimated_hour': estimated_hour, 'estimated_minute': estimated_minute, 'important': important, 'description': description, 'task': task, 'module': module },
			success: function(data) {
				if (data === 'name') {
					$(".response").append('<p class="error-message">The taskname is a required field</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'responsible') {
					$(".response").append('<p class="error-message">Please select the responsible person</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'category') {
					$(".response").append('<p class="error-message">The category is a required field</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'deadline') {
					$(".response").append('<p class="error-message">Please enter a valid deadline</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'success') {
					$(".response").append('<p class="success-message">The task has been successfully edited</p>');
					setTimeout(function() {
						window.location = 'index.php?pid='+project;
					}, 1000);
				} else {
					$(".response").append(data);
				}
			}
		});
	});
	/*$(".module-heading").click(function(e) {
		$(this).next(".task-container").slideToggle();
	});
	$(".module-heading span.icons").click(function(e) {
		e.stopPropagation();
	});*/
	$(".transfer-to").click(function() {
		$("span.task-amount").empty();
		var amount = $("input[type='checkbox'].transfer-task:checked").length,
			project = $(".project-id").val();
			tasks = new Array();
		if (amount > 0) {
			$("input[type='checkbox'].transfer-task:checked").each(function() {
				tasks.push($(this).val());
			});
			$("span.task-amount").append(amount);
			$("#TransferTo").modal("show");
			$(".transfer-task").click(function() {
				var new_module = $(".newmodule").val();
				$.ajax({
					type: "post",
					url: "assets/ajax/transfer-tasks.php",
					data: { 'module': new_module, 'tasks': tasks },
					success: function() {
						$(".response").append('<p class="success-message">The task(s) have been succesfully transferred</p>');
						setTimeout(function() {
							window.location = 'index.php?pid='+project;
						}, 1000);
					}
				});
			});
		} else {
			alert("Select at least 1 task to transfer!");
		}
	});
	$(function() {
		var $tasks = $(".list-tasks"),
			$calendar = $(".calendar-tasks");
		$("li", $tasks).draggable({
			revert: "invalid",
			helper: function (event, ui) {
				return $("<div class='task-image'><span class='icon-pushpin'></span></div>").appendTo("body");
			},
      		cursorAt: { left: 25 },
			zIndex: 9000
		});
		$("li", $calendar).draggable({
			revert: "invalid",
			helper: function (event, ui) {
				return $("<div class='task-image' style='z-index: 10000;'><span class='icon-pushpin'></span></div>").appendTo("body");
			},
      		cursorAt: { left: 25 },
			zIndex: 10000
		});
		$calendar.droppable({
			accept: ".list-tasks > li, .calendar-tasks > li",
			activeClass: "highlight-active",
			hoverClass: "highlight-hover",
			drop: function (event, ui) {
				var dropped = $(this).append(ui.draggable);
				var task = $(ui.draggable).attr("data-task"),
					text = $(ui.draggable).attr("title");
					classname = $(ui.draggable).attr("data-class"),
					user = $(".user-choice").val(),
					year = $(this).attr("data-year"),
					month = $(this).attr("data-month"),
					day = $(this).attr("data-day");
				// $(this).find("li").addClass("has-task");
				$(this).find("li").each(function() {
					if ($(this).hasClass("has-task")) {
						// Do nothing
					} else {
						$(this).append("<div data-task='" + task + "' class='listed-task " + classname + "'>" + text + "</div>");
						$(this).addClass("has-task");
					}
				});
				$.ajax({
					type: "post",
					url: "assets/ajax/schedule-task.php",
					data: { 'task': task, 'user': user, 'year': year, 'month': month, 'day': day },
					success: function(data) {
					}
				});
			}
		});
		$tasks.droppable({
			accept: ".calendar-tasks > li",
			activeClass: "highlight-active2",
			hoverClass: "highlight-hover2",
			drop: function (event, ui) {
				$(this).append(ui.draggable);
				if ($(this).find("li").hasClass("has-task")) {
					$(this).find("li").removeClass("has-task");
				}
				if ($(this).find(".listed-task")) {
					$(this).find(".listed-task").remove();
				}
				var task = ui.draggable.attr("data-task");
				$.ajax({
					type: "post",
					url: "assets/ajax/remove-schedule-task.php",
					data: { 'task': task },
					success: function(data) {

					}
				});
			}
		});
	});
	$("li.tool").tooltip();
	$(function () {
	  $('[data-toggle="popover"]').popover()
	});
	$(".my-schedule").click(function() {
		$(".my-tasks-summary").empty();
		$("#MyScheduleToday").modal("show");
		var day = $(this).attr("data-day"),
			month = $(this).attr("data-month"),
			year = $(this).attr("data-year"),
			user = $(this).attr("data-user");
		$.ajax({
			type: "post",
			url: "assets/ajax/view-daily-schedule.php",
			data: { 'year': year, 'month': month, 'day': day, 'user': user },
			success: function(data) {
				$(".my-tasks-summary").append(data);
			}
		});
	});
	$("li.tool").click(function() {
		$("li.tool").each(function() {
			$(this).removeClass("active");
			$(this).closest(".containing-tool").removeClass("active2");
		});
		$(this).addClass("active");
		$(this).closest(".containing-tool").addClass("active2");
	});
	$("li.containing-tool").click(function() {
		$("li.containing-tool").each(function() {
			$(this).removeClass("active2");
		});
		$(this).addClass("active2");
	});
	$("li.tool2").click(function() {
		$("li.tool2").each(function() {
			$(this).removeClass("active");
			$(this).closest(".containing-tool2").removeClass("active2");
		});
		$(this).addClass("active");
		$(this).closest(".containing-tool2").addClass("active2");
	});
	$("li.containing-tool2").click(function() {
		$("li.containing-tool2").each(function() {
			$(this).removeClass("active2");
		});
		$(this).addClass("active2");
	});
	/*
	if ($(".project-information").hasClass("hide-information")) {
		$(this).find(".section").hide();
		$(this).find(".button span.view-less").hide();
	}
	$(".view-more").click(function() {
		$(".project-information").addClass("show-information");
		$(".project-information").removeClass("hide-information");
		$(".project-references").removeClass("full-references");
		$(this).hide();
		$(".project-information .section").show();
		$(".button span.view-less").show();
	});
	$(".view-less").click(function() {
		$(".project-information").removeClass("show-information");
		$(".project-information").addClass("hide-information");
		$(".project-references").addClass("full-references");
		$(this).hide();
		$(".project-information .section").hide();
		$(".button span.view-more").show();
	});
*/
	$(".delete-task").click(function() {
		if (confirm("Are you sure you want to delete this task?")) {
			var task = $(this).attr("data-id");
			$.ajax({
				type: "post",
				url: "assets/ajax/delete-task.php",
				data: { 'task': task },
				context: this,
				success: function(data) {
					$(this).closest(".holder").remove();
				}
			});
		} else {
			// Do nothing
		}
	});
	$(".delete-bug").click(function() {
		if (confirm("Are you sure you want to delete this bug?")) {
			var task = $(this).attr("data-id");
			$.ajax({
				type: "post",
				url: "assets/ajax/delete-bug.php",
				data: { 'task': task },
				context: this,
				success: function(data) {
					$(this).closest(".holder").remove();
				}
			});
		} else {
			// Do nothing
		}
	});
	$(".edit-task").click(function() { 
		$(".edit-task-body").empty();
		$("#EditTask").modal("show");
		var task = $(this).attr("data-id");
		$.ajax({
			type: "post",
			url: "assets/ajax/edit-task.php",
			data: { 'task': task },
			success: function(data) {
				$(".edit-task-body").append(data);
				initCalendar();
			}
		});
	});
	$(".edit-bug").click(function() { 
		$(".edit-bug-body").empty();
		$("#EditBug").modal("show");
		var bug = $(this).attr("data-id");
		$.ajax({
			type: "post",
			url: "assets/ajax/edit-bug.php",
			data: { 'bug': bug },
			success: function(data) {
				$(".edit-bug-body").append(data);
				$(".bug-id").val(bug);
				initCalendar();
			}
		});
	});
	$("li.filters").click(function() {
		$(".toggle-options").toggle();
	});
	$(".edit-project").click(function() {
		$(".edit-project-body").empty();
		$("#EditProject").modal("show");
		var project = $(this).attr("data-id");
		$.ajax({
			type: "post",
			url: "assets/ajax/edit-project.php",
			data: { 'project': project },
			success: function(data) {
				$(".edit-project-body").append(data);
				initCalendar();
			}
		});
	});
	$(".save-edit-project").click(function() {
		var name = $(".name2").val(),
		 	responsible = $(".responsible2").val(),
		 	responsible2 = $(".responsible3").val(),
			deadline = $(".deadline3").val(),
			label = $(".label3").val(),
			description = $(".description2").val(),
			customer = $(".customer3").val(),
			channel = $(".channel2").val(),
			project = $(".project-id").val(),
			visibility = $("#private2").is(":checked");
		$.ajax({
			type: "post",
			url: "assets/ajax/save-edit-project.php",
			data: { 'name': name, 'responsible': responsible, 'responsible2': responsible2, 'deadline': deadline, 'label': label, 'description': description, 'customer': customer, 'channel': channel, 'project': project, 'private': visibility },
			success: function(data) {
				if (data === 'name') {
					$(".response").append('<p class="error-message">The projectname is a required field</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'customer') {
					$(".response").append('<p class="error-message">The customer is a required field</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'deadline') {
					$(".response").append('<p class="error-message">Please enter a valid deadline</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'responsible') {
					$(".response").append('<p class="error-message">Please select the responsible person</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'label') {
					$(".response").append('<p class="error-message">Please select a label</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'channel') {
					$(".response").append('<p class="error-message">Please select a channel</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'success') {
					$(".response").append('<p class="success-message">The project has successfully been edited</p>');
					setTimeout(function() {
						window.location = 'index.php';
					}, 1000);
				} else {
					$(".response").append(data);
				}
			}
		});
	});
	$("#chooseFile").click(function() {
		$("#uploadfiles").trigger("click");
	});
	$("#uploadfiles").on("change", function() {
		var names = [];
		$(".selected-files").empty();
		for (var i = 0; i < $("#uploadfiles").files.length; ++i) {
			$(".selected-files").append($("#uploadfiles").val().replace(/C:\\fakepath\\/i, ""));
			$().append(',');
		}
	})
	$(".create-folder").click(function() {
		$("#CreateFolder").modal("show");	
	});
	$(".save-new-folder").click(function() {
		var name = $(".foldername").val(),
			project = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/create-new-folder.php",
			data: { 'name': name, 'project': project },
			success: function(data) {
				if (data === 'name') {
					$(".response").append('<p class="error-message">The foldername is a required field</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'success') {
					$(".response").append('<p class="success-message">The folder has been created</p>');
					setTimeout(function() {
						window.location = 'index.php?pid=' + project + '&file=true';
						$("#uploadfiles").modal("hide");
					}, 1000);
				}
			}
		});	
	});
	$(".accept-remote").click(function() {
		var id = $(this).attr("data-id");
		$.ajax({
			type: "post",
			url: "assets/ajax/accept-remote-work.php",
			data: { 'id': id},
			success: function () {
				window.location = 'index.php?cat=requests';
			}
		});
	});
	$("td.calendar-day").click(function() {
		$(".day-date").empty();
		$(".daily-report").empty();
		$("#thisDay").modal("show");
		var date = $(this).attr("data-date"),
			user = $(this).attr("data-user");
		var arr = date.split("-");
		$.ajax({
			type: "post",
			url: "assets/ajax/my-planning.php",
			data: { 'user': user, 'date': date},
			success: function (data) {
				$(".day-date").append(arr[2] + "/" + arr[1] + "/" + arr[0]);
				$(".daily-report").append(data);
				init();
			}
		});
	});
	$("td.calendar-day").click(function(e) {
		if ($(this).is("td")) {
			alert("td");
		} else if ($(this).is("li")) {
			alert("li");
		}
	});
	$("li.open-requests").click(function() {
		$("#MyRequests").modal("show");
	});
	$(".delete-task-log").click(function() {
		var id = $(this).attr("data-id");
		if (confirm("Are you sure you want to delete this log?")) {
			$.ajax({
				type: "post",
				url: "assets/ajax/delete-task-log.php",
				data: { 'id': id },
				context: this,
				success: function() {
					$(this).parents("tr.hover-class").fadeOut();
				}
			});
		} else {
			// Do nothing
		}
	});
	$(".edit-task-log").click(function() {
		var id = $(this).attr("data-id");
		$(".edit-custom-timing").empty();
		$(".task-log-id").val("");
		$("#EditCustomTiming").modal("show");
		$.ajax({
			type: "post",
			url: "assets/ajax/edit-task-log.php",
			data: { 'id': id },
			success: function(data) {
				$(".edit-custom-timing").append(data);
				$(".task-log-id").val(id);
				initCalendar();
			}
		});
	});
	$(".decline-remote").click(function() {
		var id = $(this).attr("data-id");
		$.ajax({
			type: "post",
			url: "assets/ajax/decline-remote-work.php",
			data: { 'id': id},
			success: function () {
				window.location = 'index.php?cat=requests';
			}
		});
	});
	$(".delete-module").click(function() {
		if (confirm("You are about to delete a whole module, including tasks. Are you sure?")) {
			var id = $(this).attr("data-id"),
				project = $(this).attr("data-project");
			$.ajax({
				type: "post",
				url: "assets/ajax/delete-module.php",
				data: { 'id': id, 'project': project },
				success: function () {
					window.location = 'index.php?pid=' + project;
				}
			});
		} else {
			// Do nothing
		}
	});
	$(".edit-module").click(function() {
		$(".edit-module-body").empty();
		$(".module-id").empty();
		$("#EditModule").modal("show");
		var id = $(this).attr("data-id");
		$.ajax({
			type: "post",
			url: "assets/ajax/edit-module.php",
			data: { 'id': id },
			success: function (data) {
				$(".edit-module-body").append(data);
				$(".module-id").val(id);
			}
		});
	});
	$(".save-edit-module").click(function() {
		var id = $(".module-id").val(),
			name = $(".module-name").val(),
			project = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/save-edit-module.php",
			data: { 'id': id, 'name': name },
			success: function (data) {
				if (data === 'name') {
					$(".response").append('<p class="error-message">The module name is a required field</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'success') {
					$(".response").append('<p class="success-message">The module has been successfully created</p>');
					setTimeout(function() {
						window.location = 'index.php?pid='+project;
					}, 1000);
				}
			}
		});
	});
	$(".save-edit-custom-timing").click(function() {
		var first_hour = $(".estimated_hour20").val(),
			first_min = $(".estimated_minute20").val(),
			second_hour = $(".estimated_hour40").val(),
			second_min = $(".estimated_minute40").val(),
			date = $(".when3").val(),
			task = $(".task-log-id").val(),
			pid = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/save-edit-task-log.php",
			data: { 'hour1': first_hour, 'min1': first_min, 'hour2': second_hour, 'min2': second_min, 'date': date, 'id': task },
			success: function(data) {
				if (data === 'negative') {
					$(".response").append('<p class="error-message">Please enter valid hours</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else 
				if (data === 'date') {
					$(".response").append('<p class="error-message">Please select a date</p>');
					setTimeout(function() {
						$(".error-message").slideUp(1000);
					}, 1500);
				} else
				if (data === 'success') {
					$(".response").append('<p class="success-message">Your custom time has been saved</p>');
					setTimeout(function() {
						$(".success-message").slideUp(1000);
					}, 1500);
					setTimeout(function() {
						window.location = 'index.php?pid='+pid+'&redirect=timings';
					}, 1000);
				} else {
					$(".response").append(data);
				}
			}
		});
	});
	$(".custom-time").click(function() {
		var id = $(this).attr("data-id");
		$(".task-id").val(id);
		$("#CustomTiming").modal("show");
		var	project = $(".project-id").val();
		var type = $(this).attr("data-type");
		if (type == '1') {
			$(".project-id").val('');
			var project_id = $(this).attr("data-project");
			$(".project-id").val(project_id);
		}
	});
	$("textarea.autocomplete-pt").bind("input", function() {
		if (!$.trim($("textarea.autocomplete-pt").val())) {
			$(".suggest-box").empty();
		}
	}).trigger("input");
	$(".project-heading").click(function(e) {
		$(this).next(".toggle-this-project").slideToggle();
	});
	$(".intranet").click(function() {
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/intranet/show-intranet-page.php",
			success: function(data) {
				$(".my-listings").append(data);
			},
			complete: function() {
				$("#loading").hide();
			}
		});
	});
	$(".treeview").click(function() {
		var project = $(this).attr("data-project");
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/structure/show-project-structure.php",
			data: { 'project': project },
			success: function(data) {
				$(".my-listings").append(data);
			},
			complete: function() {
				$("#loading").hide();
			}
		});
	});
});
$(window).on('popstate', function() {
    var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
    $('a[href=' + anchor + ']').tab('show');
});
// Separated functions
function init() {
	applyFilter();
	callUser();
	initCalendar();
	openMyTask();
	openTask();
	openBug();
	saveCustomTiming();
	saveBug();
	saveMilestone();
	saveTask();
	showAllTasks();
	showMyTasks();
	showMyCompletedTasks();
	showMyBugs();
	showImportantTasks();
	showToReview();
	showMyLogs();
	showEveryoneLogs();
	showEstimatedLogs();
	showFilterLogs();
	suggestMe();
	treeView();
}
function treeView() {
	$("ul.top-level li.folder span.hover-text").click(function(e) {
		e.preventDefault();
		$(this).next("ul.second-level").toggle();
		return false;
	});
	$("ul.top-level li.folder ul.second-level li.subfolder span.hover-text").click(function(e) {
		e.preventDefault();
		$(this).next("ul.third-level").toggle();
		return false;
	});
}
function milestoneComplete() {
	$("td.milestone-bullet").on("click", "span.incomplete", function() {
		var milestone = $(this).attr("data-id");
		$.ajax({
			context: this,
			type: "post",
			url: "assets/ajax/milestones/milestone-mark_complete.php",
			data: { 'milestone': milestone },
			success: function(data) {
				$(this).addClass("complete");
				$(this).removeClass("incomplete");
			}
		});
	});
}
function milestoneIncomplete() {
	$("td.milestone-bullet").on("click", "span.complete", function() {
		var milestone = $(this).attr("data-id");
		$.ajax({
			context: this,
			type: "post",
			url: "assets/ajax/milestones/milestone-mark_incomplete.php",
			data: { 'milestone': milestone },
			success: function(data) {
				$(this).addClass("incomplete");
				$(this).removeClass("complete");
			}
		});
	});
}
function showMyLogs() {
	$(".show-my-timings").click(function() {
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		var project = $(this).attr("data-project");
		if ((typeof(project) === 'undefined') || (project == '')) {
			project = '0';
		}
		$.ajax({
			type: "post",
			url: "assets/ajax/subpanel/show-my-project-logs.php",
			data: { 'project': project },
			success: function(data) {
				$(".my-listings").append(data);
			},
			complete: function() {
				$("#loading").hide();
			}
		});
	});
}
function showEveryoneLogs() {
	$(".show-all-timings").click(function() {
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		var project = $(this).attr("data-project");
		if ((typeof(project) === 'undefined') || (project == '')) {
			project = '0';
		}
		$.ajax({
			type: "post",
			url: "assets/ajax/subpanel/show-everyone-project-logs.php",
			data: { 'project': project },
			success: function(data) {
				$(".my-listings").append(data);
			},
			complete: function() {
				$("#loading").hide();
			}
		});
	});
}
function showEstimatedLogs() {
	$(".show-timings-vs").click(function() {
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		var project = $(this).attr("data-project");
		if ((typeof(project) === 'undefined') || (project == '')) {
			project = '0';
		}
		$.ajax({
			type: "post",
			url: "assets/ajax/subpanel/show-estimate_vs_effective-logs.php",
			data: { 'project': project },
			success: function(data) {
				$(".my-listings").append(data);
			},
			complete: function() {
				$("#loading").hide();
			}
		});
	});
}
function showFilterLogs() {
	$(".show-filter-timings").click(function() {
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/subpanel/show-filter-timings.php",
			success: function(data) {
				$(".my-listings").append(data);
				initCalendar();
				initPeriod();
				applyFilter();
			},
			complete: function() {
				$("#loading").hide();
			}
		});
	});
}
function applyFilter() {
	$(".filter-this").click(function() {
		$(".filter-results").empty();
		var view = $("#view").val(),
			project = $("#project-filter").val(),
			user = $("#user").val(),
			filter = $("#date-filter").val(),
			start_date = $(".start_date").val(),
			end_date = $(".end_date").val(),
			orderby = $("#orderby").val(),
			orderdirection = $("#orderdirection").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/subpanel/set-session-filters.php",
			data: { 'view': view, 'project': project, 'user': user, 'filter': filter, 'start_date': start_date, 'end_date': end_date, 'orderby': orderby, 'orderdirection': orderdirection },
			success: function(data) {
				$(".filter-results").append(data);
				showFilterLogs();
			},
			complete: function() {
				$("#loading").hide();
			}
		});
	});
}
function removeAfterAjax() {
	$(".remove-log-entry").click(function() {
		var id = $(this).attr("data-id");
		$.ajax({
			type: "post",
			url: "assets/ajax/delete-log-entry.php",
			data: { 'id': id },
			context: this,
			success: function() {
				$(this).closest(".group").slideUp(500);
			}
		});
	});
}
function changeLabelTask() {
	var task = $(".task-id").val(),
		label = $("#change-task-label").val();
	$.ajax({
		type: "post",
		url: "assets/ajax/change-label.php",
		data: { 'task': task, 'label': label },
		success: function() {
			$(".status-message").append('<span class="icon icon-checkmark" style="color: #287619; margin-right: 3px; margin-left: 15px; font-size: 10px;"></span> <span style="color: #287619; font-weight: bold;">Changes saved</span>');
			setTimeout(function() {
				$(".status-message").empty();
			}, 2500);
		}
	});
	$.ajax({
		type: "post",
		url: "assets/ajax/get-label.php",
		data: { 'task': task, 'label': label },
		success: function(data) {
			$("tr.activeRow td.the-label").empty();
			$("tr.activeRow td.the-label").append(data);
			$("span.the-label").empty();
			$("span.the-label").append(data);
		}
	});
}
function changeLabelBug() {
	var task = $(".task-id").val(),
		label = $("#change-task-label").val();
	$.ajax({
		type: "post",
		url: "assets/ajax/change-label-bug.php",
		data: { 'task': task, 'label': label },
		success: function() {
			$(".status-message").append('<span class="icon icon-checkmark" style="color: #287619; margin-right: 3px; margin-left: 15px; font-size: 10px;"></span> <span style="color: #287619; font-weight: bold;">Changes saved</span>');
			setTimeout(function() {
				$(".status-message").empty();
			}, 2500);
		}
	});
	$.ajax({
		type: "post",
		url: "assets/ajax/get-label.php",
		data: { 'task': task, 'label': label },
		success: function(data) {
			$("tr.activeRow td.the-label").empty();
			$("tr.activeRow td.the-label").append(data);
			$("span.the-label").empty();
			$("span.the-label").append(data);
		}
	});
}
function hopProject() {
	var id = $(".hop-project").val();
	if ((typeof(id) === 'undefined') || (id == '')) {
		window.location = 'index.php?cat=work';
	} else {
		window.location = 'index.php?cat=work&pid=' + id;
	}
}
function redirectUser() {
	var id = $(".user-choice").val();
	window.location = 'index.php?cat=calendar&uid='+ id;
}
function redirectUser2() {
	var id = $(".user-choice").val(),
		project = $(".user-choice").attr("data-project");
	window.location = 'index.php?pid='+project+'&mode=full-screen&uid='+ id;
}
function initPeriod() {
	var selection = $("#date-filter").val();
	if (selection == 'x') {
		$(".inline-hidden-period-selector").hide();
	} else {
		$(".inline-hidden-period-selector").show();
	}
}
function openTask() {
	$(".view-task").click(function() {
		if ($(".real-time-clock")[0]) {
			$(".real-time-clock").remove();
		}
		$(".view-task-body").empty();
		$("#ViewTask").modal("show");
		var task_id = $(this).attr("data-id"),
			project = $(".project-id").val();
		if (!project) {
			var project_id = $(this).attr("data-project");
			$(".project-id").val(project_id);
		}
		$.ajax({
			type: "post",
			url: "assets/ajax/view-project-task.php",
			data: { 'task-id': task_id, 'project': project },
			success: function(data) {
				$(".view-task-body").append(data);
				$(".task-id").val(task_id);
				commentTask();
			}
		});
		$.ajax({
			type: "post",
			url: "assets/ajax/init-timer.php",
			data: { 'task_id': task_id, 'project': project },
			success: function(data) {
				$(".timer").append(data);
			}
		});
	});
}
function saveTask() {
	$(".save-task").click(function() {
		var label = $(".view-task-body .label3").val(),
			task_id = $(".task-id").val(),
			project = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/save-project-task.php",
			data: { 'label': label, 'task': task_id },
			success: function(data) {
				$(".response").append('<p class="success-message">Edits have been successfully saved</p>');
				setTimeout(function() {
					window.location = 'index.php?pid='+project;
				}, 1000);
			}
		});
	});
}

function initCalendar() {
    $(".calendar").datepicker({
    	dateFormat: 'dd-mm-yy',
    	firstDay: 1 // Set first day of the week on monday
    });
}

function callUser() {
	$(".suggest-user").click(function() {
		var name = $(this).attr("data-name");
		$("textarea.autocomplete-pt").val($("textarea.autocomplete-pt").val() + name);
		$(".suggest-box").empty();
	});
}

function suggestMe() {
	$("textarea.autocomplete-pt").keypress(function(e) {
		if (e.which == 64) {
			$.ajax({
				type: "post",
				url: "assets/ajax/load-users.php",
				success: function(data) {
					$(".suggest-box").empty();
					$(".suggest-box").append(data);
					callUser();
				}
			});
		} else {
			$(".suggest-box").empty();
		}
	});
}

function commentTask() {
	$(".submit-comment").click(function() {
		var id = $(".task-id").val(),
			comment = $(".add-comment").val(),
			project = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/add-comment.php",
			data: { 'task': id, 'comment': comment, 'project': project },
			success: function (data) {
				$(".post-messages").prepend(data);
				$(".add-comment").val('');
			}
		});
	});
}
function openBug() {
	$(".view-bug").click(function() {
		$(".view-bug-body").empty();
		$("#ViewBug").modal("show");
		var bug_id = $(this).attr("data-id"),
			project = $(".project-id").val();
		if (!project) {
			var project_id = $(this).attr("data-project");
			$(".project-id").val(project_id);
		}
		$.ajax({
			type: "post",
			url: "assets/ajax/view-project-bug.php",
			data: { 'bug-id': bug_id, 'project': project },
			success: function(data) {
				$(".view-bug-body").append(data);
				$(".bug-id").val(bug_id);
				// commentBug();
				markComplete();
				markIncomplete();
			}
		});
	});
}
function saveBug() {
	$(".save-bug").click(function() {
		var label = $(".label6").val(),
			order = $(".custom_order").val(),
			responsible = $(".assignedto").val(),
			bug_id = $(".bug-id").val(),
			project = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/save-project-bug.php",
			data: { 'label': label, 'bug_id': bug_id, 'order': order, 'responsible': responsible },
			success: function(data) {
				$(".response").append(data);
				setTimeout(function() {
					window.location = 'index.php?pid='+project+'&redirect=bugs';
				}, 1000);
			}
		});
	});
}
function showAllTasks() {
	$(".show-all-tasks").click(function() {
		var origin = $(this).attr("data-origin");
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/show-all-tasks.php",
			success: function(data) {
				$(".my-listings").append(data);
				$("tr.no-divider").attr("data-origin", origin);
			},
			complete: function() {
				$("#loading").hide();
				openMyTask();
				milestoneIncomplete();
				milestoneComplete();
			}
		});
	});
}
function showMyTasks() {
	$(".show-my-tasks").click(function() {
		var origin = $(this).attr("data-origin");
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/show-my-tasks.php",
			success: function(data) {
				$(".my-listings").append(data);
				$("tr.no-divider").attr("data-origin", origin);
			},
			complete: function() {
				$("#loading").hide();
				openMyTask();
				milestoneIncomplete();
				milestoneComplete();
			}
		});
	});
}
function showMyCompletedTasks() {
	$(".show-my-completed-tasks").click(function() {
		var origin = $(this).attr("data-origin");
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/show-my-completed-tasks.php",
			success: function(data) {
				$(".my-listings").append(data);
				$("tr.no-divider").attr("data-origin", origin);
			},
			complete: function() {
				$("#loading").hide();
				openMyTask();
				milestoneIncomplete();
				milestoneComplete();
			}
		});
	});
}
function showMyBugs() {
	$(".show-my-bugs").click(function() {
		var origin = $(this).attr("data-origin");
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/show-my-bugs.php",
			success: function(data) {
				$(".my-listings").append(data);
				$("tr.no-divider").attr("data-origin", origin);
			},
			complete: function() {
				$("#loading").hide();
				openMyTask();
				milestoneIncomplete();
				milestoneComplete();
			}
		});
	});
}
function showToReview() {
	$(".show-all-for_reviews").click(function() {
		var origin = $(this).attr("data-origin");
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/show-my-to_reviews.php",
			success: function(data) {
				$(".my-listings").append(data);
				$("tr.no-divider").attr("data-origin", origin);
			},
			complete: function() {
				$("#loading").hide();
				openMyTask();
				milestoneIncomplete();
				milestoneComplete();

			}
		});
	});
}
function showImportantTasks() {
	$(".show-important-tasks").click(function() {
		var origin = $(this).attr("data-origin");
		$(".my-listings").empty();
		$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
		$.ajax({
			type: "post",
			url: "assets/ajax/show-important-tasks.php",
			success: function(data) {
				$(".my-listings").append(data);
				$("tr.no-divider").attr("data-origin", origin);
			},
			complete: function() {
				$("#loading").hide();
				openMyTask();
				milestoneIncomplete();
				milestoneComplete();
			}
		});
	});
}
function openMyTask() {
	$("tr.no-divider, .treeview-task").click(function() {
		var origin = $(this).attr("data-origin");
		$("tr.no-divider").each(function() {
			if ($(this).hasClass("activeRow")) {
				$(this).removeClass("activeRow");
			}
		});
		// $(this).nextUntil("tr.no-divider").not(".view-divider").toggle();
		$("#loading").remove();
		$(this).addClass("activeRow");
		$(".my-listings").empty();
		var type = $(this).attr("data-type"),
			id = $(this).attr("data-id");

		if (type === 'task') {
			$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
			$.ajax({
				type: "post",
				url: "assets/ajax/panel/show-task-details.php",
				data: { 'id': id },
				success: function(data) {
					$(".my-listings").append(data);
					initCalendar();
					saveCustomTiming();
					milestoneIncomplete();
					milestoneComplete();
					markComplete();
					markIncomplete();
					saveMilestone();
					$("span.my-tasks-incomplete, span.my-tasks-complete").attr("data-origin", origin);
				},
				complete: function() {
					$("#loading").hide();
				}
			});
		} else
		if (type === 'bug') {
			$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
			$.ajax({
				type: "post",
				url: "assets/ajax/panel/show-bug-details.php",
				data: { 'id': id },
				success: function(data) {
					$(".my-listings").append(data);
					initCalendar();
					milestoneIncomplete();
					milestoneComplete();
					markComplete();
					markIncomplete();
					$("span.my-tasks-incomplete, span.my-tasks-complete").attr("data-origin", origin);
				},
				complete: function() {
					$("#loading").hide();
				}
			});
		}

	});
}
function saveCustomTiming() {
	$(".save-custom-timing").click(function() {
		var first_hour = $(".estimated_hour2").val(),
			first_min = $(".estimated_minute2").val(),
			second_hour = $(".estimated_hour4").val(),
			second_min = $(".estimated_minute4").val(),
			date = $(".when").val(),
			task = $(".task-id").val(),
			description = $(".description98").val(),
			pid = $(".project-id").val();
		$.ajax({
			type: "post",
			url: "assets/ajax/add-custom-timing.php",
			data: { 'hour1': first_hour, 'min1': first_min, 'hour2': second_hour, 'min2': second_min, 'date': date, 'id': task, 'description': description },
			success: function(data) {
				if (data === 'negative') {
					$(".worklog-message").append('<span style="color: #a70000; font-weight: bold;">Enter valid hours</span>');
					setTimeout(function() {
						$(".worklog-message").empty();
					}, 3500);
				} else 
				if (data === 'date') {
					$(".worklog-message").append('<span style="color: #a70000; font-weight: bold;">Select a date</span>');
					setTimeout(function() {
						$(".worklog-message").empty();
					}, 3500);
				} else
				if (data === 'description') {
					$(".worklog-message").append('<span style="color: #a70000; font-weight: bold;">Description required</span>');
					setTimeout(function() {
						$(".worklog-message").empty();
					}, 3500);
				} else
				if (data === 'success') {
					$(".worklog-message").append('<span class="icon icon-checkmark" style="color: #287619; margin-right: 3px; font-size: 10px;"></span> <span style="color: #287619; font-weight: bold;">Log saved</span>');
					setTimeout(function() {
						$(".worklog-message").empty();
					}, 2500);
				} else {
					$(".response").append(data);
				}
			}
		});
	});
}
function saveMilestone() {
	$(".save-milestone").click(function() {
		var milestone_name = $("#milestone-name").val(),
			deadline = $(".deadline43").val(),
			task = $(".task-id").val();
		// alert(milestone_name);
		$.ajax({
			type: "post",
			url: "assets/ajax/save-milestone.php",
			data: { 'milestone_name': milestone_name, 'deadline': deadline, 'task': task },
			success: function (data) {
				if (data === 'name') {
					$(".milestone-message").append('<span style="color: #a70000; font-weight: bold;">Description required</span>');
					setTimeout(function() {
						$(".milestone-message").empty();
					}, 3500);
				} else 
				if (data === 'deadline') {
					$(".milestone-message").append('<span style="color: #a70000; font-weight: bold;">Select a deadline</span>');
					setTimeout(function() {
						$(".milestone-message").empty();
					}, 3500);
				} else
				if (data === 'deadline2') {
					$(".milestone-message").append('<span style="color: #a70000; font-weight: bold;">Invalid deadline</span>');
					setTimeout(function() {
						$(".milestone-message").empty();
					}, 3500);
				} else
				if (data === 'success') {
					$(".milestone-message").append('<span class="icon icon-checkmark" style="color: #287619; margin-right: 3px; font-size: 10px;"></span> <span style="color: #287619; font-weight: bold;">Milestone created</span>');
					setTimeout(function() {
						$(".milestone-message").empty();
					}, 2500);
					$(".my-listings").empty();
					$.ajax({
						type: "post",
						url: "assets/ajax/show-my-tasks.php",
						success: function(data) {
							$(".my-listings").append(data);
						},
						complete: function() {
							$("#loading").hide();
							openMyTask();
						}
					});
				} else {
					$(".milestone-message").append(data);
				}
			}
		});
	});
}
function markIncomplete() {
	$(".my-tasks-incomplete").click(function() {
		var type = $(this).attr("data-type"),
			id = $(this).attr("data-id")
			origin = $(this).attr("data-origin");

		var url = 'assets/ajax/';
		
		if (origin == 'show-my-tasks') {
			url += 'show-my-tasks.php';
		} else
		if (origin == 'show-my-bugs') {
			url += 'show-my-bugs.php';
		} else
		if (origin == 'show-important-tasks') {
			url += 'show-important-tasks.php';
		} else
		if (origin == 'show-all-tasks') {
			url += 'show-all-tasks.php';
		} else
		if (origin == 'show-all-for_reviews') {
			url += 'show-my-to_reviews.php';
		} else 
		if (origin == 'show-my-completed-tasks') {
			url += 'show-my-completed-tasks.php';
		}

		if (type === 'task') {
			var task = id;
			$.ajax({
				type: "post",
				url: "assets/ajax/mark-incomplete-task.php",
				data: { 'task': task },
				success: function () {
					$(".my-listings").empty();
					$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
					$.ajax({
						type: "post",
						url: url,
						success: function(data) {
							$(".my-listings").append(data);
							$("tr.no-divider").attr("data-origin", origin);
							alert(origin);
							openMyTask();
						},
						complete: function() {
							$("#loading").hide();
						}
					});
				}
			});
		} else
		if (type === 'bug') {
			var bug = id;
			$.ajax({
				type: "post",
				url: "assets/ajax/mark-incomplete-bug.php",
				data: { 'bug': bug },
				success: function () {
					$(".my-listings").empty();
					$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
					$.ajax({
						type: "post",
						url: url,
						success: function(data) {
							$(".my-listings").append(data);
							$("tr.no-divider").attr("data-origin", origin);
							openMyTask();
						},
						complete: function() {
							$("#loading").hide();
						}
					});
				}
			});
		} 
	});
}
function markComplete() {
	$(".my-tasks-complete").click(function() {
		var type = $(this).attr("data-type"),
			id = $(this).attr("data-id"),
			origin = $(this).attr("data-origin");

		var url = 'assets/ajax/';
		
		if (origin == 'show-my-tasks') {
			url += 'show-my-tasks.php';
		} else
		if (origin == 'show-my-bugs') {
			url += 'show-my-bugs.php';
		} else
		if (origin == 'show-important-tasks') {
			url += 'show-important-tasks.php';
		} else
		if (origin == 'show-all-tasks') {
			url += 'show-all-tasks.php';
		} else
		if (origin == 'show-all-for_reviews') {
			url += 'show-my-to_reviews.php';
		} else 
		if (origin == 'show-my-completed-tasks') {
			url += 'show-my-completed-tasks.php';
		}

		if (type === 'task') {
			var task = id;
			$.ajax({
				type: "post",
				url: "assets/ajax/mark-complete-task.php",
				data: { 'task': task },
				success: function () {
					$(".my-listings").empty();
					$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
					$.ajax({
						type: "post",
						url: url,
						success: function(data) {
							$(".my-listings").append(data);
							$("tr.no-divider").attr("data-origin", origin);
							openMyTask();
						},
						complete: function() {
							$("#loading").hide();
						}
					});
				}
			});
		} else
		if (type === 'bug') {
			var bug = id;
			$.ajax({
				type: "post",
				url: "assets/ajax/mark-complete-bug.php",
				data: { 'bug': bug },
				success: function () {
					$(".my-listings").empty();
					$(".my-listings").append('<div id="loading" align="center"><img src="assets/images/ajax-loader.gif"></div>');
					$.ajax({
						type: "post",
						url: url,
						success: function(data) {
							$(".my-listings").append(data);
							$("tr.no-divider").attr("data-origin", origin);
							openMyTask();
						},
						complete: function() {
							$("#loading").hide();
						}
					});
				}
			});
		} 
	});
}