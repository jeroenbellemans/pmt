<?php
	
	// Start Session
	session_start();
	// Set timezont on GMT+1
	date_default_timezone_set('Europe/Amsterdam');
	// Include database connection
	include 'database/my-connection.php';
	// Include the escape-function
	include 'config/functions/escape.php';

	global $con;

	if (isset($_SESSION['id'])) {

		header("location: index.php?cat=work");
		exit();

	} else {

		if (isset($_POST['login'])) {

			$email = escape($_POST['email']);
			$password = md5(escape($_POST['password']));

			$sql = "SELECT * FROM `users` WHERE `email` = '". $email ."' AND `password` = '". $password ."' LIMIT 1";

			if ($res = $con->query($sql)) {

				$count = $res->num_rows;
				$error = '';

				if ($count != 1) {

					$error = '<p class="error">Wrong email or password</p>';

				} else {

					while ($user = $res->fetch_object()) {

						if (($user->privileges == '1') || ($user->privileges == '0')) {
							$_SESSION['admin'] = true;
						} else {
							$_SESSION['admin'] = false;
						}

						$_SESSION['id'] = $user->id;

						$sql2 = "UPDATE `users` SET `status` = '1' WHERE `id` = '" . $user->id . "'";
						$con->query($sql2);

						header("location: index.php?cat=work");
						exit();

					}

				}

			}

		}

	}

?>
<html>

	<head>

		<link rel="stylesheet" href="assets/css/style.css">
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
		<title>Time2Change | Project Management Tool</title>

	</head>

	<body>

		<div class="small-wrap">

			<div class="box-title">

				<h3>Sign In</h3>

			</div>

			<div class="box-content">

				<?php if (isset($error)) : echo $error; endif; ?>

				<form action="" method="post">

				<div class="group">

					<label for="email">Email</label>
					<input name="email" type="email" id="email" autocomplete="off" value="<?php if (isset($email)) : echo $email; endif; ?>">

				</div>

				<div class="group">

					<label for="password">Password</label>
					<input name="password" type="password" id="password" autocomplete="off">

				</div>

				<button name="login" type="submit">Sign in</button>

				</form>

			</div>

			<div class="box-footer">

				<p>Project Management Tool <span>&copy; Time2change</span></p>

			</div>

		</div>

	</body>

</html>