<?php

	function time_left ($string) {

		// Fix for wrong inputted deadlines

		$date = explode(" ", $string); // Explode on space

		if (($date[0] != '0000-00-00') && ($date[1] == '00:00:00')) {
			$date[1] = '23:59:59';
			$string = $date[0] . ' ' . $date[1];
		}

		$current_date = strtotime(date("Y-m-d H:i:s"));
		$previous_date = strtotime($string);
		$error = '-';
		$elapsed = '';

		if ($string === '0000-00-00 00:00:00' || $string === '1970-01-01 00:00:00' || $string === '1970-01-01 23:59:59' || $string === '0000-00-00 23:59:59') {

			return $error;

		} else {

			$seconds = $previous_date - $current_date;

		// Calculations if negative seconds result

			if ($seconds > -30 && $seconds < 0) { $elapsed = '<span class="too-late">' . abs($seconds) . ' secs late</span>'; } else
			if ($seconds <= -30 && $seconds > -60) { $elapsed = '<span class="too-late">' . abs($seconds) . ' secs late</span>'; } else
			if ($seconds <= -60 && $seconds > -3599) { $elapsed = '<span class="too-late">' . abs(number_format(round(($seconds / 60)))) . ' mins late</span>'; } else
			if ($seconds <= -3600 && $seconds > -86399) { $elapsed = '<span class="too-late">' . abs(round(($seconds / 60) / 60)) . ' hours late</span>'; } else
			if ($seconds <= -86400 && $seconds > -2591999) { $elapsed = '<span class="too-late">' . abs(round((($seconds / 60) / 60) / 24)) . ' days late</span>'; } else
			if ($seconds <= -2592000 && $seconds > -31103999) { $elapsed = '<span class="too-late">' . abs(round(((($seconds / 60) / 60) / 24) / 30)) . ' months late</span>'; } else
			if ($seconds <= -31104000) { $elapsed = '<span class="too-late">' . abs(round((((($seconds / 60) / 60) / 24) / 30) / 12)) . ' years late</span>'; } else

		// Calculations if positive seconds result

			if ($seconds < 30) { $elapsed = $seconds . ' secs left'; } else
			if ($seconds >= 30 && $seconds < 60) { $elapsed = $seconds . ' secs left'; } else
			if ($seconds >= 60 && $seconds < 3599) { $elapsed = number_format(round(($seconds / 60))) . ' mins left '; } else
			if ($seconds >= 3600 && $seconds < 86399) { $elapsed = round(($seconds / 60) / 60) . ' hours left '; } else
			if ($seconds >= 86400 && $seconds < 2591999) { $elapsed = round((($seconds / 60) / 60) / 24) . ' days left'; } else
			if ($seconds >= 2592000 && $seconds < 31103999) { $elapsed = round(((($seconds / 60) / 60) / 24) / 30) . ' months left'; } else
			if ($seconds >= 31104000) { $elapsed = round((((($seconds / 60) / 60) / 24) / 30) / 12) . ' years left'; }

		}

		return $elapsed; // return $seconds too display the seconds (positive or negative)

	}

	function define_priority($string) {

	    $current_date = strtotime(date("Y-m-d H:i:s"));
	    $previous_date = strtotime($string);
	    $error = 'p38';
	    $error2 = 'p1';
	    $class = '';

	    if ($string === '0000-00-00 00:00:00') {

	    	return $error;

	    } else

	    if ($string === '1970-01-01 00:00:00') {

	    	return $error2;

	    } else {

		    $seconds = $previous_date - $current_date;
		    if ($seconds <= -1296000) { $class = 'p1';  } else
		    if ($seconds >= -1296001 && $seconds <= -864000 ) { $class = 'p2';  } else
		    if ($seconds >= -864001  && $seconds <= -432000 ) { $class = 'p3';  } else
		    if ($seconds >= -432001  && $seconds <= -259200 ) { $class = 'p5';  } else
		    if ($seconds >= -259201  && $seconds <= -172800 ) { $class = 'p4';  } else
		    if ($seconds >= -172801  && $seconds <= 0       ) { $class = 'p6';  } else
		    if ($seconds >= 1        && $seconds <= 86400   ) { $class = 'p7';  } else
		    if ($seconds >= 86401    && $seconds <= 172800  ) { $class = 'p8';  } else
		    if ($seconds >= 172801   && $seconds <= 259200  ) { $class = 'p9';  } else
		    if ($seconds >= 259201   && $seconds <= 345600  ) { $class = 'p10'; } else
		    if ($seconds >= 345601   && $seconds <= 432000  ) { $class = 'p11'; } else
		    if ($seconds >= 432001   && $seconds <= 518400  ) { $class = 'p12'; } else
		    if ($seconds >= 518401   && $seconds <= 604800  ) { $class = 'p13'; } else
		    if ($seconds >= 604801   && $seconds <= 691200  ) { $class = 'p14'; } else
		    if ($seconds >= 691201   && $seconds <= 777600  ) { $class = 'p15'; } else
		    if ($seconds >= 777601   && $seconds <= 864000  ) { $class = 'p16'; } else
		    if ($seconds >= 864001   && $seconds <= 950400  ) { $class = 'p17'; } else
		    if ($seconds >= 950401   && $seconds <= 1036800 ) { $class = 'p18'; } else
		    if ($seconds >= 1036801  && $seconds <= 1123200 ) { $class = 'p19'; } else
		    if ($seconds >= 1123201  && $seconds <= 1209600 ) { $class = 'p20'; } else
		    if ($seconds >= 1209601  && $seconds <= 1296000 ) { $class = 'p21'; } else
		    if ($seconds >= 1296001  && $seconds <= 1382400 ) { $class = 'p22'; } else
		    if ($seconds >= 1382401  && $seconds <= 1468800 ) { $class = 'p23'; } else
		    if ($seconds >= 1468801  && $seconds <= 1555200 ) { $class = 'p24'; } else
		    if ($seconds >= 1555201  && $seconds <= 1641600 ) { $class = 'p25'; } else
		    if ($seconds >= 1641601  && $seconds <= 1728000 ) { $class = 'p26'; } else
		    if ($seconds >= 1728001  && $seconds <= 2160000 ) { $class = 'p27'; } else
		    if ($seconds >= 2160001  && $seconds <= 2592000 ) { $class = 'p28'; } else
		    if ($seconds >= 2592001  && $seconds <= 3024000 ) { $class = 'p29'; } else
		    if ($seconds >= 3024001  && $seconds <= 3456000 ) { $class = 'p30'; } else
		    if ($seconds >= 3456001  && $seconds <= 3888000 ) { $class = 'p31'; } else
		    if ($seconds >= 3888001  && $seconds <= 4320000 ) { $class = 'p32'; } else
		    if ($seconds >= 4320001  && $seconds <= 6480000 ) { $class = 'p33'; } else
		    if ($seconds >= 6480001  && $seconds <= 8640000 ) { $class = 'p34'; } else
		    if ($seconds >= 8640001  && $seconds <= 10800000) { $class = 'p35'; } else
		    if ($seconds >= 10800001 && $seconds <= 12960000) { $class = 'p36'; } else
		    if ($seconds >= 12960001 && $seconds <= 15120000) { $class = 'p37'; } else
		    if ($seconds >  15120001) { $class = 'p38'; }

	   	}

	  	  return $class; // return $string for actual date => inspect element's class

	}

	function convert_to_time($seconds) {

		$time = round($seconds);

		return sprintf("%02d:%02d", ($time / 3600), ($time / 60%60));

	}

?>