<?php

	function escape($string) {

		return htmlentities($string);

	}

	function absolute_value($string) {

		$value = explode(":", $string);
		$string = round($string);
		$string = sprintf("%02d:%02d", abs($string / 3600), abs($string / 60%60));
		$return = '';

			if ($value[0] < 0) {

				$return .= '<span class="negative-result">- ';

			} else {

				$return .= '<span class="positive-result">+ ';

			}

		$return .= $string;
		$return .= '</span>';

		return $return;

	}

?>