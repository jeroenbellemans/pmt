<?php

	function elapsed_time ($string) {

		$current_date = strtotime(date("Y-m-d H:i:s"));
		$previous_date = strtotime($string);
		$error = '-';
		$elapsed = '';

		if ($string === NULL) {

			return $error;

		} else {

			$seconds = $current_date - $previous_date;

			if ($seconds < 30) { $elapsed = 'About now'; } else
			if ($seconds >= 30 && $seconds < 60) { $elapsed = $seconds . ' seconds ago'; } else
			if ($seconds >= 60 && $seconds < 3599) { $elapsed = number_format(round(($seconds / 60))) . ' minutes ago '; } else
			if ($seconds >= 3600 && $seconds < 86399) { $elapsed = round(($seconds / 60) / 60) . ' hours ago '; } else
			if ($seconds >= 86400 && $seconds < 2591999) { $elapsed = round((($seconds / 60) / 60) / 24) . ' days ago'; } else
			if ($seconds >= 2592000 && $seconds < 31103999) { $elapsed = round(((($seconds / 60) / 60) / 24) / 30) . ' months ago'; } else
			if ($seconds >= 31104000) { $elapsed = round((((($seconds / 60) / 60) / 24) / 30) / 12) . ' years ago'; }

		}

		return $elapsed;

	}

?>