<?php

	// Each refresh a query will be executed to initialize their latest activity
	$sql = "UPDATE `users` SET `last_seen` = now() WHERE `id` = '" . $_SESSION['id'] . "'";
	$con->query($sql);

	$sql2 = "SELECT * FROM `users`";

	if ($result2 = $con->query($sql2)) {

		while ($user = $result2->fetch_object()) {

			if ($user->last_seen !== null) {

				$now = strtotime(date("Y-m-d H:i:s"));
				$last_activity = strtotime($user->last_seen);

				$last_activity = $now - $last_activity;

				if (($last_activity < (10 * 60))) {
					$status = '1';
				} else
				if (($last_activity == (10 * 60)) || ($last_activity < (30 * 60))) {
					$status = '2';
				} else
				if (($last_activity == (30 * 60)) || ($last_activity < (8 * 60 * 60))) {
					$status = '3';
				} else {
					$status = '0';
				}

			} else {
				$status = '0';
			}

			$sql2 = "UPDATE `users` SET `status` = '" . $status . "' WHERE `id` = '" . $user->id . "'";
			$con->query($sql2);

		}

	}

?>