<table width="100%" style="background-color: #efefef; border-collapse: collapse;">

	<td style="padding: 40px;">

		<table width="100%">

			<tr>

				<td width="25%"></td>
				
				<td style="background-color: #fefefe;  border-top-left-radius: 20px; border-top-right-radius: 20px; padding: 20px;" width="50%">

					<table style="font-family: Lucida Grande, Verdana, Arial, Helvetica, sans-serif; font-size: 12px;" width="100%">

						<tr>

							<td width="95%">

								<div style="padding-bottom: 10px;">Hi ' . ucfirst($user->name) . ',</div>
								<div style="font-size: 18px; font-weight: bold; padding-bottom: 20px; text-transform: uppercase;">New bug reported</div>

							</td>

							<td valign="top" width="5%">

								<img style="width: 100%; height: auto; max-height: auto;" src="http://www.time2change.com/pmt/assets/images/logo_t2c.png">

							</td>

						</tr>

						<tr>

							<td colspan="2"><p>' . ucfirst($assigner->name) . ' ' . ucfirst($assigner->surname) . ' reported a bug: <strong>' . ucfirst($name) . '</strong></p></td>

						</tr>

						<tr>

							<td colspan="2">

								<div style="background-color: #f5f5f5; border: 1px dashed #d1d1d1; padding: 20px; font-size: 12px; font-style: italic; color: #303030; margin-top: 20px;">

									' . nl2br($description) . '

								</div>

							</td>

						</tr>

					</table>

				</td>

				<td width="25%"></td>

			</tr>

			<tr>

				<td width="25%"></td>

				<td style="background-color: #1d3f6a; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; padding: 20px;" width="50%">

					<table style="font-family: Lucida Grande, Verdana, Arial, Helvetica, sans-serif; font-size: 12px;" width="100%">

						<tr>

							<td align="left" width="50%">

								<p style="color: #ffffff;">Project Management Tool</p>

							</td>
							
							<td align="right" width="50%">

								<p style="font-size: 11px; color: #ffffff;">&copy; <a style="color: #ffffff; text-decoration: underline;" target="_blank" href="http://www.time2change.com">Time2change</a></p>

							</td>

						</tr>

					</table>

				</td>

				<td width="25%"></td>

			</tr>

		</table>

	</td>

</table>