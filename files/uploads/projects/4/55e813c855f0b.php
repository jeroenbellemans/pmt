<?php

	function base_url() {

		$url = 'localhost/other/master.roaming-souls/';	// http://www.roaming-souls.com
		return $url;

	}

	function dashboard_url() {
		$url = '';  // http://admin.roaming-souls.com
		return $url;
	}

	function format_date($string) {

		$date = strtotime($string);
		$new_date = date("l d F", $date);

		return $new_date;

	}

	function format_time($string) {

		$date = strtotime($string);
		$new_time = date("g:i", $date);

		return $new_time;

	}

	function get_author($string) {
		$sql = mysql_query("SELECT * FROM `userlogin` WHERE id = ". $string ."");
		while ($row = mysql_fetch_array($sql)) {
			$result = $row['username'];
		}

		return $result;
	}

	function days_left($string) {
		$date = strtotime($string);
		$start_date = date('j', $date);
		$now = date('j');

		$days_left = $start_date - $now;

		if ($days_left == 0) {
			$return .= '<span style="color: green;">Today</span>';
		} else
		if ($days_left == 1) {
			$return .= '<span style="color: orange;">Tomorrow</span>';
		} else
		if ($days_left >= 1) {
			$return .= 'In '. $days_left . ' days';
		} 

		return $return;
	}

	function get_slides() {

		$dir = 'assets/content/uploads/slides';

		foreach (glob($dir .'/*.png') as $file) {
			echo '<li><img src="'. $file .'" /></li>';
		}

	}

	function get_news($id) {

		$sql = mysql_query("SELECT * FROM `news` WHERE `id` = ". $id ." ORDER BY `created_at` DESC LIMIT 6");

			while ($row = mysql_fetch_array($sql)) {

				$date = $row['created_at'];
				$category = $row['cat'];
				$author = $row['author'];

					$return .= '<h1>'. $row['title'] .'</h1>';
					$return .= '<img src="../'. $row['image'] .'" alt="'. $row['title'] .'" />';
					$return .= '<p class="author">'. format_date($date) .'&nbsp; -&nbsp; '. get_author($author) .'</p>';
					$return .= '<p class="text">'. nl2br($row['content']) .'</p>';
					
					$array = explode(", ", $category);
					$return .= '<hr class="bottom-news" />';
					$return .= '<ul class="categories">';

					foreach ($array as $cat) {
						$return .= '<li><a href="search.php?cat='. $cat .'">'. ucfirst($cat) .'</a></li>';
					}

					$return .= '</ul>';

			}

		echo $return;
	}

	function get_events($limit) {

		$date = date("Y-m-d h:i:s");

		$sql = mysql_query("SELECT * FROM `events` WHERE `start_date` >= curdate() ORDER BY `start_date` ASC LIMIT $limit");
		$count = mysql_num_rows($sql);


		if ($count == 0) {

			echo '
			<div class="body_fake">
				<p class="link normal"><a href="events/">No upcoming events</a></p>
			</div>
			';

		} else
		if ($count > 0) {

			while ($row = mysql_fetch_array($sql)) {

				$author = $row['host'];
				$date = $row['start_date'];
				$time = $row['start_date'];

				$return .= '<div class="body_fake">';
					$return .= '<div class="event-title">';
						$return .= $row['name'] .'<span class="right smaller defont darker">'. days_left($date).'</span>';
					$return .= '</div>';
					$return .= '<div class="event-toggle">';
						$return .= '<p class="host">'. get_author($author) .'<span class="right">'. $row['location'] .'</span></p>';
						$return .= '<p class="info small">'. format_date($date) .'<br />'. format_time($time) .' <span style="text-transform: uppercase;">'. $row['start_type'] .'</span> <span class="icon-checkmark medium right accept" id="join"></span> </p>';
					$return .= '</div>';
				$return .= '</div>';

			}

			echo $return;

		}

	}

	function new_event() {

		if ($_SESSION['id']) {

			$return .= '<form action="" method="POST">';
				$return .= '<p>Event title</p>';
				$return .= '<input type="text" name="title" />';
				$return .= '<p>Start date</p>';
				$return .= '<select name="start_day">';
					$return .= '<option value="01">1</option>';
					$return .= '<option value="02">2</option>';
					$return .= '<option value="03">3</option>';
					$return .= '<option value="04">4</option>';
					$return .= '<option value="05">5</option>';
					$return .= '<option value="06">6</option>';
					$return .= '<option value="07">7</option>';
					$return .= '<option value="08">8</option>';
					$return .= '<option value="09">9</option>';
					$return .= '<option value="10">10</option>';
					$return .= '<option value="11">11</option>';
					$return .= '<option value="12">12</option>';
					$return .= '<option value="13">13</option>';
					$return .= '<option value="14">14</option>';
					$return .= '<option value="15">15</option>';
					$return .= '<option value="16">16</option>';
					$return .= '<option value="17">17</option>';
					$return .= '<option value="18">18</option>';
					$return .= '<option value="19">19</option>';
					$return .= '<option value="20">20</option>';
					$return .= '<option value="21">21</option>';
					$return .= '<option value="22">22</option>';
					$return .= '<option value="23">23</option>';
					$return .= '<option value="24">24</option>';
					$return .= '<option value="25">25</option>';
					$return .= '<option value="26">26</option>';
					$return .= '<option value="27">27</option>';
					$return .= '<option value="28">28</option>';
					$return .= '<option value="29">29</option>';
					$return .= '<option value="30">30</option>';
					$return .= '<option value="31">31</option>';
				$return .= '</select>';
				$return .= '<select name="start_month">';
					$return .= '<option value="01">January</option>';
					$return .= '<option value="02">February</option>';
					$return .= '<option value="03">March</option>';
					$return .= '<option value="04">April</option>';
					$return .= '<option value="05">May</option>';
					$return .= '<option value="06">June</option>';
					$return .= '<option value="07">July</option>';
					$return .= '<option value="08">August</option>';
					$return .= '<option value="09">September</option>';
					$return .= '<option value="10">October</option>';
					$return .= '<option value="11">November</option>';
					$return .= '<option value="12">December</option>';					
				$return .= '</select>';
				$return .= '<select name="start_year">';
					$return .= '<option value="'. date('Y') .'">'. date('Y') .'</option>';
				$return .= '</select>';
				$return .= '<p>End date</p>';
				$return .= '<select name="end_day">';
					$return .= '<option value="01">1</option>';
					$return .= '<option value="02">2</option>';
					$return .= '<option value="03">3</option>';
					$return .= '<option value="04">4</option>';
					$return .= '<option value="05">5</option>';
					$return .= '<option value="06">6</option>';
					$return .= '<option value="07">7</option>';
					$return .= '<option value="08">8</option>';
					$return .= '<option value="09">9</option>';
					$return .= '<option value="10">10</option>';
					$return .= '<option value="11">11</option>';
					$return .= '<option value="12">12</option>';
					$return .= '<option value="13">13</option>';
					$return .= '<option value="14">14</option>';
					$return .= '<option value="15">15</option>';
					$return .= '<option value="16">16</option>';
					$return .= '<option value="17">17</option>';
					$return .= '<option value="18">18</option>';
					$return .= '<option value="19">19</option>';
					$return .= '<option value="20">20</option>';
					$return .= '<option value="21">21</option>';
					$return .= '<option value="22">22</option>';
					$return .= '<option value="23">23</option>';
					$return .= '<option value="24">24</option>';
					$return .= '<option value="25">25</option>';
					$return .= '<option value="26">26</option>';
					$return .= '<option value="27">27</option>';
					$return .= '<option value="28">28</option>';
					$return .= '<option value="29">29</option>';
					$return .= '<option value="30">30</option>';
					$return .= '<option value="31">31</option>';
				$return .= '</select>';
				$return .= '<select name="end_month">';
					$return .= '<option value="01">January</option>';
					$return .= '<option value="02">February</option>';
					$return .= '<option value="03">March</option>';
					$return .= '<option value="04">April</option>';
					$return .= '<option value="05">May</option>';
					$return .= '<option value="06">June</option>';
					$return .= '<option value="07">July</option>';
					$return .= '<option value="08">August</option>';
					$return .= '<option value="09">September</option>';
					$return .= '<option value="10">October</option>';
					$return .= '<option value="11">November</option>';
					$return .= '<option value="12">December</option>';					
				$return .= '</select>';
				$return .= '<select name="end_year">';
					$return .= '<option value="'. date('Y') .'">'. date('Y') .'</option>';
				$return .= '</select>';
				$return .= '<p>Start hour</p>';
				$return .= '<select name="start_hour">';
					$return .= '<option value="00">00</option>';
					$return .= '<option value="01">01</option>';
					$return .= '<option value="02">02</option>';
					$return .= '<option value="03">03</option>';
					$return .= '<option value="04">04</option>';
					$return .= '<option value="05">05</option>';
					$return .= '<option value="06">06</option>';
					$return .= '<option value="07">07</option>';
					$return .= '<option value="08">08</option>';
					$return .= '<option value="09">09</option>';
					$return .= '<option value="10">10</option>';
					$return .= '<option value="11">11</option>';
				$return .= '</select>';
				$return .= '<select name="start_minute">';
					$return .= '<option value="00">00</option>';
					$return .= '<option value="10">10</option>';
					$return .= '<option value="20">20</option>';
					$return .= '<option value="30">30</option>';
					$return .= '<option value="40">40</option>';
					$return .= '<option value="50">50</option>';
				$return .= '</select>';
				$return .= '<select name="start_type">';
					$return .= '<option value="pm">PM</option>';
					$return .= '<option value="am">AM</option>';
				$return .= '</select>';
				$return .= '<p>End hour</p>';
				$return .= '<select name="end_hour">';
					$return .= '<option value="00">00</option>';
					$return .= '<option value="01">01</option>';
					$return .= '<option value="02">02</option>';
					$return .= '<option value="03">03</option>';
					$return .= '<option value="04">04</option>';
					$return .= '<option value="05">05</option>';
					$return .= '<option value="06">06</option>';
					$return .= '<option value="07">07</option>';
					$return .= '<option value="08">08</option>';
					$return .= '<option value="09">09</option>';
					$return .= '<option value="10">10</option>';
					$return .= '<option value="11">11</option>';
				$return .= '</select>';
				$return .= '<select name="end_minute">';
					$return .= '<option value="00">00</option>';
					$return .= '<option value="10">10</option>';
					$return .= '<option value="20">20</option>';
					$return .= '<option value="30">30</option>';
					$return .= '<option value="40">40</option>';
					$return .= '<option value="50">50</option>';
				$return .= '</select>';
				$return .= '<select name="end_type">';
					$return .= '<option value="pm">PM</option>';
					$return .= '<option value="am">AM</option>';
				$return .= '</select>';
				$return .= '<p>World</p>';
				$return .= '<input class="small-input" type="text" name="world" /><br />';
				$return .= '<input type="submit" name="create_event" value="Create event" />';
			$return .= '</form>';

		} else {

			$return .= '<p class="alert">We require you to <a href="../sign-in">sign in</a> to view this content.</p>';
		
		}

		echo $return;

	}

	function create_event() {

		$title 				= 		$_POST['title'];
		$day1				=		$_POST['start_day'];
		$month1				= 		$_POST['start_month'];
		$year1				=		$_POST['start_year'];
		$day2				=		$_POST['end_day'];
		$month2				=		$_POST['end_month'];
		$year2				=		$_POST['end_year'];
		$hour1				=		$_POST['start_hour'];
		$minute1			=		$_POST['start_minute'];
		$type1				=		$_POST['start_type'];
		$hour2				=		$_POST['end_hour'];
		$minute2			=		$_POST['end_minute'];
		$type2				=		$_POST['end_type'];
		$world				=		$_POST['world'];

		$start_date = date("$year1-$month1-$day1 $hour1:$minute1:00");
		$end_date = date("$year2-$month2-$day2 $hour2:$minute2:00");

		// Checklist

		if (!$title) {
			$return .= '<script>alert("Provide the event title");</script>';
		} else
		if (!$world) {
			$return .= '<script>alert("Provide the world where the event should take place")</script>';
		} else
		if (($start_date < date()) || ($end_date) < date()) {
			$return .= '<script>alert("The event can\'t take place in the history");</script>';
		} else
		if ($start_date > $end_date) {
			$return .= '<script>alert("The event can\'t start after it should be finished");</script>';
		} else {

			// Insert date in database
			$insert = mysql_query("INSERT INTO `events` (`name`, `host`, `location`, `start_date`, `start_type`, `end_date`, `end_type`, `created_at`) VALUES ('". $title ."','". $_SESSION['id'] ."','W". $world ."','". $start_date ."','". $type1 ."','". $end_date ."','". $type2 ."',now())") or die(mysql_error());
			header("Location: ../events/");
		}

		message($return);
	}

	function register_account() {

		$username 			= 		$_POST['username'];
		$email 				= 		$_POST['email'];
		$repeat_email 		= 		$_POST['repeatemail'];
		$password 			= 		$_POST['password'];
		$repeat_password 	= 		$_POST['repeatpassword'];
		$ip_address 		=		$_SERVER['REMOTE_ADDR'];
		$captcha 			= 		$_POST['captcha']; 			// VALUE: AD5f8McBXa

		// Checklist

		$check_email_duplicate = mysql_query("SELECT * FROM `userlogin` WHERE `email` = ". $email ."");
		$count_email = mysql_num_rows($check_email_duplicate);

		$check_username_duplicate = mysql_query("SELECT * FROM `userlogin` WHERE `username` = ". $username ."");
		$count_username = mysql_num_rows($check_username_duplicate);

		if ($count_email > 0) {
			$return .= '<script>alert("That e-mail address is already in use");</script>';
		} else
		if ($count_username > 0) {
			$return .= '<script>alert("That username is already in use");</script>';
		} else
		if (!$username) {
			$return .= '<script>alert("Provide your RuneScape username to continue");</script>';
		} else
		if (!$email) {
			$return .= '<script>alert("Provide your e-mail address to continue");</script>';
		} else
		if (!$repeat_email) {
			$return .= '<script>alert("Confirm your e-mail address to continue");</script>';
		} else
		if ($email != $repeat_email) {
			$return .= '<script>alert("The e-mail addresses do not match");</script>';
		} else 
		if (!$password) {
			$return .= '<script>alert("Choose a password to continue");</script>';
		} else
		if (!$repeat_password) {
			$return .= '<script>alert("Confirm your password to continue");</script>';
		} else
		if ($password != $repeat_password) {
			$return .= '<script>alert("The passwords do not match");</script>';
		} else
		if (!$captcha) {
			$return .= '<script>alert("The captcha key is required");</script>';
		} else
		if ($captcha != 'AD5f8McBXa') {
			$return .= '<script>alert("The captcha key was invalid");</script>';
		} else {

		// Secure password
		$encrypted = md5($password);

		// Generate activation code
		$code = mt_rand();

		// Insert data in database
		$insert = mysql_query("INSERT INTO `userlogin` (`email`, `password`, `username`, `code`, `ip`) VALUES ('". $email ."','". $encrypted ."','". $username ."','". $code ."','". $ip_address ."')");

		// Send mail
		$id = mysql_insert_id();
		$to = $email;
		$from = 'noreply@roaming-souls.com';
		$subject = 'Roaming Souls - Activation Code';

		$message = '
		<html>
			<head>
				<style type="text/css">
					body {
						background-color: #101010;
						font-size: 14px;
						line-height: 21px;
						margin: 0;
						text-align: center;
					}

					h1 {
						color: #daa338;
						font-size: 28px;
					}

					h2 {
						color: #c1c1c1;
						font-size: 18px;
					}

					p {
						color: #c1c1c1;
						font-size: 14px;
						line-height: 21px;
					}

					a:link, a:visited {
						color: #daa338;
						text-decoration: underline;
					}

					a:active, a:hover {
						color: #daa338;
						text-decoration: none;
					}
				</style>
			</head>
			<body>
				<table bgcolor="#101010" width="100%">
					<tr>
						<td align="center">
							<h1>Roaming Souls</h1>
							<h2>Welcome, '. $username .'!</h2><br /><br />
							<p>Thank you for joining our clan!</p>
							<p><u>To complete your registration, please copy this code:</u></p><br /><br /><br /><br />
							<h1><strong>'. $code .'</strong></h1> <br /><br /><br /><br />
							<p>And paste it into the designated box on our website.</p><br /><br /><br/><br />
							<p>Hoping to meet you soon!</p>
							<p><em>The Roaming Souls Staff</em>‏</p>
							<p><a href="http://www.roaming-souls.com">http://www.roaming-souls.com</a></p>
						</td>
					</tr>
				</table>
			</body>
		</html>
		';
		
		$headers = "From: $from\r\n";
		$headers .= "Content-type: text/html\r\n";
		
		// Send the activation email to the registered user
		mail($to, $subject, $message, $headers);

		// Redirect to step 2
		header("Location: ?step=2");
		}

		message($return);

	}

	function message($return) {

		echo $return;

	}

	function activate_account() {

		$email 		=		$_POST['email'];
		$code 		= 		$_POST['code'];

		$check_code_existance = mysql_query("SELECT * FROM `userlogin` WHERE `email` = '". $email ."' AND `code` = '". $code ."'") or die(mysql_error());
		$count_code = mysql_num_rows($check_code_existance);

		if ($count_code > 0) {

			$update = mysql_query("UPDATE `userlogin` SET `activated` = '1' WHERE `email` = '". $email ."' LIMIT 1");

			header("Location: ../sign-in/");
		}

	}

	function do_login() {

		$email 		= 		$_POST['email'];
		$password 	= 		md5($_POST['password']);

		$check_combination = mysql_query("SELECT * FROM `userlogin` WHERE `email` = '". $email ."' AND `password` = '". $password ."' LIMIT 1");
		$count_combination = mysql_num_rows($check_combination);

		if ($count_combination == 1) {

			$get_user_details = mysql_query("SELECT * FROM `userlogin` WHERE `email` = '". $email ."'");
			
			while ($row = mysql_fetch_assoc($get_user_details)) {
				
				$_SESSION['id'] = $row['id'];
				$_SESSION['username'] = $row['username'];

				header("Location: ../index.php");

			}
		}
	}

	function the_excerpt($string) {

		$sql = mysql_query("SELECT * FROM `". $string ."` ORDER BY `created_at` DESC LIMIT 6");

			while ($row = mysql_fetch_array($sql)) {

				$date = $row['created_at'];
				$category = $row['cat'];
				$author = $row['author'];

				$text = implode(' ', array_slice(explode(' ', $row['content']), 0, 60));



				$return .= '<div class="fake">';
					$return .= '<div class="top">';
						$return .= '<h1><a href="news/article.php?id='. $row['id'] .'">'. $row['title'] .'</a></h1>';
						$return .= '<p class="author">'. format_date($date) .'&nbsp; -&nbsp; '. get_author($author) .'</p>';
					$return .= '</div>';
					$return .= '<div class="main">';
						$return .= '<img src="'. $row['image'] .'" />';
						$return .= '<p class="text">'. nl2br($text) .'...</p>';
						$return .= '<p class="read-more"><a href="news/article.php?id='. $row['id'] .'">Full story &raquo;</a></p>';
						
						$array = explode(", ", $category);

					$return .= '</div>';
				$return .= '</div>';
			}

		echo $return;

	}

	function check_level() {

		$sql = mysql_query("SELECT * FROM `userlogin` WHERE `id` = '". $_SESSION['id'] ."' AND `admin` = '1'");
		$count = mysql_num_rows($sql);

		if ($count == 1) {
			$return .= '<li class="utility-box">';
				$return .= '<span class="icon-tree very-large"></span><br />';
				$return .= '<h2><a href="#">Dashboard</a></h2>';		// Link nog aanpassen ../admin
			$return .= '</li>';
		}

		echo $return;

	}

	function get_public_memberlist() {

		$sql = mysql_query("SELECT * FROM `userlogin` ORDER by `rank` DESC");

		while ($row = mysql_fetch_array($sql)) {

			$return .= '<div class="body_fake">';
				$return .= '<div class="event-title">';
					$return .= '<p class="link">'. $row['username'] .'<span class="right smaller defont darker">'. get_rank($row['id']).'</span></p>';
				$return .= '</div>';
				$return .= '<div class="event-toggle">';
					$return .= '<p><img class="exact" src="../assets/images/combat.png"><span class="detail">'. get_combat_level($row['id']) .'</span></p>';
					$return .= '<p><img class="exact" src="../assets/images/total.png"><span class="detail">'. get_total_level($row['id']) .'</span></p>';
				$return .= '</div>';
			$return .= '</div>';

		}

		echo $return;

	}

	function get_rank($id) {

		$sql = mysql_query("SELECT `rank` FROM `userlogin` WHERE id = ". $id ."");

		while ($row = mysql_fetch_array($sql)) {

			if ($row['rank'] == '0') {
				$return .= 'Recruit';
			} else
			if ($row['rank'] == '1') {
				$return .= 'Corporal';
			} else
			if ($row['rank'] == '2') {
				$return .= 'Sergeant';
			} else
			if ($row['rank'] == '3') {
				$return .= 'Lieutenant';
			} else
			if ($row['rank'] == '4') {
				$return .= 'Captain';
			} else
			if ($row['rank'] == '5') {
				$return .= 'General';
			} else
			if ($row['rank'] == '6') {
				$return .= 'Admin';
			} else
			if ($row['rank'] == '7') {
				$return .= 'Deputy Owner';
			} else
			if ($row['rank'] == '8') {
				$return .= 'Owner';
			}

		}

		return $return;

	}

	function get_total_level($id) {

		$sql = mysql_query("SELECT * FROM `userlogin` WHERE `id` = '". $id ."'");

		while ($row = mysql_fetch_array($sql)) {

			$username = $row['username'];

			$order = array(

				'Overall'

			);

			$get = file_get_contents("http://hiscore.runescape.com/index_lite.ws?player=$username");
			$get = explode("\n", $get);
			$i = 0;

			foreach ($order as $key => $value)
			{
					$value = strtolower($value);
					$temp = explode(",", $get[$i]);
					$temp = array("rank" => $temp[0], "level" => $temp[1], "exp" => $temp[2]);
					$stats[$value] = $temp;
					$eval = "\$value = array(\$temp[\"rank\"], \$temp[\"level\"], \$temp[\"exp\"]);";
					eval($eval);
					$i++;
					
			}

			$return .= $stats["overall"]["level"];

		}

		return $return;

	}

	function get_combat_level($id) {

		$sql = mysql_query("SELECT * FROM `userlogin` WHERE `id` = '". $id ."'");

		while ($row = mysql_fetch_array($sql)) {

			$username = $row['username'];

			
			$order = array(

				"Overall",
				"Attack",
				"Defence",
				"Strength",
				"Hitpoints",
				"Ranged",
				"Prayer",
				"Magic",
				"Cooking",
				"Woodcutting",
				"Fletching",
				"Fishing",
				"Firemaking",
				"Crafting",
				"Smithing",
				"Mining",
				"Herblore",
				"Agility",
				"Thieving",
				"Slayer",
				"Farming",
				"Runecraft",
				"Hunter",
				"Construction",
				"Summoning",
				"Dungeoneering"

			);

			$get = file_get_contents("http://hiscore.runescape.com/index_lite.ws?player=$username");
			$get = explode("\n", $get);
			$i = 0;

			foreach ($order as $key => $value)
			{
				$value = strtolower($value);
				$temp = explode(",", $get[$i]);
				$temp = array("rank" => $temp[0], "level" => $temp[1], "exp" => $temp[2]);
				$stats[$value] = $temp;
				$eval = "\$value = array(\$temp[\"rank\"], \$temp[\"level\"], \$temp[\"exp\"]);";
				eval($eval);
				$i++;
					
			}

			$attack = $stats["attack"]["level"];
			$strength = $stats["strength"]["level"];
			$defence = $stats["defence"]["level"];
			$ranged = $stats["ranged"]["level"];
			$magic = $stats["magic"]["level"];
			$prayer = $stats["prayer"]["level"];
			$hitpoints = $stats["hitpoints"]["level"];
			$summoning = $stats["summoning"]["level"];

			$base = ($defence + $hitpoints + ($prayer / 2) + ($summoning / 2));

			$warrior = (1.3 * ($attack + $strength));
			$ranger = (1.3 * (2 * $ranged));
			$mager = (1.3 * (2 * $magic));

			$type = max(array(

				$warrior,
				$ranger,
				$mager

			));

			$combat = (($base + $type) / 4);

		}

		return floor($combat);

	}