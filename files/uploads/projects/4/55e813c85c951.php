<?php 
	session_start();

	include 'assets/includes/functions.php';
	include 'assets/includes/connection.php';
?>
<!DOCTYPE html>
<html lang="en-US">
<html>
	<head>
		<meta charset="UTF-8" />
		<meta name="robots" content="index, follow">
		<meta name="description" content="The official website of the Roaming Souls clan" />
		<meta name="keywords" content="roaming, souls, runescape, clan, official, website, 120+, pvm, events, fun, community, join, rs, gmt" />
		<link href="assets/css/default.css" rel="stylesheet" />
		<link href="assets/css/icofont.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Eagle+Lake' rel='stylesheet' type='text/css'>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Roaming Souls</title>
	</head>
	<body>
		<div id="top">
			<div class="wrap">
				<div id="menu">
					<ul>
						<li class="active"><a href="index.php"><span class="icon-home2 large"></span><br />Home</a></li>
						<li><a href="clan"><span class="icon-info large"></span><br />Clan</a></li>
						<li><a href="events"><span class="icon-calendar large"></span><br />Events</a></li>
						<li><a href="forums"><span class="icon-bubbles large"></span><br />Forums</a></li>
						<li><a href="media"><span class="icon-camera large"></span><br />Media</a></li>
						<li><a href="members"><span class="icon-users large"></span><br />Members</a></li>
					</ul>
				</div>
				<div id="usernav">
					<ul>
					<?php
					if (isset($_SESSION['id'])) {

						echo '<li><a href="profile/"><span class="icon-user large"></span><br />Profile</a></li>';
						echo '<li><a href="sign-out/"><span class="icon-exit large"></span><br />Logout</a></li>';

					} else
					if (!isset($_SESSION['id'])) {

						echo '<li><a href="sign-in/"><span class="icon-enter large"></span><br />Sign In</a></li>';
						echo '<li><a href="sign-up/?step=1"><span class="icon-star3 large"></span><br />Sign Up</a></li>';

					}
					?>
					</ul>
				</div>
				<div class="only-mobile">	
					<div id="logo">
						<img src="assets/images/logo.png" />
					</div>
					<select class="mobile-nav" onchange="javascript:location.href = this.value;">
						<option value="">-- Home --</option>
						<option value="clan/">Clan</option>
						<option value="events/">Events</option>
						<option value="forums/">Forums</option>
						<option value="media">Media</option>
						<option value="members">Members</option>
					</select>
				</div>
			</div>
		</div>
		<div class="banner">
			<ul class="rslides">
				<?php get_slides(); ?>
			</ul>
		</div>
		<div class="wrap">
			<div id="content">
				<ul>
					<li>
						<div class="holder">
							<div class="top">
								<img src="assets/content/uploads/featured-images/box-3.png" />
							</div>
							<div class="bottom">
								<p><a href="clan/#requirements">Clan requirements</a></p>
							</div>
						</div>
					</li>
					<li>
						<div class="holder">
							<div class="top">
								<img src="assets/content/uploads/featured-images/box-3.png" />
							</div>
							<div class="bottom">
								<p><a href="clan/#rules">Our Rules</a></p>
							</div>
						</div>
					</li>
					<li>
						<div class="holder">
							<div class="top">
								<img src="assets/content/uploads/featured-images/box-3.png" />
							</div>
							<div class="bottom">
								<p><a href="sign-up">Join us now!</a></p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="wrap" style="clear: both; position: relative;">
			<div id="holder">
				<div id="main-content">
					<div class="body">
						<?php the_excerpt(news); ?>
					</div>
				</div>
				<div id="side-bar">
					<?php get_events(3); ?>
				</div>
			</div>
		</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="assets/js/functionalities.js"></script>
	<script src="assets/js/responsiveslides.min.js"></script>
	<script>
		$(function() {
			$(".rslides").responsiveSlides();
		});
	</script>
	<script>
		$(".rslides").responsiveSlides({
			auto: true,             // Boolean: Animate automatically, true or false
			speed: 1000,             // Integer: Speed of the transition, in milliseconds
			timeout: 25000,          // Integer: Time between slide transitions, in milliseconds
			pager: false,           // Boolean: Show pager, true or false
			nav: true,             // Boolean: Show navigation, true or false
			random: false,          // Boolean: Randomize the order of the slides, true or false
			pause: true,           // Boolean: Pause on hover, true or false
			pauseControls: true,    // Boolean: Pause when hovering controls, true or false
			prevText: "<span class='icon-arrow-left slide_navigation_left'></span>",   // String: Text for the "previous" button
			nextText: "<span class='icon-arrow-right slide_navigation_right'></span>",       // String: Text for the "next" button
			maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
			navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
			manualControls: "",     // Selector: Declare custom pager navigation
			namespace: "rslides",   // String: Change the default namespace used
			before: function(){},   // Function: Before callback
			after: function(){}     // Function: After callback
		});
	</script>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-57443294-1', 'auto');
	ga('send', 'pageview');

	</script>
	</body>
</html>