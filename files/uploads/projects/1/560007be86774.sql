-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Gegenereerd op: 26 aug 2015 om 12:51
-- Serverversie: 5.6.17
-- PHP-versie: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `t2c_workworkspace`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `calendar_holliday`
--

CREATE TABLE IF NOT EXISTS `calendar_holliday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Gegevens worden geëxporteerd voor tabel `calendar_holliday`
--

INSERT INTO `calendar_holliday` (`id`, `date`, `status`, `user_id`) VALUES
(3, '2015-08-13 00:00:00', '0', 1),
(4, '2015-08-14 00:00:00', '0', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `calendar_remotework`
--

CREATE TABLE IF NOT EXISTS `calendar_remotework` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `status` enum('0','1','2') NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Gegevens worden geëxporteerd voor tabel `calendar_remotework`
--

INSERT INTO `calendar_remotework` (`id`, `date`, `status`, `user_id`) VALUES
(6, '2015-08-06 00:00:00', '1', 1),
(7, '2015-08-04 00:00:00', '0', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `calendar_sickness`
--

CREATE TABLE IF NOT EXISTS `calendar_sickness` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `status` enum('0','1') NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Gegevens worden geëxporteerd voor tabel `calendar_sickness`
--

INSERT INTO `calendar_sickness` (`id`, `date`, `status`, `user_id`) VALUES
(2, '2015-08-04 00:00:00', '0', 1),
(3, '2015-08-05 00:00:00', '0', 1),
(5, '2015-08-10 00:00:00', '0', 5),
(6, '2015-08-11 00:00:00', '0', 5);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `calendar_worklog`
--

CREATE TABLE IF NOT EXISTS `calendar_worklog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `started_at` datetime NOT NULL,
  `ended_at` datetime NOT NULL,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Gegevens worden geëxporteerd voor tabel `calendar_worklog`
--

INSERT INTO `calendar_worklog` (`id`, `started_at`, `ended_at`, `date`, `user_id`) VALUES
(1, '2015-08-10 09:00:00', '2015-08-10 16:00:00', '2015-08-10 00:00:00', 1),
(2, '2015-08-11 09:00:00', '2015-08-11 17:30:00', '2015-08-11 00:00:00', 1),
(3, '2015-08-11 08:00:00', '2015-08-11 08:50:00', '2015-08-11 00:00:00', 1),
(4, '2015-08-10 08:00:00', '2015-08-10 09:00:00', '2015-08-10 00:00:00', 1),
(5, '2015-08-10 17:00:00', '2015-08-10 18:45:00', '2015-08-10 00:00:00', 1),
(6, '2015-08-13 10:30:00', '2015-08-13 10:45:00', '2015-08-13 00:00:00', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `labels`
--

CREATE TABLE IF NOT EXISTS `labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Gegevens worden geëxporteerd voor tabel `labels`
--

INSERT INTO `labels` (`id`, `name`, `type`) VALUES
(1, 'In progress', 1),
(2, 'On hold', 1),
(3, 'Finished', 1),
(4, 'Canceled', 1),
(5, 'Assigned', 2),
(6, 'For review', 2),
(7, 'Unclear', 2),
(8, 'On hold', 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `responsible` int(11) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `label` int(11) NOT NULL,
  `deadline` datetime DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Gegevens worden geëxporteerd voor tabel `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `responsible`, `customer`, `label`, `deadline`, `channel`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(2, 'Group-f Hoogtewerkers', 'Nieuwe website voor group-f hoogtewerkers', 4, 'Group-f', 1, '2016-10-16 00:00:00', '2', 1, '2015-08-12 14:43:44', NULL, NULL),
(3, 'Time2Change Website', 'A total remake of the website. Smoother design, better functionalities. Must be ready for the launch of the article in ''De Kn', 1, 'Time2Change', 2, '2016-11-30 00:00:00', '1', 1, '2015-08-12 15:18:26', NULL, NULL),
(17, 'DC Mechanics', 'Lorem ipsum dolor sit amed con sesetetuor ip motonakal', 2, 'DC Mechanics', 2, NULL, '1', 1, '2015-08-12 18:01:52', NULL, NULL),
(18, 'test', 'just another thestdfhsqdkjflhjskdhfqksjdhlkqdshjfhsldfqsdfsd hflsdh sf hsqldfhslfj hsldf hsdlfkhsd lkfhsdlkfjhsdklfh ssldjfhsdlkf sdjkfh sldfh', 1, 't2c', 1, '2015-08-28 00:00:00', '1', 1, '2015-08-17 14:47:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `projects_categories`
--

CREATE TABLE IF NOT EXISTS `projects_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Gegevens worden geëxporteerd voor tabel `projects_categories`
--

INSERT INTO `projects_categories` (`id`, `name`, `description`) VALUES
(1, 'Administration', NULL),
(2, 'Analyse', NULL),
(3, 'Bugfixing', NULL),
(4, 'Development', NULL),
(5, 'Marketing', NULL),
(6, 'Planning', NULL),
(7, 'Sales', NULL),
(8, 'Testing', NULL),
(9, 'Training', NULL),
(10, 'Video', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `projects_modules`
--

CREATE TABLE IF NOT EXISTS `projects_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Gegevens worden geëxporteerd voor tabel `projects_modules`
--

INSERT INTO `projects_modules` (`id`, `name`, `project_id`) VALUES
(2, 'test', 2),
(3, 'blabla', 2),
(4, 'ljhfdslfq', 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `projects_tasks`
--

CREATE TABLE IF NOT EXISTS `projects_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `category` int(11) NOT NULL,
  `responsible` int(11) NOT NULL,
  `deadline` datetime DEFAULT NULL,
  `label` int(11) NOT NULL,
  `estimated_time` varchar(5) NOT NULL,
  `custom_sort` int(11) DEFAULT NULL,
  `important` enum('0','1') NOT NULL,
  `completed` enum('0','1') NOT NULL,
  `planned` enum('0','1') NOT NULL,
  `project_id` int(11) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Gegevens worden geëxporteerd voor tabel `projects_tasks`
--

INSERT INTO `projects_tasks` (`id`, `name`, `description`, `category`, `responsible`, `deadline`, `label`, `estimated_time`, `custom_sort`, `important`, `completed`, `planned`, `project_id`, `module_id`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'test', 'test', 8, 1, '0000-00-00 00:00:00', 5, '', NULL, '1', '0', '1', 2, 4, 1, '2015-08-18 11:37:04', NULL, NULL),
(2, 'test', 'test', 8, 1, '2015-08-21 00:00:00', 5, '', NULL, '1', '0', '1', 2, 4, 1, '2015-08-18 12:01:48', NULL, NULL),
(3, 'another test', 'jersjngfslk fjsqlfdj sdlfjhskld fsdklfh slkfhsl kfsdfh sdfjhsl dfjshd fsjfh lsfhsdklfh sdkfh lsfsdkl fskd fsdqlf hsdfskl fs', 3, 1, '0000-00-00 00:00:00', 7, '', NULL, '1', '0', '1', 2, 4, 1, '2015-08-18 14:12:36', NULL, NULL),
(4, 'Footer background', 'De backgroundcolor van de footer bij space is wit momenteel, maar bepaalde afbeeldingen worden hierdoor onduidelijk weergegeven. Kan je deze veranderen naar ofwel grijs, zwart, donkerblauw\n\nthnx :)', 3, 5, '2015-09-04 00:00:00', 5, '', NULL, '1', '0', '0', 2, 2, 1, '2015-08-19 13:24:35', NULL, NULL),
(5, 'Estimated task', 'Gewoon een andere description dan de andere', 4, 1, '2015-11-20 00:00:00', 8, '04:00', NULL, '1', '0', '1', 2, 3, 1, '2015-08-19 15:16:08', NULL, NULL),
(6, 'Een andere task met estimate', '', 4, 1, '2015-09-04 00:00:00', 5, '03:30', NULL, '1', '0', '1', 2, 3, 1, '2015-08-26 10:48:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `projects_tasks_schedule`
--

CREATE TABLE IF NOT EXISTS `projects_tasks_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Gegevens worden geëxporteerd voor tabel `projects_tasks_schedule`
--

INSERT INTO `projects_tasks_schedule` (`id`, `task_id`, `user_id`, `date`) VALUES
(69, 1, 1, '2015-08-28'),
(75, 3, 1, '2015-08-29'),
(77, 5, 1, '2015-08-27'),
(78, 2, 1, '2015-08-27'),
(79, 6, 1, '2015-08-27');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `projects_tasks_users`
--

CREATE TABLE IF NOT EXISTS `projects_tasks_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Gegevens worden geëxporteerd voor tabel `projects_tasks_users`
--

INSERT INTO `projects_tasks_users` (`id`, `project_id`, `task_id`, `user_id`) VALUES
(1, 2, 1, 1),
(2, 2, 2, 1),
(3, 2, 3, 1),
(4, 2, 4, 5),
(5, 2, 0, 1),
(6, 2, 0, 1),
(7, 2, 5, 1),
(8, 2, 6, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `projects_users`
--

CREATE TABLE IF NOT EXISTS `projects_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Gegevens worden geëxporteerd voor tabel `projects_users`
--

INSERT INTO `projects_users` (`id`, `project_id`, `user_id`) VALUES
(2, 18, 4),
(3, 18, 5),
(5, 18, 3),
(29, 2, 3),
(32, 2, 1),
(33, 2, 4),
(35, 18, 1),
(36, 18, 2),
(37, 19, 1),
(38, 2, 5),
(39, 2, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(80) NOT NULL,
  `surname` varchar(80) NOT NULL,
  `branch` int(11) NOT NULL,
  `privileges` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `ban` int(11) NOT NULL,
  `last_seen` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `surname`, `branch`, `privileges`, `status`, `ban`, `last_seen`, `created_at`, `updated_at`) VALUES
(1, 'jeroenbellemans@time2change.com', '5b5f04139ec6ffbdbd0b3074681fe70a', 'Jeroen', 'Bellemans', 1, 0, 1, 0, '2015-08-26 12:47:28', '2015-02-05 10:21:37', '2015-05-07 09:21:08'),
(2, 'sam.vervaeke@time2change.com', '5b5f04139ec6ffbdbd0b3074681fe70a', 'Sam', 'Vervaeke', 1, 0, 0, 0, NULL, '2015-02-05 08:17:39', NULL),
(3, 'mario.vervaeke@time2change.com', 'd9c66bc61a0253eed1150219c983bca5', 'Mario', 'Vervaeke', 1, 0, 1, 0, NULL, '2015-02-09 23:29:34', NULL),
(4, 'jimi.heyndrickx@time2change.com', '56da7f9dd5ea962de7830225ad4e1214', 'Jimi', 'Heyndrickx', 1, 0, 3, 0, NULL, '2015-02-09 23:30:01', '2015-03-21 15:47:42'),
(5, 'frederik.vandenberge@time2change.com', 'b5ba2d05141738b573c8a0ce7bd1abd6', 'Frederik', 'Van Den Berghe', 2, 3, 2, 0, '2015-08-02 12:26:24', '2015-02-09 23:30:16', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `color` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Gegevens worden geëxporteerd voor tabel `users_groups`
--

INSERT INTO `users_groups` (`id`, `name`, `color`) VALUES
(1, 'Time2Change', '#1899ce'),
(2, 'Bravooh', '#11981e'),
(22, 'CDN Communications', '#000000');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
